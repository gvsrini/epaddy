package vtek.GL11;

public class AndroidPrinter {
	public native int prn_write_text(String text, int len, int font);
	public native int prn_open(); 
	public native int prn_paper_feed(int scanlines);
	public native int prn_close();
	public native int prn_paperstatus();
	public native int prn_lid_status();
	public native int prn_write_bmp(int[] bmpbuff, long Len);
	public native int prn_code128_barcode(String text);
	
}

package com.callippus.epaddy.dbfunctions;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.callippus.epaddy.resources.DBConnection;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by callippus on 02-02-2018.
 */

public class PaddyMovementDB extends DBConnection {

    public static final String TAG="PaddyMovementDB";
    public static  final int SUCCESS=0;
    public static  final int ERROR=-1;
    public static String final_qty;

    DBConnection db = null;
    SQLiteDatabase database;
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
            Locale.US);
    SimpleDateFormat onlyDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    Calendar cal = Calendar.getInstance();

    public PaddyMovementDB(Context context) throws IOException {
        super(context);
        try {
            db = new DBConnection(context);
            database = db.myDataBase;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String getConfiguration(String key)
    {
        String id = null;
        StringBuilder sb = new StringBuilder();
       /* sb.append("select VALUE from CONFIGURATION WHERE KEY='").append(key).append("'");*/
        sb.append("select VALUE from CONFIGURATION WHERE KEY=?");

        Cursor cursor = null;
        try {
            cursor = database.rawQuery(sb.toString(),new String[]{key});
            cursor.moveToFirst();
            id = cursor.getString(0);
        } catch (Exception e) {

        } finally {
            if (cursor != null)
                cursor.close();
        }
        return id;
    }

    public void setConfig(String key, String value) {
        StringBuilder sb = new StringBuilder();
        sb.append("update configuration set value='").append(value).append("' where key='").append(key).append("'");
        database.execSQL(sb.toString());
    }


    public int saveDetails(ArrayList<ContentValues> list,String table) {
        try {
            for (int i = 0; i < list.size(); i++) {
                System.out.println("value is :" + list.get(i).toString());
                long ret=database.insert(table, null, list.get(i));
                if(ret > 0)
                    return SUCCESS;
                else
                    return ERROR;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ERROR;
        }

        return ERROR;

    }

    /*check vehicle number*/
    public  int isValidVehicle(String vehicleno)
    {
        int count = 0;
        StringBuilder sb = new StringBuilder();
        /*sb.append("SELECT COUNT(*) TOTAL FROM VEHICLE_DETAILS WHERE VEHICLE_NUMBER ='").append(vehicleno).append("'");*/
        sb.append("SELECT COUNT(*) TOTAL FROM VEHICLE_DETAILS WHERE VEHICLE_NUMBER=?");

        Log.d(TAG,"The query in isValidVehicle() :: "+sb.toString());
        try {
            Cursor cr = myDataBase.rawQuery(sb.toString(),new String[]{vehicleno});
            cr.moveToFirst();
            count = cr.getInt(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(count > 0) {
            return  SUCCESS;
        }
        else
        {
            return ERROR;
        }

    }


    //update Vehicle Details

    public int updateVehicleDetails(ArrayList<ContentValues> list,String vehicle_no) {
        try {
            for (int i = 0; i < list.size(); i++) {
                StringBuilder sb = new StringBuilder();
/*
                sb.append("VEHICLE_NUMBER ='").append(vehicle_no).append("'");
*/              sb.append("VEHICLE_NUMBER =?");
                System.out.println("value is :::" + list.get(i).toString());
                long ret=database.update("VEHICLE_DETAILS",list.get(i),sb.toString(),new String[]{vehicle_no});
                if(ret > 0)
                    return SUCCESS;
                else
                    return ERROR;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ERROR;
        }

        return ERROR;

    }



    //opoulate vehicle details

    public  ArrayList<String> populateVehicleDetails(String vehicle_no)
    {
        ArrayList<String> wordList = new ArrayList<>();
        Cursor cursor = null;
        try {
            StringBuilder sb = new StringBuilder();
           /* sb.append("SELECT * FROM VEHICLE_DETAILS WHERE VEHICLE_NUMBER ='").append(vehicle_no).append("'");*/
            sb.append("SELECT * FROM VEHICLE_DETAILS WHERE VEHICLE_NUMBER =?");
            Log.d(TAG,"The query in populateVehicleDetails() :: "+sb.toString());
            cursor = database.rawQuery(sb.toString(),new String[]{vehicle_no});

            cursor.moveToFirst();
            if (cursor != null) {
                do {
                    for (int i = 0; i < cursor.getColumnCount(); i++) {

                        Log.e("", "The col value ::->" + cursor.getString(i));
                        wordList.add(cursor.getString(i));
                    }
                }while (cursor.moveToNext());
            }
            else
            {
                Log.d(TAG, "No Value :found in list.... ");
            }
            cursor.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return wordList;

    }


    /* vehicle details to show list view*/

    public ArrayList<String[]> getVehicleDetails(ArrayList<String[]> wordList) {

        wordList.clear();
        String val = null;

        StringBuilder sb = new StringBuilder();
        Cursor cursor = null;
        try {
            sb.append("SELECT ID,VEHICLE_NUMBER,VEHICLE_TYPE  FROM VEHICLE_DETAILS");

            cursor = database.rawQuery(sb.toString(), null);
            Log.e("Query", sb.toString());
            while (cursor.moveToNext()) {
                String[] obj = {cursor.getString(0), cursor.getString(1), cursor.getString(2)};
                wordList.add(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

        return wordList;
    }



    /*delete vehicle details*/

    public int deleteVehicle(String vehicle_no) {
        try {
            StringBuilder sb = new StringBuilder();
           /* sb.append("VEHICLE_NUMBER ='").append(vehicle_no).append("'");*/
            sb.append("VEHICLE_NUMBER =?");
            long ret=database.delete("VEHICLE_DETAILS",sb.toString(),new String[]{vehicle_no});
            if(ret > 0)
                return SUCCESS;
            else
                return ERROR;

        } catch (Exception e) {
            e.printStackTrace();
            return ERROR;
        }

    }

    public int insert_stock_header(double qty,String truck_no)
    {

        final_qty = String.valueOf(qty);
        ContentValues values = new ContentValues();
        String dpc_id=db.getPropValue("DPC_ID");
        String version = db.appVersion();
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateTime = new SimpleDateFormat("yyMMddHHmmss",
                Locale.US);
        String current_date=dateTime.format(cal.getTime());
       String id= dpc_id+current_date;


        values.put("ID", id);
        values.put("DPC_ID", dpc_id);
        values.put("REF_ORDER_ID", 0);
        values.put("TXN_DATE", date.format(cal.getTime()));
        values.put("STORAGE_TYPE", 0);
        values.put("STORAGE_ID", 0);
        values.put("TXN_TYPE", 3);
        values.put("TXN_MODE", "ONLINE");
        values.put("TOTAL_QTY", qty);
        values.put("TRUCK_MEMO_ID", truck_no);
        values.put("STOCK_ACK", 0);
        values.put("STATUS", 0);
        values.put("SYNC_STATUS", 0);
        values.put("VERSION", version);

        int temp=Integer.parseInt(id)+1;
        setConfig("STOCK_ID",String.valueOf(temp));

        try {
            database.insert("STOCK_HEADER", null, values);
         int response=insert_stock_trailer(id,truck_no);
        return response;
        } catch (Exception e) {
            e.printStackTrace();
            return  ERROR;
        }

    }

    public int insert_stock_trailer(String id,String truckno)
    {
        int res=0;

        StringBuilder sb = new StringBuilder();
        Cursor cursor = null;
        try {
            sb.append("SELECT ITEM_ID,BAG_WEIGHT,BAG_COUNT,MOISTURE,TOTAL_QTY FROM STOCK_IN_PROCESS");

            cursor = database.rawQuery(sb.toString(), null);
            Log.d("Query", sb.toString());
            while (cursor.moveToNext()) {

                ContentValues values = new ContentValues();

                values.put("ID", id);
                values.put(" ITEM_CODE", cursor.getString(0));
                values.put(" BAG_WEIGHT",cursor.getString(1));
                values.put("NO_OF_BAGS", cursor.getString(2));
                values.put("MOISTURE_PERCENTAGE",cursor.getString(3));
                values.put("QTY",cursor.getString(4));

                    Log.d(TAG,"Values in Stock Trailer :: "+values);
                    database.insert("STOCK_TRAILER", null, values);
                    insert_transport_details(id,truckno);
                    insert_transport_details_trailer(id,cursor.getString(0),cursor.getString(4),cursor.getString(3),"0");
                    update_stock_inventory(cursor.getString(0),cursor.getString(4));
                    update_gunny_inventory(cursor.getString(0),cursor.getString(2));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

        return SUCCESS;
    }


    public void update_stock_inventory(String id, String qty) {


//        SQLiteDatabase db = this.getWritableDatabase();

        String sql = " UPDATE STOCK_INVENTORY SET CLOSING=ROUND(CLOSING -"+qty+",2),sync_status='0' WHERE PRODUCT_ID='"+id+"'  AND TXN_DATE=DATE('now','localtime') ";

        Log.d(TAG,"Query in Update stock_inventory--> "+sql);
       database.execSQL(sql);
    }

    public void update_gunny_inventory(String id, String bag_count)
    {
        String sql = " UPDATE GUNNY_INVENTORY SET GUNNY_CLOSING=CLOSING -"+bag_count+",sync_status='0' WHERE GUNNY_ID='"+id+"'  AND TXN_DATE=DATE('now','localtime') ";

        Log.d(TAG,"Query in update_gunny_inventory--> "+sql);
        database.execSQL(sql);
    }

    public void insert_transport_details(String id,String truck_no)
    {
        String dpc_id=db.getPropValue("DPC_ID");
        String vehicle_details_id=getVehiceDetailsId(truck_no);

        ContentValues values = new ContentValues();

        values.put("ID", id);
        values.put("VEHICLE_NUMBER", truck_no);
        values.put("TRANSPORT_DATE", date.format(cal.getTime()));
        values.put("VEHICLE_DETAILS_ID", vehicle_details_id);
        values.put("STATUS", 1);
        values.put("SYNC_STATUS", 0);
        values.put("DPC_ID", dpc_id);

        try {
            database.insert("TRANSPORT_DETAILS", null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_transport_details_trailer(String id,String product_id,String qty,String moisture,String destinaton)
    {
        ContentValues values = new ContentValues();

        values.put("ID", id);
        values.put("PRODUCT_ID", product_id);
        values.put("QTY",qty);
        values.put("MOISTURE", moisture);
        values.put("DESTINATION", destinaton);

        try {
            database.insert("TRANSPORT_DETAILS_TRAILER", null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getVehiceDetailsId(String vehicleno)
    {
        String vehicledetailsid = null;
        StringBuilder sb = new StringBuilder();

        sb.append("SELECT ID  FROM VEHICLE_DETAILS WHERE VEHICLE_NUMBER=?");
        Log.d(TAG,"The query in getVehiceDetailsId() :: "+sb.toString());
        try {
            Cursor cr = myDataBase.rawQuery(sb.toString(),new String[]{vehicleno});
            cr.moveToFirst();
            vehicledetailsid = cr.getString(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG,"Vehicle Details Id in getVehiceDetailsId() :: "+vehicledetailsid);
            return vehicledetailsid;

    }


        /* get Vehicle type codes*/

    public ArrayList<String> getVehicleType()
    {
        ArrayList<String> vehicle_type_list = new ArrayList<>();
        Cursor cursor = null;
        try {
            StringBuilder sb=new StringBuilder();
            sb.append("SELECT NAME FROM VEHICLE_TYPE");
            cursor = database.rawQuery(sb.toString(), null);
            cursor.moveToFirst();
            if (cursor != null) {
                do {
                    for (int i = 0; i < cursor.getColumnCount(); i++) {
                        vehicle_type_list.add(cursor.getString(i));
                    }
                }while (cursor.moveToNext());
            }
            else
            {
                Log.d(TAG, "No Value :found in list.... ");
            }
            cursor.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return vehicle_type_list;
    }

}

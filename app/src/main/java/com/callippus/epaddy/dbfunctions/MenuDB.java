package com.callippus.epaddy.dbfunctions;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.EditText;
import android.widget.RadioButton;

import com.callippus.epaddy.resources.DBConnection;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MenuDB extends DBConnection {

	DBConnection db = null;
	SQLiteDatabase database1;
	Calendar cal = Calendar.getInstance();
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
	SimpleDateFormat dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
			Locale.US);

	//////////////////////////////////////////////////////////////////////////////////////// getting super class instance
	public MenuDB(Context context) throws IOException {
		super(context);
		try {
			db = new DBConnection(context);
            database1 = db.myDataBase;
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void BOD() {
		String sql = "INSERT INTO STOCK_INVENTORY (txn_date,PRODUCT_ID,opening,closing,sync_status)"
				+ " select date('NOW') txn_date,product_id,CLOSING opening,closing,'0' from"
				+ " (SELECT * FROM stock_inventory where date(txn_date)= (select max(date(txn_date)) from "
				+ "stock_inventory) ) MAX_INV_TABLE where txn_date != date('NOW') and closing > 0";
		Log.e("update query", sql);
		// database1.rawQuery(sql, null);
		database1.execSQL(sql);
	}

	// Test Inital Cash---Sale
	public boolean getAccess() {
		boolean check = false;
        String sql = "select * from initial_cash";
        try (Cursor mCur = database1.rawQuery(sql, null)) {
            String date;
            String today = dateFormat.format(cal.getTime());
            if (mCur.moveToFirst()) {
                do {
                    date = mCur.getString(0).substring(0, 10);
                    if (today.equals(date))
                        check = true;
                } while (mCur.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////for
	// Menu_Sale
	// Insert Initial Cash
	public void insertInitalCash(String value) {
		ContentValues values = new ContentValues();
		values.put("INITIAL_CASH_DATE", dateTime.format(cal.getTime()));
		values.put("INITIAL_CASH", value);
		values.put("RECONCILE_STATUS", 0);
		values.put("RECONCILE_ID", 0);
		database1.insert("INITIAL_CASH", null, values);
	}
////////////////////////////////////////////////////////////////////////////////////////////////// State & Locations
	// Retrieve Route codes
	public Cursor getRouteCodes() {
		String selectQuery = "select CODE, DESCRIPTION from State_Master group by CODE";
		Cursor cursor = database1.rawQuery(selectQuery, null);
		return cursor;
	}

	// Retrieve Route Stops
	public Cursor getRouteStops(String code) {
		String selectQuery = "select NAME, TYPE, WH_CODE, RETAILER_ID  from WAREHOUSE_LOCATIONS where"
				+ " STATE_CODE= '" + code + "'";
		Cursor cursor = database1.rawQuery(selectQuery, null);
		return cursor;
	}

	///////////////////////////////////////////////////////////////////////////////////////// Add Customer
	public String AddCustomer(ContentValues values) {
		// TODO Auto-generated method stub
		
		String custSeqId = db.fromConfiguration("CUST_SEQ_ID");
		String retailId = db.fromProperties("retailer_id");
		String custId = Integer.toString(Integer.parseInt(
				retailId.substring(2, retailId.length()), 10) + 100000)
				+ custSeqId;
		values.put("CUST_ID", custId);
		values.put("PRICE_LIST_ID", db.fromProperties("kirana_pricelist_id"));
		values.put("TRANS_ID", db.fromConfiguration("TRANS_ID"));
		try {
			database1.insert("CUSTOMER_DETAILS", null, values);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return custId;
	}

	public String AddNewCustomer(EditText firstName, EditText lastName,
			EditText address1, EditText address2, EditText city,
			EditText state, EditText pin, EditText mobile, EditText emailId,
			EditText tin, RadioButton mORf, EditText dob) {
		
		ContentValues values = new ContentValues();
		values.put("CUST_NAME", firstName.getText().toString() + " "
				+ lastName.getText().toString());
		values.put("MOBILE_NO", mobile.getText().toString());
		values.put("EMAIL_ID", emailId.getText().toString());
		values.put("GENDER", mORf.getText().toString());
		values.put("STATE", state.getText().toString());
		values.put("CITY", city.getText().toString());
		values.put("PINCODE", pin.getText().toString());
		values.put("DOB", dob.getText().toString());
		values.put("ADDRESS1", address1.getText().toString());
		values.put("address2", address2.getText().toString());
		values.put("TIN", tin.getText().toString());
		values.put("OUT_STANDING", "null");
		values.put("LAST_MOD_DATE", "");
		values.put("CUST_TYPE", "null");
		values.put("CREDIT_DEBIT", "");
		values.put("REWARD_POINTS", "");
		values.put("TOTAL_PURCHASES_TILL_DATE", "null");
		values.put("TOTAL_PURCHASES_ELIGIBLE_FOR_OFFER", "");
		values.put("CURRENT_DISCOUNT", "null");
		values.put("USED_DISCOUNT", "0");
		values.put("CARD_ID", "0");
		values.put("STATUS", "1");
		values.put("SYNC_STATUS", "0");

		String custId = AddCustomer(values);
		return custId;
	}
	////////////////////////////////////////////////////////////////////////////Pending Transactions Query
	public String getPending(String tableName) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("select count(*) from ").append(tableName).append(" ");
		if (!tableName.equals("STOCK_HEADER"))
			sb.append(" where sync_status in ('0', 'I')");
		else
			sb.append(" where sync_status+0!=1 and order_type='1'");
        String id = null;
        try (Cursor cr = database1.rawQuery(sb.toString(), null)) {
            cr.moveToFirst();
            id = cr.getString(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
		return id;
	}

	public void incrementCustId(String custId) {
		
		// TODO Auto-generated method stub
		int cust = Integer.parseInt(custId)+1;
		custId = String.format("%04d",cust);
		String sql = "update configuration set value='"+custId+"' where key='CUST_SEQ_ID'";
		database1.execSQL(sql);
	}
}

package com.callippus.epaddy.dbfunctions;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.EditText;
import android.widget.RadioButton;

import com.callippus.epaddy.resources.DBConnection;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class MainActivityDAO {
    DBConnection db = null;
    public static final int SUCCESS = 0;
    public static final int  ERROR = -1;
    SQLiteDatabase database1;
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    SimpleDateFormat dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
            Locale.US);

    //////////////////////////////////////////////////////////////////////////////////////// getting super class instance
    public MainActivityDAO(Context context) throws IOException {
        try {
            db = new DBConnection(context);
            database1 = db.myDataBase;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void deleteDistribution() {
        database1.delete("MEMBERS_ONLINE", null, null);
        database1.delete("DISTRIBUTION_ONLINE", null, null);
    }

    public ArrayList<String[]> getMembers(ArrayList<String[]> wordList) {
        wordList.clear();
        String val = null;

        StringBuilder sb = new StringBuilder();
        Cursor cursor = null;
        try {
            sb.append("SELECT MEMBERNAME, RELATION, UID, SLNO FROM MEMBERS_ONLINE ORDER BY SLNO");

            cursor = database1.rawQuery(sb.toString(), null);
            Log.e("Query", sb.toString());
            while (cursor.moveToNext()) {
                String[] obj = {cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3)};
                wordList.add(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }
        return wordList;
    }

    public ArrayList<String[]> getFarmers(ArrayList<String[]> wordList) {

        wordList.clear();
        String val = null;

        StringBuilder sb = new StringBuilder();
        Cursor cursor = null;
        try {
            sb.append("SELECT FARMER_CODE, FIRST_NAME,LAST_NAME  FROM FARMER_DETAILS");

            cursor = database1.rawQuery(sb.toString(), null);
            Log.e("Query", sb.toString());
            while (cursor.moveToNext()) {
                String[] obj = {cursor.getString(0), cursor.getString(1), cursor.getString(2)};
                wordList.add(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

        return wordList;
    }



    public ArrayList<String[]>  getProduct_details(ArrayList<String[]> wordList) {
        wordList.clear();
        String val = null;

        StringBuilder sb = new StringBuilder();
        Cursor cursor = null;
        try {
            sb.append("select id ID,name_en Name,Ifnull((select total_qty from stock_in_process where  Item_id=pd.id),'0.0') TOTAL_Qty from product_details pd");

            cursor = database1.rawQuery(sb.toString(), null);
            Log.e("Query", sb.toString());
            while (cursor.moveToNext()) {
                String[] obj = {cursor.getString(0), cursor.getString(1), cursor.getString(2)};
                wordList.add(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }

    return wordList;

    }



        public Cursor getOrdersInProcess() {
        // TODO Auto-generated method stub
        Cursor cr = database1.rawQuery("select (select p.NAME from DISTRIBUTION_ONLINE p where p.PRODUCT_ID=ID) as name," +
                "(select p.PRICE from DISTRIBUTION_ONLINE p where p.PRODUCT_ID=ID) as PRICE," +
                "ID,QTY,AMOUNT,VAT from ORDERS_IN_PROCESS", null);
        return cr;
    }

    public ArrayList<String[]> getProducts(ArrayList<String[]> wordList) {
        wordList.clear();
        String val = null;

        StringBuilder sb = new StringBuilder();
        Cursor cursor = null;
        try {
            sb.append("SELECT PRODUCT_ID,NAME,CARD_TYPE,ALLOTMENT_TYPE,IFNULL(QUANTITY,'0.00') QUANTITY,PRICE,UOM,MAX_QUANTITY,AVAILED_QTY, IFNULL(AMT,'0.00') AMT FROM " +
                    "( " +
                    "SELECT PRODUCT_ID,NAME,CARD_TYPE,ALLOTMENT_TYPE,(SELECT QTY FROM ORDERS_IN_PROCESS WHERE ID=D.PRODUCT_ID)QUANTITY,PRICE,UOM,MAX_QUANTITY,AVAILED_QTY,(SELECT AMOUNT FROM ORDERS_IN_PROCESS WHERE ID=D.PRODUCT_ID) AMT FROM DISTRIBUTION_ONLINE D " +
                    ")");

            cursor = database1.rawQuery(sb.toString(), null);
            Log.e("Query", sb.toString());
            while (cursor.moveToNext()) {
                String[] obj = {cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9)};
                wordList.add(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }
        return wordList;
    }

    public void finishOrder(String id) {
        deleteProcessedOrders("");
        long nextOrder = Long.valueOf(id) + 1;
        setConfig("MAX_ORDER_ID", Long.valueOf(nextOrder).toString());
    }

    public void deleteProcessedOrders(String pr_id) {
        // TODO Auto-generated method stub
        if (pr_id.equals(""))
            database1.delete("ORDERS_IN_PROCESS", null, null);
        else
            database1.delete("ORDERS_IN_PROCESS", "PRODUCT_ID='" + pr_id + "'", null);
    }


    public void BOD() {
        String sql =" INSERT INTO stock_inventory ( "+
                    " 	txn_date "+
                    " 	,product_id "+
                    " 	,opening "+
                    " 	,closing "+
                    " 	,sync_status "+
                    " 	,dpc_id "+
                    " 	) "+
                    "  select * from( "+
                    " SELECT DATE ('NOW') txn_date "+
                    " 	,product_id "+
                    " 	,closing opening "+
                    " 	,closing "+
                    " 	,'0' SYNC_STATUS "+
                    " 	,( "+
                    " 		SELECT value "+
                    " 		FROM properties "+
                    " 		WHERE KEY = 'DPC_ID' "+
                    " 		) DPC_ID "+
                    " FROM ( "+
                    " 	SELECT product_id "+
                    " 		,Sum(opening) opening "+
                    " 		,Sum(closing) closing "+
                    " 	FROM ( "+
                    " 		SELECT product_id "+
                    " 			,opening "+
                    " 			,closing "+
                    " 		FROM stock_inventory st "+
                    " 		WHERE DATE (txn_date) = ( "+
                    " 				SELECT Max(DATE (txn_date)) "+
                    " 				FROM stock_inventory  where product_id=st.product_id and dpc_id=st.dpc_id "+
                    " 				) "+
                    " 		 "+
                    " 		UNION "+
                    " 		 "+
                    " 		SELECT id product_id "+
                    " 			,0 opening "+
                    " 			,0 closing "+
                    " 		FROM product_details "+
                    " 		) "+
                    " 	GROUP BY product_id + 0 "+
                    " 	)) x   "+
                    " 	 	where not exists (select 1 from stock_inventory s   "+
                    "             				where x.txn_date = s.txn_date  "+
                    "             				and x.product_id = s.product_id   "+
                    "             				and x.dpc_id = s.dpc_id   ) " ;

        Log.e(" query in BOD() :: ", sql);
        // database1.rawQuery(sql, null);
        database1.execSQL(sql);
    }

    public void gunny_BOD() {
        String sql =" INSERT INTO gunny_inventory ( "+
                " 	date "+
                " 	,gunny_id "+
                " 	,gunny_opening "+
                " 	,gunny_closing "+
                " 	,sync_status "+
                " 	,dpc_id "+
                " 	) "+
                "  select * from( "+
                " SELECT DATE ('NOW') date "+
                " 	,gunny_id "+
                " 	,closing opening "+
                " 	,closing "+
                " 	,'0' SYNC_STATUS "+
                " 	,( "+
                " 		SELECT value "+
                " 		FROM properties "+
                " 		WHERE KEY = 'DPC_ID' "+
                " 		) DPC_ID "+
                " FROM ( "+
                " 	SELECT gunny_id "+
                " 		,Sum(gunny_opening) opening "+
                " 		,Sum(gunny_closing) closing "+
                " 	FROM ( "+
                " 		SELECT gunny_id "+
                " 			,gunny_opening "+
                " 			,gunny_closing "+
                " 		FROM gunny_inventory gt "+
                " 		WHERE DATE (date) = ( "+
                " 				SELECT Max(DATE (date)) "+
                " 				FROM gunny_inventory  where gunny_id=gt.gunny_id and dpc_id=gt.dpc_id "+
                " 				) "+
                " 		 "+
                " 		UNION "+
                " 		 "+
                " 		SELECT id gunny_id "+
                " 			,0 gunny_opening "+
                " 			,0 gunny_closing "+
                " 		FROM product_details "+
                " 		) "+
                " 	GROUP BY gunny_id + 0 "+
                " 	)) x   "+
                " 	 	where not exists (select 1 from gunny_inventory s   "+
                "             				where x.date = s.date  "+
                "             				and x.gunny_id = s.gunny_id   "+
                "             				and x.dpc_id = s.dpc_id   ) " ;

        Log.e(" gunny_BOD() :: ", sql);
        // database1.rawQuery(sql, null);
        database1.execSQL(sql);
    }

    // Test Inital Cash---Sale
    public boolean getAccess() {
        boolean check = false;
        String sql = "select * from initial_cash";
        try (Cursor mCur = database1.rawQuery(sql, null)) {
            String date;
            String today = dateFormat.format(cal.getTime());
            if (mCur.moveToFirst()) {
                do {
                    date = mCur.getString(0).substring(0, 10);
                    if (today.equals(date))
                        check = true;
                } while (mCur.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    }

    // /////////////////////////////////////////////////////////////////////////////////////////for
    // Menu_Sale
    // Insert Initial Cash
    public void insertInitalCash(String value) {
        ContentValues values = new ContentValues();
        values.put("INITIAL_CASH_DATE", dateTime.format(cal.getTime()));
        values.put("INITIAL_CASH", value);
        values.put("RECONCILE_STATUS", 0);
        values.put("RECONCILE_ID", 0);
        database1.insert("INITIAL_CASH", null, values);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////// State & Locations
    // Retrieve Route codes
    public Cursor getRouteCodes() {
        String selectQuery = "select CODE, DESCRIPTION from State_Master group by CODE";
        Cursor cursor = database1.rawQuery(selectQuery, null);
        return cursor;
    }

    // Retrieve Route Stops
    public Cursor getRouteStops(String code) {
        String selectQuery = "select NAME, TYPE, WH_CODE, RETAILER_ID  from WAREHOUSE_LOCATIONS where"
                + " STATE_CODE= '" + code + "'";
        Cursor cursor = database1.rawQuery(selectQuery, null);
        return cursor;
    }

    ///////////////////////////////////////////////////////////////////////////////////////// Add Customer
    public String AddCustomer(ContentValues values) {
        // TODO Auto-generated method stub

        String custSeqId = db.fromConfiguration("CUST_SEQ_ID");
        String retailId = db.fromProperties("retailer_id");
        String custId = Integer.toString(Integer.parseInt(
                retailId.substring(2, retailId.length()), 10) + 100000)
                + custSeqId;
        values.put("CUST_ID", custId);
        values.put("PRICE_LIST_ID", db.fromProperties("kirana_pricelist_id"));
        values.put("TRANS_ID", db.fromConfiguration("TRANS_ID"));
        try {
            database1.insert("CUSTOMER_DETAILS", null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return custId;
    }

    public String AddNewCustomer(EditText firstName, EditText lastName,
                                 EditText address1, EditText address2, EditText city,
                                 EditText state, EditText pin, EditText mobile, EditText emailId,
                                 EditText tin, RadioButton mORf, EditText dob) {

        ContentValues values = new ContentValues();
        values.put("CUST_NAME", firstName.getText().toString() + " "
                + lastName.getText().toString());
        values.put("MOBILE_NO", mobile.getText().toString());
        values.put("EMAIL_ID", emailId.getText().toString());
        values.put("GENDER", mORf.getText().toString());
        values.put("STATE", state.getText().toString());
        values.put("CITY", city.getText().toString());
        values.put("PINCODE", pin.getText().toString());
        values.put("DOB", dob.getText().toString());
        values.put("ADDRESS1", address1.getText().toString());
        values.put("address2", address2.getText().toString());
        values.put("TIN", tin.getText().toString());
        values.put("OUT_STANDING", "null");
        values.put("LAST_MOD_DATE", "");
        values.put("CUST_TYPE", "null");
        values.put("CREDIT_DEBIT", "");
        values.put("REWARD_POINTS", "");
        values.put("TOTAL_PURCHASES_TILL_DATE", "null");
        values.put("TOTAL_PURCHASES_ELIGIBLE_FOR_OFFER", "");
        values.put("CURRENT_DISCOUNT", "null");
        values.put("USED_DISCOUNT", "0");
        values.put("CARD_ID", "0");
        values.put("STATUS", "1");
        values.put("SYNC_STATUS", "0");

        String custId = AddCustomer(values);
        return custId;
    }

    ////////////////////////////////////////////////////////////////////////////Pending Transactions Query
    public String getPending(String tableName) {

        StringBuilder sb = new StringBuilder();
        sb.append("select count(*) from ").append(tableName).append(" ");
        if (!tableName.equals("STOCK_HEADER"))
            sb.append(" where sync_status in ('0', 'I')");
        else
            sb.append(" where sync_status+0!=1 and order_type='1'");
        String id = null;
        try (Cursor cr = database1.rawQuery(sb.toString(), null)) {
            cr.moveToFirst();
            id = cr.getString(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public void deleteOrder() {
        String sql = "delete from orders_in_process";
        database1.execSQL(sql);
    }

    public void incrementCustId(String custId) {

        // TODO Auto-generated method stub
        int cust = Integer.parseInt(custId) + 1;
        custId = String.format("%04d", cust);
        String sql = "update configuration set value='" + custId + "' where key='CUST_SEQ_ID'";
        database1.execSQL(sql);
    }


    public void setConfig(String key, String value) {
        String sql = "update configuration set value='" + value + "' where key='" + key + "'";
        database1.execSQL(sql);
    }

    public String getConfig(String key) {
        String id = null;
        String sql = "select VALUE from CONFIGURATION WHERE KEY='" + key + "'";

        Cursor cursor = null;
        try {
            cursor = database1.rawQuery(sql, null);
            cursor.moveToFirst();
            id = cursor.getString(0);
        } catch (Exception e) {

        } finally {
            if (cursor != null)
                cursor.close();
        }
        return id;
    }

    public double getqty_from_stock_in_process()
    {
        String id = null;
        String sql = "select sum(total_qty) totalqty from STOCK_IN_PROCESS ";

        Cursor cursor = null;
        try {
            cursor = database1.rawQuery(sql, null);
            cursor.moveToFirst();
            id = cursor.getString(0);
        } catch (Exception e) {
            return ERROR;
        } finally {
            if (cursor != null)
                cursor.close();
        }

        double qty = Double.parseDouble(id);
        return qty;
    }
}

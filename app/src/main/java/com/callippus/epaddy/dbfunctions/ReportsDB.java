package com.callippus.epaddy.dbfunctions;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.callippus.epaddy.resources.DBConnection;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by callippus on 02-02-2018.
 */

public class
ReportsDB extends DBConnection {

    public static final String TAG="ReportsDB";
    public static  final int SUCCESS=0;
    public static  final int ERROR=-1;

    DBConnection db = null;
    SQLiteDatabase database;
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
            Locale.US);
    SimpleDateFormat onlyDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    Calendar cal = Calendar.getInstance();

    public ReportsDB(Context context) throws IOException {
        super(context);
        try {
            db = new DBConnection(context);
            database = db.myDataBase;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /* get monthly farmer registration report*/

    public Cursor getMonthlyFarmerReport(String fr, String to) {

        String s = "select FARMER_CODE,FIRST_NAME,LAST_NAME,MOBILE_NO,AADHAAR_NO,BANK_ACCOUNT_NO from FARMER_DETAILS "+
                " where  date(INSERTED_DATE) between '"+fr.toString()+"' and '"+to.toString()+"'";
        Cursor cr = database.rawQuery(s, null);
        return cr;
    }

    /*SELECT NAME,PRODUCT_ID,OPENING,CLOSING FROM  STOCK_INVENTORY  SI  JOIN PRODUCT_DETAILS  PD ON (SI.PRODUCT_ID=PD.ID)*/


    public Cursor getStockSummaryReport(String fr) {

        String s = "SELECT PRODUCT_ID,NAME_EN,OPENING,CLOSING FROM  STOCK_INVENTORY  SI  JOIN PRODUCT_DETAILS  PD ON (SI.PRODUCT_ID=PD.ID)  WHERE date(SI.TXN_DATE)='"+fr.toString()+"'";
        Cursor cr = database.rawQuery(s, null);
        return cr;
    }


    /*get daily farmer registration report*/


    public Cursor getDailyFarmerReport(String fr) {

        String s = "select FARMER_CODE,FIRST_NAME,LAST_NAME,MOBILE_NO,AADHAAR_NO,BANK_ACCOUNT_NO from FARMER_DETAILS "+
                " where  date(INSERTED_DATE)='"+fr.toString()+"'";
        Cursor cr = database.rawQuery(s, null);
        return cr;
    }




     /*get daily vehicle movement report*/


    public Cursor getDailyVehicleMovementReport(String fr) {

        String s = "select  td.vehicle_number,pd.name_en,sh.TOTAL_QTY ,st.MOISTURE_PERCENTAGE,sh.TRUCK_MEMO_ID,sh.TXN_DATE,st.BAG_WEIGHT,st.NO_OF_BAGS from stock_header sh join stock_trailer st on (sh.id=st.id) \n" +
                "join product_details pd on(st.ITEM_CODE=pd.id) join transport_details td on (td.id=sh.TRUCK_MEMO_ID) where date(sh. TXN_DATE)='"+fr.toString()+"'";
        Cursor cr = database.rawQuery(s, null);
        return cr;
    }



    /* get monthly vehicle movement report*/

    public Cursor getMonthlyVehicleMovementReport(String fr,String to) {

        String s = "select  td.vehicle_number,pd.name_en,sh.TOTAL_QTY ,st.MOISTURE_PERCENTAGE,sh.TRUCK_MEMO_ID,sh.TXN_DATE,st.BAG_WEIGHT,st.NO_OF_BAGS from stock_header sh join stock_trailer st on (sh.id=st.id) \n" +
                "join product_details pd on(st.ITEM_CODE=pd.id) join transport_details td on (td.id=sh.TRUCK_MEMO_ID) where date(sh. TXN_DATE) between date('"+fr.toString()+"') and date('"+to.toString()+"')";
        Cursor cr = database.rawQuery(s, null);
        return cr;
    }

    public Cursor getGunnySummaryReport(String fr) {

        String s = "SELECT GUNNY_ID,(SELECT NAME_EN FROM PRODUCT_DETAILS PD WHERE PD.ID+0 = GI.GUNNY_ID+0) NAME,GUNNY_OPENING,GUNNY_CLOSING FROM  GUNNY_INVENTORY  GI    WHERE DATE(GI.DATE)='"+fr.toString()+"'";
        Log.d(TAG,"Query in getGunnySummaryReport() ::"+s);
        Cursor cr = database.rawQuery(s, null);
        return cr;
    }

    /*get stock detailed report*/
    public Cursor getStockDetailedReport(String fr,String to) {

        String s = "SELECT PRODUCT_ID,NAME_EN,OPENING,CLOSING FROM  STOCK_INVENTORY  SI  JOIN PRODUCT_DETAILS  PD ON (SI.PRODUCT_ID=PD.ID)  WHERE date(SI.TXN_DATE) between '"+fr.toString()+"' and '"+to.toString()+"'";
        Cursor cr = database.rawQuery(s, null);
        return cr;
    }

}

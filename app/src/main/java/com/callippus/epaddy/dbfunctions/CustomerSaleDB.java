package com.callippus.epaddy.dbfunctions;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.callippus.epaddy.resources.DBConnection;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class CustomerSaleDB extends DBConnection {

    DBConnection db = null;
	SQLiteDatabase database;
	SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
			Locale.US);
	SimpleDateFormat onlyDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
	Calendar cal = Calendar.getInstance();

	public CustomerSaleDB(Context context) throws IOException {
		super(context);
        try {
			db = new DBConnection(context);
            database = db.myDataBase;
        } catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Cursor getProducts(String text, String type) {
		String val = null;
		String s = "select VALUE from PROPERTIES where KEY='default_price_list'";

		if (type.equals("custSale") || type.equals("grn")
				|| type.equals("reverseGrn"))
			val = db.fromProperties("default_price_list");
		else if (type.equals("kiranaSale"))
			val = db.fromProperties("kirana_pricelist_id");

		Cursor c1 = null;
        Cursor cursor = null;
        try {
            c1 = database.rawQuery(s, null);
            c1.moveToFirst();
            String sql = "SELECT NAME,pd.ID,pp.LOWER_LIMIT,VAT,VAT_INCLUDED, BASE_PRICE FROM "
                    + " (select * FROM PRODUCT_DETAILS where price_list_id='"
                    + val
                    + "' AND "
                    + "(NAME LIKE '%"
                    + text
                    + "%' OR OPTIMAL_LEVEL like '%"
                    + text
                    + "%')) PD JOIN "
                    + "(select * from product_price where price_list_id='"
                    + val
                    + "') pp on pd.id=pp.id order by NAME Desc";
            Log.e("query", sql);

            cursor = database.rawQuery(sql, null);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally {
            if(c1!= null)
               c1.close();
        }
		return cursor;
	}

	public void addOrderTrailer(ArrayList<ContentValues> list) {
		try {
			for (int i = 0; i < list.size(); i++) {
				System.out.println("value is :::" + list.get(i).toString());
				database.insert("ORDER_TRAILER", null, list.get(i));
				// delete(list.get(i).getAsString("PRODUCT_ID"),list.get(i).getAsInteger("QTY"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getId() {
		int id = 0;
		Cursor cursor  =null;
		try {
			String sql = "select max(ID+0) from order_header";
			cursor = database.rawQuery(sql, null);
			cursor.moveToFirst();
			id = cursor.getInt(0);
			id = id + 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			if(cursor!=null) cursor.close();
		}
		return id;
	}

	

	public ContentValues addCustomerSaleItems(Float amt, String vat,
			String qty, String price, String pr_id, String net,
			Float tVat, int count, String type, String kiranaId, String disc,
			String promoId, String freeFlg, String promoType) {

		ContentValues values = new ContentValues();
		values.put("ID","000"+ getId());
		values.put("UNIQUE_CODE", count);
		values.put("AMOUNT", String.format("%.2f", amt));
		System.out.println("AMOUNT" + amt);
		values.put("VAT",String.format("%.2f", Float.parseFloat(vat)));
		System.out.println("VAT" + vat);
		values.put("ORDER_TYPE", "5");
		values.put("QTY", qty);
		System.out.println("QTY" + qty);
		values.put("ABS_REL", "0");
		values.put("DISC_AMT",
				String.format("%.2f", 0.0000));
		values.put("PRICE", price);
		System.out.println("price" +String.format("%.2f",Float.parseFloat( price)));
		values.put("FINAL_AMOUNT",
				String.format("%.2f",Float.parseFloat(net) - 0.0000));
		values.put("DISCOUNT",
				String.format("%.2f",0.0000));
		values.put("PRODUCT_ID", pr_id);
		System.out.println("pr id " + pr_id);
		values.put("PRODUCT_GROUP_ID", "0");
		values.put(
				"PRICE_AFTER_LINE_DISCOUNT",
				String.format("%.2f",0.0000));
		System.out.println("pald " + String.format("%.2f",Float.parseFloat(price)));
		values.put("MULTI_ABS_REL", "0");
		values.put("MULTI_DISC_AMT", "0");
		values.put("MULTI_DISCOUNT", "0");
		values.put("VAT_AMT", String.format("%.2f",Float.parseFloat(vat)));
		System.out.println("VAT AMT " + Float.parseFloat(vat));
		values.put("INDENT_QTY", "0");
		values.put("NON_SALE_FLAG", "0");
		values.put("RECEIVED_ITEM", "0");

		//if (type.equals("custSale"))
			values.put("CUST_ID", "0");
		/*else if (type.equals("kiranaSale"))
			values.put("CUST_ID", String.format("%d",Integer.parseInt(kiranaId)));*/

		values.put("PRICE_LIST_ID", "0");
		values.put("REDEEM_FLAG", "0");
		values.put("VAT_INCLUDED", String.format("%.2f",Float.parseFloat(vat)));
		System.out.println("VAT_IN " + vat);
		values.put("PROMO_ID", promoId);
		values.put("FREE_FLG", freeFlg);
		values.put("PROMO_TYPE", promoType);
		return values;
	}

	@SuppressWarnings("null")
	public void addOrderHeader(Float totalAmt, Float totalVat, String code,
			String stop, String kiranaId, String[] a2, Float totalDisc, String retailerId) {
		String office_id = fromProperties("retailer_id");
		String price_id = fromProperties("default_price_list");
		String ord_disc = "0";
		String custId = "0";
		String custType = "0";
		String net = String.format("%.2f",(totalAmt + totalVat));

		if (!kiranaId.equals(""))
			custType = "2";

		if (a2 != null) {
			if (a2.length >= 6) {
				ord_disc = String.format("%.2f",(totalAmt
						* (Float.parseFloat(a2[1]) / 100)));
				net = String.format("%.2f",(totalAmt
						* (1 - (Float.parseFloat(a2[1]) / 100))));
				custId = a2[0];
				custType = "1";
				if ((Float.parseFloat(ord_disc)) > (Float.parseFloat(a2[4])))
					ord_disc = a2[4];

				insertDiscountDetails(getId(), a2, ord_disc);
			}
		}

		ContentValues val = new ContentValues();
		val.put("OFFICE_ID", office_id);
		val.put("ID", "000"+getId());
		val.put("SUPPLIER_ID", retailerId);
		val.put("REF", "0");
		val.put("ORDER_DATE", date.format(cal.getTime()));
		val.put("ORDER_TYPE", "5");
		val.put("AMOUNT", Float.toString(totalAmt));
		val.put("VAT", Float.toString(totalVat));
		val.put("ORDER_DISCOUNT", Float.parseFloat(ord_disc) + totalDisc);
		val.put("PAYMENT_MODE", "1");
		val.put("STATUS", "0");
		val.put("REMARKS", "");
		val.put("USER_ID", "1");
		val.put("RECONCILE_STATUS", "0");
		val.put("SYNC_STATUS", "0");
		val.put("ORIGINAL_ORDER_ID", "0");
		val.put("PRICE_LIST_ID", price_id);
		val.put("DISCOUNT_VALUE", Float.parseFloat(ord_disc) + totalDisc);
		val.put("DISCOUNT_ABS_REL", "0");
		val.put("NET_AMOUNT", net);
		if (kiranaId.equals(""))
			val.put("CUST_ID", "10016800019");
		else
			val.put("CUST_ID", kiranaId);
		val.put("CURRENT_DUE", "0");
		val.put("CUST_TYPE", custType);
		val.put("EMP_ID", "1");
		val.put("RECONCILE_ID", "0");
		val.put("VAT_AMT", Float.toString(totalVat));
		val.put("ROUTE_CODE", code);
		val.put("ROUTE_STOP", stop);

		val.put("RETAILER_ID", retailerId);
		val.put("TERMINAL_ID", "0");
		val.put("PROMO_ID", "0");
		val.put("PROMO_TYPE", "0");
		try {
			database.insert("ORDER_HEADER", null, val);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void insertDiscountDetails(int i, String[] a2, String ord_disc) {

		ContentValues values = new ContentValues();
		values.put("ORDER_ID", Integer.toString(i));
		values.put("DISCOUNT_AMT", ord_disc);
		values.put("SALE_TYPE", "PLAN_H");
		values.put("BALANCE", a2[4]);
		values.put("PERCENTAGE", a2[1]);
		database.insert("DISCOUNT_DETAILS", null, values);
	}

	// GRN & reverse Grn additional Functionality Starts
	public int getMaxId() {
		int id = 0;
		Cursor cursor=null;
		try {
			String sql = "select max(id) from stock_header";
			cursor = database.rawQuery(sql, null);
			cursor.moveToFirst();
			id = cursor.getInt(0);
			id = id + 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			if(cursor!=null) cursor.close();
		}
		return id;
	}

	public ContentValues addStockItems(String pr_id, String qty, int id,
			String avail, String type) {
		ContentValues values = new ContentValues();
		values.put("id", id);
		values.put("avail", avail);
		values.put("product_id", pr_id);

		if (type.equals("custSale") || type.equals("kiranaSale")
				|| type.equals("reverseGrn"))
			values.put("qty", "-" + qty);
		else if (type.equals("grn"))
			values.put("qty", qty);
		return values;
	}

	public int addStockTrailer(ArrayList<ContentValues> list) {
		int j = 0;
		try {
			for (int i = 0; i < list.size(); i++) {
				int qty = list.get(i).getAsInteger("avail");
				list.get(i).remove("avail");
				System.out.println("value is :::" + list.get(i).toString());
				database.insert("Stock_Trailer", null, list.get(i));
				updateStockInventory(list.get(i), qty);
			}
			j = 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return j;
	}

	private void updateStockInventory(ContentValues contentValues, int qty) {

		contentValues.put("opening", qty);
		contentValues.put("closing", qty + contentValues.getAsInteger("qty"));
		contentValues.put("sync_status", 0);
		contentValues.remove("id");
		contentValues.remove("qty");
		try {
			// database.delete("Stock_Inventory",
			// "product_id="+contentValues.getAsInteger("product_id").intValue(),
			// null);
			// database.insert("stock_Inventory", null, contentValues);
			database.update("STOCK_INVENTORY", contentValues, "product_id="
					+ contentValues.getAsInteger("product_id")
					+ " and txn_date='" + onlyDate.format(cal.getTime()) + "'",
					null);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addStockHeader(int size, int totalQty, int id, String text,
			String order_id, String type) {
		ContentValues values = new ContentValues();
		values.put("id", id);
		values.put("txn_date", date.format(cal.getTime()));
		values.put("line_items", size);
		values.put("total_qty", totalQty);
		values.put("status", 0);
		values.put("sync_status", 0);

        switch (type) {
            case "custSale":
            case "kiranaSale":
                values.put("order_type", db.OrderType("SALES"));
                break;
            case "grn":
                values.put("order_type", db.OrderType("GRN"));
                break;
            case "Rgrn":
                values.put("order_type", db.OrderType("REVERSE GRN"));
                break;
            default:
                values.put("order_type", "1");
                break;
        }

		if (text == null || text.equals("") )
			values.put("ext_ref_id", "");
		else
			values.put("ext_ref_id", Integer.parseInt(text));

		if (order_id == null || order_id.equals(""))
			values.put("ref_order_id", "");
		else
			values.put("ref_order_id", Integer.parseInt(order_id));
		try {
			database.insert("Stock_Header", null, values);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    public void addOrdersInProcess(String[] item, int i, String type) {
        // TODO Auto-generated method stub

        ContentValues values = new ContentValues();
        values.put("ID", item[0]);
        values.put("QTY", item[4]);
        values.put("AMOUNT", item[9]);
        values.put("VAT", "0");

        database.insert("ORDERS_IN_PROCESS", null, values);
    }

	public void deleteProcessedOrders(String pr_id) {
		// TODO Auto-generated method stub
		if(pr_id.equals(""))
			database.delete("ORDERS_IN_PROCESS", null, null);
		else
			database.delete("ORDERS_IN_PROCESS", "PRODUCT_ID='"+pr_id+"'", null);
	}

	public Cursor getOrdersinProcess() {
		// TODO Auto-generated method stub
		Cursor cr = database.rawQuery("select (select p.NAME from PRODUCT_DETAILS p where p.ID=PRODUCT_ID) as name," +
				" ID as _id, *, (select s.closing from STOCK_INVENTORY s where s.product_id=PRODUCT_ID and txn_date=date('now')) " +
				"as AVAIL from ORDERS_IN_PROCESS", null);
		return cr;
	}

	public void changeQty(String pr_id, String required, String price, String vat) {
		// TODO Auto-generated method stub
        BigDecimal amount = new BigDecimal(price).multiply(new BigDecimal(required));

        ContentValues values = new ContentValues();
        values.put("AMOUNT", amount.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());
        values.put("QTY", required);
        database.update("ORDERS_IN_PROCESS", values, "ID='" + pr_id + "'", null);
    }

	public Cursor checkCart(String pr_id) {
		// TODO Auto-generated method stub
        String sql = "select * from ORDERS_IN_PROCESS where id='" + pr_id + "'";
        Cursor cr = database.rawQuery(sql, null);
		return cr;
	}

	public CharSequence getProcessedOrdersTotal() {
		// TODO Auto-generated method stub
		String sql = "select sum(AMOUNT) from ORDERS_IN_PROCESS";
        CharSequence total = "0.0";
        try (Cursor cr = database.rawQuery(sql, null)) {
            cr.moveToFirst();
            total = String.format("%.2f", Float.parseFloat(cr.getString(0)));
        } catch (Exception e) {
            e.printStackTrace();
        }
		return total;
	}

	public CharSequence getState(String code) {
		// TODO Auto-generated method stub
		String id = null;

        String sql = "select DESCRIPTION from state_master where CODE='" + code + "'";
        try (Cursor cursor = database.rawQuery(sql, null)) {
            cursor.moveToFirst();
            id = cursor.getString(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
		return id;
	}

	public CharSequence getLoc(String stop) {
		String id = null;
        String sql = "select NAME from WAREHOUSE_LOCATIONS where WH_CODE='"+stop+"'";
        try (Cursor cursor = database.rawQuery(sql, null)) {
            cursor.moveToFirst();
            id = cursor.getString(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
	}
}

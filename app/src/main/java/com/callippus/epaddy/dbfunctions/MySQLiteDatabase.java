package com.callippus.epaddy.dbfunctions;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class MySQLiteDatabase {

	SQLiteDatabase database;
	SQLiteDatabase getWritableDatabase(){
		
		return database;
		
	}
	int update(String table, ContentValues values, String whereClause, String[] whereArgs)
	{
		Log.d("Update query","Table:"+table+"\nValues:"+values.toString()+"\nwhereClause:"+whereClause+"\nwhereArgs"+whereArgs.toString());
		return database.update(table, values, whereClause, whereArgs);
	}
	
}

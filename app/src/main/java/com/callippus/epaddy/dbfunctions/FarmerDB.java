package com.callippus.epaddy.dbfunctions;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.callippus.epaddy.resources.DBConnection;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by callippus on 02-02-2018.
 */

public class FarmerDB  extends DBConnection {

    public static final String TAG="FARMERDB";
    public static final  int SUCCESS=0;
    public static final  int ERROR=-1;
    DBConnection db = null;
    SQLiteDatabase database;
    SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss",
            Locale.US);
    SimpleDateFormat onlyDate = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
    Calendar cal = Calendar.getInstance();

    public FarmerDB(Context context) throws IOException {
        super(context);
        try {
            db = new DBConnection(context);
            database = db.myDataBase;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public int insert_tableData(ArrayList<ContentValues> list,String table_name) {
        try {
            for (int i = 0; i < list.size(); i++) {
                System.out.println("value is :::" + list.get(i).toString());
                long ret=database.insert(table_name, null, list.get(i));
                if(ret > 0)
                    return SUCCESS;
                else
                    return ERROR;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ERROR;
        }

        return ERROR;

    }

    public  int isValidCode(String farmercode)
        {
            int resp=-1;
            int count = 0;
            StringBuilder sb=new StringBuilder();

           /* sb.append("SELECT COUNT(*) TOTAL FROM FARMER_DETAILS WHERE FARMER_CODE =").append(farmercode);*/
            sb.append("SELECT COUNT(*) TOTAL FROM FARMER_DETAILS WHERE FARMER_CODE =?");

                Log.d(TAG,"The query in isValidCode() :: "+sb.toString());
                try {
                    Cursor cr = myDataBase.rawQuery(sb.toString(),new String[]{farmercode});
                cr.moveToFirst();
                count = cr.getInt(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //int cnt =count.length(); /*Integer.parseInt(count.replaceAll("[\\D]", ""));*/
            if(count > 0) {
                return  0;
            }
            else
            {
                return resp;
            }

        }


    public  ArrayList<String> populateFarmerDetails(String code)
    {
        ArrayList<String> wordList = new ArrayList<>();
        Cursor cursor = null;
        try {
            StringBuilder sb=new StringBuilder();

         /* sb.append("SELECT * FROM FARMER_DETAILS WHERE FARMER_CODE =").append(code);

            Log.d(TAG,"The query in populateFarmerDetails() :: "+sb.toString());
            cursor = database.rawQuery(sb.toString(), null);*/
            sb.append("SELECT FARMER_CODE,FIRST_NAME,LAST_NAME,FATHERS_NAME,PRESENT_ADDRESS,PERMANENT_ADDRESS,TOWN,DISTRICT,PIN_CODE,BLOCK_CODE,DISTRICT_CODE,DOB,MOBILE_NO,ALT_MOBILE_NO,BANK_ACCOUNT_NO,IFSC,BANK_NAME,AADHAAR_NO,VERSION,INSERTED_DATE FROM FARMER_DETAILS WHERE FARMER_CODE =?");
            Log.d(TAG,"The query in populateFarmerDetails() :: "+sb.toString());
            cursor = database.rawQuery(sb.toString(), new String[]{code});
            Log.d(TAG,"The query in populateFarmerDetails() :: "+database.rawQuery(sb.toString(), new String[]{code}));

            cursor.moveToFirst();
            if (cursor != null) {
                do {
                    for (int i = 0; i < cursor.getColumnCount(); i++) {

                        Log.e("", "The col value ::->" + cursor.getString(i));
                        wordList.add(cursor.getString(i));
                    }
                }while (cursor.moveToNext());
            }
            else
            {
                Log.d(TAG, "No Value :found in list.... ");
            }
            cursor.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return wordList;

    }




    // update famer details
    public String updateFarmer(ArrayList<ContentValues> list,String fcode) {
        try {
            for (int i = 0; i < list.size(); i++) {
                StringBuilder sb=new StringBuilder();
                sb.append("FARMER_CODE=?");
                System.out.println("value is :::" + list.get(i).toString());
                long ret=database.update("FARMER_DETAILS",list.get(i),sb.toString(),new String[]{fcode});
                if(ret > 0)
                    return "Success";
                else
                    return "001_UPDATE_FARMER_DEATILS";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Failure";
        }

        return "Error";

    }


    // delete farmer detailas
    public String deleteFarmer(String fcode) {
        try {
            StringBuilder sb=new StringBuilder();
            sb.append("FARMER_CODE=?");
            long ret=database.delete("FARMER_DETAILS",sb.toString(),new String[]{fcode});
            if(ret > 0)
                return "Success";
            else
                return "001_DELETE_FARMER_DEATILS";
        } catch (Exception e) {
            e.printStackTrace();
            return "Failure";
        }

    }


    /* get Block codes*/

    public ArrayList<String> getBlockCodes()
    {
        ArrayList<String> blockcodelist = new ArrayList<>();
        Cursor cursor = null;
        try {
            StringBuilder sb=new StringBuilder();
            sb.append("SELECT BLOCK_CODE FROM BLOCK_MASTER");
            cursor = database.rawQuery(sb.toString(), null);
            cursor.moveToFirst();
            if (cursor != null) {
                do {
                    for (int i = 0; i < cursor.getColumnCount(); i++) {
                        blockcodelist.add(cursor.getString(i));
                    }
                }while (cursor.moveToNext());
            }
            else
            {
                Log.d(TAG, "No Value :found in list.... ");
            }
            cursor.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return blockcodelist;
    }

    /* district codes*/

    public ArrayList<String> getDistrictCodes()
    {
        ArrayList<String> districtcodelist = new ArrayList<>();
        Cursor cursor = null;
        try {
            StringBuilder sb=new StringBuilder();
            sb.append("SELECT ID FROM DISTRICT_MASTER");
            cursor = database.rawQuery(sb.toString(), null);
            cursor.moveToFirst();
            if (cursor != null) {
                do {
                    for (int i = 0; i < cursor.getColumnCount(); i++) {
                        districtcodelist.add(cursor.getString(i));
                    }
                }while (cursor.moveToNext());
            }
            else
            {
                Log.d(TAG, "No Value :found in list.... ");
            }
            cursor.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return districtcodelist;
    }

    public String get_configuration(String key)
    {
        String id = null;
        String sql = "select VALUE from CONFIGURATION WHERE KEY='" + key + "'";

        Cursor cursor = null;
        try {
            cursor = database.rawQuery(sql, null);
            cursor.moveToFirst();
            id = cursor.getString(0);
        } catch (Exception e) {

        } finally {
            if (cursor != null)
                cursor.close();
        }
        return id;
    }

    public void setConfig(String key, String value) {
        String sql = "update configuration set value='" + value + "' where key='" + key + "'";
        database.execSQL(sql);
    }

    public void insert_StockHeader(int size, int totalQty, int id, String text,
                               String order_id, String type) {
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("txn_date", date.format(cal.getTime()));
        values.put("line_items", size);
        values.put("total_qty", totalQty);
        values.put("status", 0);
        values.put("sync_status", 0);

        switch (type) {
            case "custSale":
            case "kiranaSale":
                values.put("order_type", db.OrderType("SALES"));
                break;
            case "grn":
                values.put("order_type", db.OrderType("GRN"));
                break;
            case "Rgrn":
                values.put("order_type", db.OrderType("REVERSE GRN"));
                break;
            default:
                values.put("order_type", "1");
                break;
        }

        if (text == null || text.equals("") )
            values.put("ext_ref_id", "");
        else
            values.put("ext_ref_id", Integer.parseInt(text));

        if (order_id == null || order_id.equals(""))
            values.put("ref_order_id", "");
        else
            values.put("ref_order_id", Integer.parseInt(order_id));
        try {
            database.insert("Stock_Header", null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public ArrayList<String> getgunny_types()
    {
        ArrayList<String> list = new ArrayList<>();
        Cursor cursor = null;
        try {
            String sql = "SELECT GUNNY_TYPE FROM GUNNY_TYPE_DETAILS";
            cursor = database.rawQuery(sql, null);
            cursor.moveToFirst();
            if (cursor != null) {
                do {
                    for (int i = 0; i < cursor.getColumnCount(); i++) {
                        list.add(cursor.getString(i));
                    }
                }while (cursor.moveToNext());
            }
            else
            {
                Log.d(TAG, "No Value :found in list.... ");
            }
            cursor.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public  int isValidTruckId(String id)
    {
        int resp=-1;
        int count = 0;


        String query = "SELECT COUNT(*) TOTAL FROM VEHICLE_DETAILS WHERE VEHICLE_NUMBER ='"+id+"'";

        Log.d(TAG,"The query in isValidTruckId() :: "+query);
        try {
            Cursor cr = myDataBase.rawQuery(query, null);
            cr.moveToFirst();
            count = cr.getInt(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(count > 0) {
            return  0;
        }
        else
        {
            return resp;
        }

    }

    public int delete_table_data(String table_name)
    {
       int resp= database.delete(table_name,null,null);
       return resp;
    }

    public long
    insert_stockinprocess(ContentValues values)
    {
        long ret=database.insert("STOCK_IN_PROCESS", null, values);

        return ret;
    }

    public  void update_stock_in_process(String id,double qty,int bag_count,String truck_id,String bag_weight,String moisture)
    {

        String sql = "UPDATE STOCK_IN_PROCESS SET TOTAL_QTY ='"+qty+"',BAG_COUNT="+bag_count+",TRUCK_MEMO_ID ='"+truck_id+"',BAG_WEIGHT ='"+bag_weight+"',MOISTURE ='"+moisture+"' WHERE ITEM_ID='"+id+"' ";
        database.execSQL(sql);

    }

    public int check_rowexists(String id)
    {
        int count=-1;
        String query = "SELECT COUNT(*) TOTAL FROM STOCK_IN_PROCESS WHERE ITEM_ID ='"+id+"' AND TXN_DATE = DATE('NOW','LOCALTIME')";

        Log.d(TAG,"The query in isValidTruckId() :: "+query);
        try {
            Cursor cr = myDataBase.rawQuery(query, null);
            cr.moveToFirst();
            count = cr.getInt(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG,"Id Count in Stock In Process :: "+count+" where  Id :: "+id);
        return  count;
    }

    public int check_date_exists()
    {
        int count=-1;
        String query = "SELECT COUNT(*) TOTAL FROM STOCK_INVENTORY WHERE TXN_DATE = DATE('NOW','LOCALTIME')";

        Log.d(TAG,"The query in check_date_exists() :: "+query);
        try {
            Cursor cr = myDataBase.rawQuery(query, null);
            cr.moveToFirst();
            count = cr.getInt(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG,"rows Count in Stock Inventory :: "+count);
        return  count;
    }

    public int check_gunny_exists()
    {
        int count = -1;
        String query = "SELECT COUNT(*) TOTAL FROM GUNNY_INVENTORY WHERE DATE = DATE('NOW','LOCALTIME')";

        Log.d(TAG,"The query in check_gunny_exists() :: "+query);
        try {
            Cursor cr = myDataBase.rawQuery(query, null);
            cr.moveToFirst();
            count = cr.getInt(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG,"rows Count in Gunny_Inventory() :: "+count);
        return  count;
    }



    /*selecting open,close from gunnty inventory based on gunny_type,date*/


    public ArrayList<String>  getGunnyInventoryOpenCloseDetails(String gunny_type)
    {
        ArrayList<String> list = new ArrayList<>();
        Cursor cursor = null;
        try {
            String sql = "SELECT DATE,GUNNY_OPENING,GUNNY_CLOSING FROM GUNNY_INVENTORY where GUNNY_ID=? ORDER BY DATE DESC limit 1";
            cursor = database.rawQuery(sql, new String[]{gunny_type});
            cursor.moveToFirst();
            if (cursor != null) {
                do {
                    for (int i = 0; i < cursor.getColumnCount(); i++) {
                        list.add(cursor.getString(i));
                    }
                }while (cursor.moveToNext());
            }
            else
            {
                Log.d(TAG, "No Value :found in list.... ");
            }
            cursor.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    /* update gunny inventory*/
    public  int updateGunnyInventory(String gunny_type,String gunnyclosevalue)
    {
        // TODO Auto-generated method stub
        ContentValues v = new ContentValues();
        v.put("GUNNY_CLOSING",gunnyclosevalue );
        int res=database.update("GUNNY_INVENTORY", v, "GUNNY_ID='" + gunny_type + "'", null);
        if(res > 0){
            return  SUCCESS;
        }
        else
        {
            return ERROR;
        }

    }



    /*get paddy category*/


    public ArrayList<String> getPaddyCategory()
    {
        ArrayList<String> paddycategory = new ArrayList<>();
        Cursor cursor = null;
        try {
            StringBuilder sb=new StringBuilder();
            sb.append("SELECT NAME_EN FROM PRODUCT_DETAILS");
            cursor = database.rawQuery(sb.toString(), null);
            cursor.moveToFirst();
            if (cursor != null) {
                do {
                    for (int i = 0; i < cursor.getColumnCount(); i++) {
                        paddycategory.add(cursor.getString(i));
                    }
                }while (cursor.moveToNext());
            }
            else
            {
                Log.d(TAG, "No Value :found in list.... ");
            }
            cursor.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return paddycategory;
    }
}

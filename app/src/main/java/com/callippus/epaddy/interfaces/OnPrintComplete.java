package com.callippus.epaddy.interfaces;
/**
 * Created by srini_000 on 02-07-2015.
 */
public interface OnPrintComplete {
    void onPrintComplete();
}

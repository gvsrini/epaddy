package com.callippus.epaddy.interfaces;

import android.widget.AdapterView;

/**
 * Created by Lenovo on 16-05-2015.
 */
public interface OnOrderEvents {
    void onGetDistributionCompleted();

    void onItemSelected(AdapterView<?> view, String[] product, int i);

    void onGetAudioFilesCompleted();
}

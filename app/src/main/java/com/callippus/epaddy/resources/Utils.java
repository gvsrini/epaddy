package com.callippus.epaddy.resources;

import android.media.MediaPlayer;
import android.util.Log;

import com.callippus.epaddy.saleitems.Constants;
import com.callippus.epaddy.saleitems.Preferences;

import java.util.ArrayList;

/**
 * Created by srini_000 on 27-05-2015.
 */
public class Utils implements MediaPlayer.OnCompletionListener {
    ArrayList<String> audio_files = new ArrayList<String>();
    String[] audioFileArray = null;
    int audioIndex = 0;
    MediaPlayer mp = null;

    public void playMP3(String s) {

        if (Preferences.audioPref) {
            try {
                Log.d(Constants.TAG, "This is the s value:" + s);

                String str = DBConnection.APP_PATH + "/audio/" + s;
                mp = new MediaPlayer();
                mp.setOnCompletionListener(this);
                audioIndex++;
                audioFileArray = new String[1];
                audioFileArray[0] = s;
                mp.setDataSource(str);
                mp.prepare();
                mp.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void playMP3() {
        if (Preferences.audioPref) {
            try {
                mp = new MediaPlayer();
                mp.setOnCompletionListener(this);
                String str = DBConnection.APP_PATH + "/audio/" + audioFileArray[audioIndex++];

                mp.setDataSource(str);

                mp.prepare();
                mp.start();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void playTotal(String value, int suffix) {
        float f;
        int r;
        char t[] = new char[10];
        float paise;
        float grams;
        char str[] = new char[100];

        audio_files.clear();

        if (suffix == 0) {
            f = Float.parseFloat(value);
            r = (int) Math.floor(Double.parseDouble(value));

            if (f >= 1.0f)
                playSpecific(value, 0);
        } else if (suffix == 1) // Break into Rupees and Paise and play
        // separately
        {
            f = Float.parseFloat(value);
            r = (int) Math.floor(Double.parseDouble(value));

            if (f >= 1.0f)
                playSpecific(value, 1);

            paise = _round((f - r) * 100.0f);

            if (paise > 0.0f) {
                playSpecific("" + paise, 2);
            }
        } else if (suffix == 2) // Break into KGs and Grams and play separately
        {
            f = Float.parseFloat(value);
            r = (int) Math.floor(Double.parseDouble(value));

            if (f >= 1.0f)
                playSpecific(value, 3);

            grams = _round((f - r) * 1000.0f);

            if (grams > 0.0f) {
                playSpecific("" + grams, 4);
            }
        } else if (suffix == 3) // Break into Lts and ml and play separately
        {
            f = Float.parseFloat(value);
            r = (int) Math.floor(Double.parseDouble(value));

            if (f >= 1.0f)
                playSpecific(value, 5);

            grams = _round((f - r) * 1000.0f);

            if (grams > 0.0f) {
                playSpecific("" + grams, 6);
            }
        }
        audioFileArray = audio_files.toArray(new String[audio_files.size()]);
        playMP3();
    }

    float _round(float n) {
        double rem;
        double frem;
        double man;

        man = Math.floor(n);
        frem = n - (float) man + 0.125f;
        rem = Math.floor(frem * 4.0f) / 4.0;

        return (float) (man + rem);
    }

    void play(String str) {
        // Play files here.
        audio_files.add(str);

    }

    int playSpecific(String str, int suffix) {
        int i;
        int a;
        int m = 1;
        int cnt = 0;
        int cnt2 = 0;
        int j = 0;
        int flag = 1;
        int res[] = new int[10];
        int res2[] = new int[10];

        i = (int) Math.floor(Double.parseDouble(str));
        if (i >= 0 && i <= 100) {
            play(i + "h.mp3");
            if (suffix == 1)
                play("rupaye.wav");
            else if (suffix == 2)
                play("paise.wav");
            else if (suffix == 3)
                play("kg.wav");
            else if (suffix == 4)
                play("gram.wav");
            else if (suffix == 5)
                play("litre.wav");
            else if (suffix == 6)
                play("ml.wav");

            return 0;
        }
        // ----------split the number-----
        while (i > 0) {
            a = i % 10;
            i = i / 10;
            a = a * m;
            m = m * 10;
            res[cnt++] = a;
            j++;
        }
        cnt2 = 0;
        cnt = 0;
        while (cnt < j) {
            cnt++;
        }
        cnt = 0;

        while (cnt < j) {
            switch (cnt) {
                case 0:
                    if (res[cnt + 1] == 0) {
                        res2[cnt2] = res[cnt];
                    }
                    break;

                case 1:
                    if (res[cnt] > 0) {
                        res2[cnt2] = res[cnt] + res[cnt - 1];
                    }
                    break;
                case 2:
                    if (res[cnt] > 0) {
                        res2[cnt2] = 100;
                        cnt2++;
                        res2[cnt2] = res[cnt] / 100;
                    }
                    break;
                case 3:
                    if (res[cnt] > 0) {
                        flag = 0;
                        res2[cnt2] = 1000;
                    }
                    if (res[cnt + 1] == 0) {
                        cnt2++;
                        res2[cnt2] = res[cnt] / 1000;
                    }
                    break;
                case 4:
                /*
                 * if(res[cnt]<10) { res2[cnt2] = res[cnt]/1000; //cnt2++; }
				 * else
				 */
                    if (res[cnt] > 0) {
                        if (flag == 1) {
                            res2[cnt2] = 1000;
                            cnt2++;
                        }
                        res2[cnt2] = (res[cnt] + res[cnt - 1]) / 1000;
                    }
                    break;
                case 5:
				/*
				 * if(res[cnt]>0) { res2[cnt2] = 100000; cnt2++; res2[cnt2] =
				 * res[cnt]/100000; }
				 */
                    if (res[cnt] > 0) {
                        res2[cnt2] = 100000;
                        // cnt2++;
                        // res2[cnt2] = res[cnt]/100000;
                    }
                    if (res[cnt + 1] >= 1000000) {
                        res2[cnt2] = 100000;
                        cnt2++;
                        res2[cnt2] = (res[cnt] + res[cnt + 1]) / 100000;
                    } else {
                        cnt2++;
                        res2[cnt2] = res[cnt] / 100000;
                    }

                    break;

            }
            if (cnt > 0)
                cnt2++;
            cnt++;
        }
        j = cnt2;
        cnt2 = 0;
        cnt = 0;
        while (cnt2 < j) {
            if (res2[cnt2] != 0) {
                res[cnt] = res2[cnt2];
                cnt++;
            }
            cnt2++;
        }

        i = 0;
        while (cnt > 0) {
            cnt--;
            play(res[cnt] + "h.mp3");
            i++;
        }

        i = 0;
        while (res[i] != '\0') {
            // play(wave[i]);
            // sleep(4);
            i++;
        }
        if (suffix == 1)
            play("rupaye.wav");
        else if (suffix == 2)
            play("paise.wav");
        else if (suffix == 3)
            play("kg.wav");
        else if (suffix == 4)
            play("gram.wav");
        else if (suffix == 5)
            play("litre.wav");
        else if (suffix == 6)
            play("ml.wav");

        return 0;

    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.i(Constants.TAG, "onComplete hit:audioIndex: " + audioIndex);
        Log.i(Constants.TAG, "onComplete hit:audioFileArray.length: " + audioFileArray.length);
        mp.stop();
        mp.release();
        if (audioIndex < audioFileArray.length)
            playMP3();
        else {
            audioIndex = 0;
            audioFileArray = null;
            return;
        }
    }
}

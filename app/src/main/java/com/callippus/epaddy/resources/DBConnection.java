package com.callippus.epaddy.resources;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import com.callippus.epaddy.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;


public class DBConnection extends SQLiteOpenHelper {

    public static SQLiteDatabase myDataBase;
    public static final  String TAG="DBConnection ";
    public static String DB_NAME = "fps.sqlite";
    public static String APP_PATH = Environment.getExternalStorageDirectory().getPath() + "/data/com.callippus.epaddy";
    public static String DB_DIR = APP_PATH + "/databases";
    public static String DB_PATH = DB_DIR + "/" + DB_NAME;
    public static String WELCOME_MP3 = APP_PATH + "/welcome.mp3";
    public static String CERT_FILE = "uidai_auth_stage.cer";
    public static String CERT_PATH = APP_PATH + "/" + CERT_FILE;
    public String VersionNo = "V 1.0.1";
    public   String App_version="" ;
    private Context mycontext;

    public DBConnection(Context context) throws IOException {
        super(context, DB_NAME, null, 1);
        this.mycontext = context;

        Log.d("CALLIPPUS1", "Reached here");
        //added anil
        String path=mycontext.getApplicationInfo().dataDir + "/databases/" + DB_NAME;
        boolean dbexist = checkFile(path);

       /* boolean dbexist = checkFile(DB_PATH);*/

        if (dbexist) {
            Log.d("CALLIPPUS1", "Database exists");
            opendatabase();
        } else {
            Log.d("CALLIPPUS1", "Database doesn't exist");
            createdatabase();
            opendatabase();
        }

        if (checkFile(CERT_PATH)) {
            Log.d("CALLIPPUS1", "Certificate exists");
            opendatabase();
        } else {
            Log.d("CALLIPPUS1", "Certificate doesn't exist");
            createCertificate();
        }
        if (checkFile(WELCOME_MP3)) {
            Log.d("CALLIPPUS1", "Welcome Mp3 exists");
            opendatabase();
        } else {
            Log.d("CALLIPPUS1", "Welcome Mp3 doesn't exist");
            createWelcomeMp3();
        }

        copyfile(DB_DIR, "CAPTURE_ISO");

    }

    // Check database ifexists or else create database
    public void createdatabase() throws IOException {
        boolean dbexist = checkFile(DB_PATH);
        SQLiteDatabase db_Read;
        if (dbexist) {
            Log.d("CALLIPPUS1", " Database exists.");
        } else {
            // this.getReadableDatabase();
            db_Read = this.getReadableDatabase();
            db_Read.close();
            try {
                Log.d("CALLIPPUS1", "Copying DB");
                copyfile(DB_DIR,DB_NAME);
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    public void createCertificate() throws IOException {
        boolean dbexist = checkFile(CERT_PATH);
        if (dbexist) {
            Log.d("CALLIPPUS1", "Certificate exists.");
        } else {
            try {
                Log.d("CALLIPPUS1", "Copying Certificate");
                copyfile(APP_PATH, CERT_FILE);
            } catch (IOException e) {
                throw new Error("Error copying certificate");
            }
        }
    }

    public void createWelcomeMp3() throws IOException {
        boolean dbexist = checkFile(WELCOME_MP3);
        if (dbexist) {
            Log.d("CALLIPPUS1", "Welcome Mp3 exists.");
        } else {
            try {
                Log.d("CALLIPPUS1", "Copying Welcome Mp3");
                copyfile(APP_PATH, "welcome.mp3");
            } catch (IOException e) {
                throw new Error("Error copying Welcome Mp3");
            }
        }
    }

    private boolean checkFile(String filePath) {
        // SQLiteDatabase checkdb = null;
        boolean checkdb = false;
        try {
            String myPath = filePath;
            File dbfile = new File(myPath);
            checkdb = dbfile.exists();
        } catch (SQLiteException e) {
            Log.d("CALLIPPUS1", "Database doesn't exist");
        }

        return checkdb;
    }

    private void copyfile(String dir, String fileName) throws IOException {

        try {
            // Open your local db as the input stream
            InputStream myinput = mycontext.getAssets().open(fileName);

            File file = new File(dir);
            file.mkdirs();

            // Open the empty db as the output stream
            OutputStream myoutput = new FileOutputStream(mycontext.getApplicationInfo().dataDir + "/databases/" + fileName);

            // transfer byte to inputfile to outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myinput.read(buffer)) > 0) {
                myoutput.write(buffer, 0, length);
            }

            // Close the streams
            myoutput.flush();
            myoutput.close();
            myinput.close();

            Log.d("CALLIPPUS", "File copied from assets to :" + dir + "/" + fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // //End of Creating DB

    // Abstract method
    public void onCreate(SQLiteDatabase db) {

        // TODO Auto-generated method stub

    }

    // Abstact Method
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

    }

    // Open DB Connection
    public void opendatabase() throws SQLException {
        // Open the database

        if (myDataBase != null && myDataBase.isOpen())
            return;

        String mypath = DB_PATH;
        String path=mycontext.getApplicationInfo().dataDir + "/databases/" + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(mycontext.getApplicationInfo().dataDir + "/databases/" + DB_NAME, null,
                SQLiteDatabase.OPEN_READWRITE);

    }

    // Close DB Connection
    public synchronized void close() {
        if (myDataBase != null) {
            myDataBase.close();
        }
        super.close();
    }

    public String fromConfiguration(String key) {
        String value = null;
        Cursor cursor = null;
        try {
            String sql = "select value from CONFIGURATION where KEY='" + key + "' group by office_id";
            cursor = myDataBase.rawQuery(sql, null);
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return value;
    }

    public String fromProperties(String key) {
        String value = null;
        Cursor cursor = null;
        try {
            String sql = "select value from PROPERTIES where KEY='" + key + "' group by office_id";
            cursor = myDataBase.rawQuery(sql, null);
            cursor.moveToFirst();
            value = cursor.getString(0);
        } catch (Exception e) {
            value = "-1";
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return value;
    }

    public String OrderType(String key) {
        String value = null;
        Cursor cursor = null;
        try {
            String sql = "select id from STOCK_ORDER_TYPE where desc='" + key + "'";
            cursor = myDataBase.rawQuery(sql, null);
            cursor.moveToFirst();
            value = cursor.getString(0);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }


        return value;
    }

    //GPRS
    public Cursor GetStatusOfGprs() {
        String selectQuery = "select  value from properties where key='background'";
        return myDataBase.rawQuery(selectQuery, null);
    }

    public void insertProperties(EditText set2, String string) {
        // TODO Auto-generated method stub
        ContentValues v1 = new ContentValues();
        v1.put("VALUE", set2.getText().toString());
        myDataBase.update("PROPERTIES", v1, "KEY='" + string + "'", null);
    }


    public void insertProperties(String key, String value) {
        // TODO Auto-generated method stub
        ContentValues v1 = new ContentValues();
        v1.put("VALUE", value);
        myDataBase.update("PROPERTIES", v1, "KEY='" + key + "'", null);
    }

    public ArrayList<String> getCategories() {
        ArrayList<String> wordList = new ArrayList<>();
        Cursor cursor = null;
        try {
            String sql = "select CATEGORY_NAME, CATEGORY_ID from CATEGORY_TYPE ";
            cursor = myDataBase.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                wordList.add(cursor.getString(0) + "-" + cursor.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }
        return wordList;
    }

    public ArrayList<String> getBrands(String category) {
        ArrayList<String> wordList = new ArrayList<>();
        Cursor cursor = null;
        try {
            String sql = "select BRAND_NAME from BRAND_MASTER where  CATEGORY_ID = " + category;
            cursor = myDataBase.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                wordList.add(cursor.getString(0));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }
        return wordList;
    }

    public ArrayList<String[]> getProducts(String brand, String name, String type) {
        ArrayList<String[]> wordList = new ArrayList<>();
        String val = null;

        if (type.equals("custSale") || type.equals("grn")
                || type.equals("reverseGrn"))
            val = fromProperties("default_price_list");
        else if (type.equals("kiranaSale"))
            val = fromProperties("kirana_pricelist_id");
        StringBuilder sb = new StringBuilder();
        Cursor cursor = null;
        try {
            sb.append("SELECT NAME,pd.ID, pp.LOWER_LIMIT,VAT,VAT_INCLUDED, BASE_PRICE FROM  (select * FROM PRODUCT_DETAILS where price_list_id='").append(val).append("' AND ").append("(NAME LIKE '%").append(name).append("%' OR OPTIMAL_LEVEL like '%").append(name).append("%')");

            if (brand != null && !brand.equals(""))
                sb.append(" and brand_id='").append(getBrandID(brand)).append("'");

            sb.append(" ) PD JOIN (select * from product_price where price_list_id='").append(val).append("') ").append("  pp on pd.id=pp.id order by NAME Desc");

			/*String sql = "SELECT NAME,pd.ID,pp.LOWER_LIMIT,VAT,VAT_INCLUDED, AVAIL,BASE_PRICE FROM "
                    + " (select * FROM PRODUCT_DETAILS where price_list_id='"+val+"' AND "
					+ "(NAME LIKE '%"+name+"%' OR OPTIMAL_LEVEL like '%"+name+"%')) PD JOIN "
					+ "(select * from product_price where price_list_id='"+val+"') pp join "
					+ "(select closing AVAIL,product_id from stock_inventory  where txn_date=date('now') and AVAIL>0)"
					+ " pi on pd.id=pp.id and pd.id=pi.product_id order by NAME Desc";*/

            cursor = myDataBase.rawQuery(sb.toString(), null);
            Log.e("Query", sb.toString());
            while (cursor.moveToNext()) {
                String[] obj = {cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5)};
                wordList.add(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }
        return wordList;
    }

    private String getBrandID(String brand) {
        // TODO Auto-generated method stub
        String sql = "select BRAND_ID from BRAND_MASTER where BRAND_NAME='" + brand + "'";
        Cursor cr = null;
        String id = null;
        try {
            cr = myDataBase.rawQuery(sql, null);
            cr.moveToFirst();
            id = cr.getString(0);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cr != null) cr.close();
        }
        return id;
    }

    public String getProductName(String promoProductId) {
        // TODO Auto-generated method stub
        String sql = "select NAME from PRODUCT_DETAILS where ID='"
                + promoProductId + "'";
        Cursor cr = null;
        String id = null;
        try {
            cr = myDataBase.rawQuery(sql, null);
            cr.moveToFirst();
            id = cr.getString(0);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cr != null) cr.close();
        }
        return id;
    }

    public String getProductId(String promoProductName) {
        // TODO Auto-generated method stub
        String sql = "select ID from PRODUCT_DETAILS where NAME='"
                + promoProductName + "'";
        Cursor cr = null;
        String id = null;
        try {
            cr = myDataBase.rawQuery(sql, null);
            cr.moveToFirst();
            id = cr.getString(0);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cr != null) cr.close();
        }
        return id;
    }
   /* public TableRow HeaderFooterRows(String[] a){

        LayoutInflater inflater1 = LayoutInflater.from(mycontext);
        TableRow row1 = (TableRow) inflater1.inflate(R.layout.report_row,null);
        while(row1.getChildCount()>a.length)
            row1.removeViewAt(row1.getChildCount()-1);
        for (int i=0; i<row1.getChildCount(); i++){

            TextView text = (TextView) row1.getChildAt(i);
            text.setText(a[i]);
        }
        return row1;
    }*/

    public Cursor getDailyReport(TextView fr, TextView to) {

//        String s = "select cust_type,sum(amount),sum(order_discount),sum(net_amount) from order_header where "+
//                " status=0 and date(order_date) between '"+fr.getText().toString()+"' and '"+to.getText().toString()+"' group by cust_type";
       // String s = "delete from order_trailer;delete from order_header;delete from orders_in_process;";
        String s = "select product_id,product_name,sum(quantity) qty, sum(actual_price) price  from order_trailer ot,order_header oh "+
                " where  date(order_date) between '"+fr.getText().toString()+"' and '"+to.getText().toString()+"' and oh.id=ot.order_id group by product_id";
        Cursor cr = myDataBase.rawQuery(s, null);
        return cr;
    }

    public String appVersion() {
        String version;

        version = "V 0.1.0";
        return version;
    }

    public String dbVersion() {
        String version;

        version = "V 0.1.0";
        return version;
    }

    //login validation

    public String fromUserDetails(String userName, String password) {
        String value = null;
        Cursor cursor = null;

        try {
            String sql = "select USER_ID from USER_DETAILS where USER_NAME='" + userName + "' AND PASSWORD='" +  password + "'";
            cursor = myDataBase.rawQuery(sql, null);
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return value;
    }

    public ArrayList poopulateTruckDetails(String truck_number){
        String value = null;
        Cursor cursor = null;
        ArrayList<String> wordList = new ArrayList<>();
        try {

            String sql = "SELECT VEHICLE_NUMBER,VEHICLE_TYPE,CONTRACTOR_NAME,DRIVER_NAME,DRIVER_MOBILE_NO FROM VEHICLE_DETAILS WHERE VEHICLE_NUMBER ='" +truck_number+"' ";

            Log.d(TAG,"The query in poopulateTruckDetails() :: "+sql);
            cursor = myDataBase.rawQuery(sql, null);

            cursor.moveToFirst();
            if (cursor != null) {
                do {
                    for (int i = 0; i < cursor.getColumnCount(); i++) {

                        Log.e("", "The col value ::->" + cursor.getString(i));
                        wordList.add(cursor.getString(i));
                    }
                }while (cursor.moveToNext());
            }
            else
            {
                Log.d(TAG, "No Value :found in list.... ");
            }
            cursor.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return wordList;

    }



    /*header footer for farmer details report*/

    public TableRow HeaderFooterRowsFarmerReport(String[] a) {

        LayoutInflater inflater1 = LayoutInflater.from(mycontext);
        TableRow row1 = (TableRow) inflater1.inflate(R.layout.farmer_report_row, null);
        while (row1.getChildCount() > a.length)
            row1.removeViewAt(row1.getChildCount() - 1);
        for (int i = 0; i < row1.getChildCount(); i++) {

            TextView text = (TextView) row1.getChildAt(i);
            text.setText(a[i]);
        }
        return row1;

    }


    public TableRow GunnySummaryTitleHead(String[] a) {

        LayoutInflater inflater1 = LayoutInflater.from(mycontext);
        TableRow row1 = (TableRow) inflater1.inflate(R.layout.activity_gunny_summary_report_rows, null);
        while (row1.getChildCount() > a.length)
            row1.removeViewAt(row1.getChildCount() - 1);
        for (int i = 0; i < row1.getChildCount(); i++) {

            TextView text = (TextView) row1.getChildAt(i);
            text.setText(a[i]);
        }
        return row1;

    }

    public String getPropValue(String key)
    {
        String value=null;

        StringBuilder sb = new StringBuilder();
       /* sb.append("select VALUE from CONFIGURATION WHERE KEY='").append(key).append("'");*/
        sb.append("SELECT VALUE FROM PROPERTIES  WHERE KEY=?");

        Cursor cursor = null;
        try {
            cursor = myDataBase.rawQuery(sb.toString(),new String[]{key});
            cursor.moveToFirst();
            value = cursor.getString(0);
        } catch (Exception e) {

        } finally {
            if (cursor != null)
                cursor.close();
        }

        return  value;

    }




    /*header footer for vehicle movement dailyreport*/

   public TableRow HeaderFooterRowsVehicleMovementDailyReport(String[] a) {

        LayoutInflater inflater1 = LayoutInflater.from(mycontext);
        TableRow row1 = (TableRow) inflater1.inflate(R.layout.vehicle_movement_daily_report_row, null);
        while (row1.getChildCount() > a.length)
            row1.removeViewAt(row1.getChildCount() - 1);
        for (int i = 0; i < row1.getChildCount(); i++) {

            TextView text = (TextView) row1.getChildAt(i);
            text.setText(a[i]);
        }
        return row1;

    }



     //*header footer for vehicle movement monthly report*//*

    public TableRow HeaderFooterRowsVehicleMovementMonthlyReport(String[] a) {

        LayoutInflater inflater1 = LayoutInflater.from(mycontext);
        TableRow row1 = (TableRow) inflater1.inflate(R.layout.vehicle_movement_monthly_report_row, null);
        while (row1.getChildCount() > a.length)
            row1.removeViewAt(row1.getChildCount() - 1);
        for (int i = 0; i < row1.getChildCount(); i++) {

            TextView text = (TextView) row1.getChildAt(i);
            text.setText(a[i]);
        }
        return row1;

    }

}
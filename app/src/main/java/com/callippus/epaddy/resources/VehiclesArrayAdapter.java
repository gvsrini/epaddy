package com.callippus.epaddy.resources;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.callippus.epaddy.R;
import java.util.ArrayList;


/**
 * Created by l_mah on 06-02-2018.
 */

public class VehiclesArrayAdapter extends ArrayAdapter<String[]> {
    private final Context context;
    private final ArrayList<String[]> arrayList;


    public VehiclesArrayAdapter(Context context, ArrayList<String[]> arrayList) {
        super(context, R.layout.rowlayout, arrayList);
        this.context = context;
        this.arrayList = arrayList;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vehicleView = inflater.inflate(R.layout.vehiclelayout, parent, false);
        TextView vehicle_id = (TextView) vehicleView.findViewById(R.id.tv_vehicle_id_VL);
        vehicle_id.setTextSize(TypedValue.COMPLEX_UNIT_DIP,30);
        TextView vehicle_no = (TextView) vehicleView.findViewById(R.id.tv_vehicle_no_VL);
        vehicle_no.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        TextView vehicle_type = (TextView) vehicleView.findViewById(R.id.tv_vehicle_type_VL);
        vehicle_type.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);


        //SELECT NAME, UID FROM MEMBERS_ONLINE
        vehicle_id.setText(arrayList.get(position)[0]);
        vehicle_no.setText(arrayList.get(position)[1]);
        vehicle_type.setText(arrayList.get(position)[2]);


        return vehicleView;
    }

}

package com.callippus.epaddy.resources;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.callippus.epaddy.R;

import java.math.BigDecimal;
import java.util.ArrayList;

public class ProductsArrayAdapter extends ArrayAdapter<String[]> {
    private final Context context;
    private final ArrayList<String[]> arrayList;

    public ProductsArrayAdapter(Context context, ArrayList<String[]> arrayList) {
        super(context, R.layout.rowlayout, arrayList);
        this.context = context;
        this.arrayList = arrayList;
    }

    //SELECT PRODUCT_ID,NAME,CARD_TYPE,ALLOTMENT_TYPE,QUANTITY,PRICE,UOM,MAX_QUANTITY,AVAILED_QTY,'0.00' AMT FROM DISTRIBUTION_ONLINE

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
        TextView productName = (TextView) rowView.findViewById(R.id.product_name);
        TextView productPrice = (TextView) rowView.findViewById(R.id.product_price);
        TextView max_qty = (TextView) rowView.findViewById(R.id.max_qty);
        TextView avail = (TextView) rowView.findViewById(R.id.avail);
        TextView qty = (TextView) rowView.findViewById(R.id.qty);
        TextView amt = (TextView) rowView.findViewById(R.id.amt);
        TextView productId = (TextView) rowView.findViewById(R.id.product_id);

        LinearLayout qtySegment = (LinearLayout) rowView.findViewById(R.id.qtySegment);
        LinearLayout inCartSegment = (LinearLayout) rowView.findViewById(R.id.inCart);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

        //SELECT PRODUCT_ID,NAME,CARD_TYPE,ALLOTMENT_TYPE,QUANTITY,PRICE,UOM,MAX_QUANTITY,AVAILED_QTY,'0.00' AMT FROM DISTRIBUTION_ONLINE

        productId.setText(arrayList.get(position)[0]);
        productName.setText(arrayList.get(position)[1]);
        productPrice.setText(arrayList.get(position)[5]);

        BigDecimal qtyLeft = new BigDecimal(arrayList.get(position)[7]).subtract(new BigDecimal(arrayList.get(position)[8]));
        max_qty.setText(qtyLeft.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());
        qty.setText(arrayList.get(position)[4]);
        avail.setText(arrayList.get(position)[8]);
        amt.setText("₹ " + arrayList.get(position)[9]);
        String id = productId.getText().toString();
        imageView.setImageResource(getFlagResource("pro_"+id));

        if (new BigDecimal(qty.getText().toString()).compareTo(new BigDecimal("0.00")) == 1) {
            qtySegment.setVisibility(View.VISIBLE);
            inCartSegment.setVisibility(View.VISIBLE);
        } else {
            qtySegment.setVisibility(View.GONE);
            inCartSegment.setVisibility(View.GONE);
        }

        return rowView;
    }
    public int getFlagResource(String name) {
        String package_name = context.getPackageName();
        int resId = getContext().getResources().getIdentifier(name, "drawable", package_name);
        return resId;
    }




}
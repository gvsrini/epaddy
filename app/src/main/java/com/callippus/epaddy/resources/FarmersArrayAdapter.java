package com.callippus.epaddy.resources;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.callippus.epaddy.R;

import java.util.ArrayList;


/**
 * Created by l_mah on 06-02-2018.
 */

public class FarmersArrayAdapter extends ArrayAdapter<String[]> {
    private final Context context;
    private final ArrayList<String[]> arrayList;


    public FarmersArrayAdapter(Context context, ArrayList<String[]> arrayList) {
        super(context, R.layout.rowlayout, arrayList);
        this.context = context;
        this.arrayList = arrayList;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View farmerView = inflater.inflate(R.layout.farmerlayout, parent, false);
        TextView fcode = (TextView) farmerView.findViewById(R.id.tvfcode_FL);
        fcode.setTextSize(TypedValue.COMPLEX_UNIT_DIP,15);
        TextView firstname = (TextView) farmerView.findViewById(R.id.tvffirstname_FL);
        firstname.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        TextView lastname = (TextView) farmerView.findViewById(R.id.tvflastname_FL);
        lastname.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);


        //SELECT NAME, UID FROM MEMBERS_ONLINE
        fcode.setText(arrayList.get(position)[0]);
        firstname.setText(arrayList.get(position)[1]);
        lastname.setText(arrayList.get(position)[2]);


        return farmerView;
    }

}

package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.FarmerDB;
import com.callippus.epaddy.dbfunctions.MenuDB;
import com.callippus.epaddy.resources.DBConnection;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by callippus on 3/3/18.
 */

public class PurchaseFarmerVerificationActivity extends Activity {



    public static final String TAG="PurchaseFarmerVerification";
    public Button next;
    public EditText farmer_code;
    public ArrayList<String> list;
    SQLiteDatabase db;
    MenuDB SDB;
    FarmerDB farmerDB = null;
    DBConnection dbc = null;
    UpdateFarmerActivity update=null;
    public static String code;
    final Context context = this;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_farmer_verification);
       /* ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        Preferences pref = new Preferences(getApplicationContext());

        next = (Button) findViewById(R.id.btnnext_PFV);
        farmer_code = (EditText) findViewById(R.id.etfcode_PFV);
        update=new UpdateFarmerActivity();

        try {
            dbc = new DBConnection(this);
            farmerDB = new FarmerDB(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                code = farmer_code.getText().toString();
                Log.d(TAG, "Entered Farmer Code in PurchaseFarmerVerification screen  :: " + code);
                if(code.length() > 0) {
                    int resp = farmerDB.isValidCode(code);
                    Log.d(TAG, "resp from  PurchaseFarmerVerification screen  :: " + resp);
                    Log.d(TAG, "After Isvalid()   :: " + resp);
                    if (resp == 0) {

                        Intent goToNextActivity = new Intent(
                                getApplicationContext(), PurchaseFarmerVerificationDetailsActivity.class);
                        startActivity(goToNextActivity);

                    } else {
                        //farmer_code.setError("Please Enter Valid Farmer Code");
                        AlertDialog.Builder alert = new AlertDialog.Builder(context);
                        alert.setTitle("Paddy Procurement");
                        alert.setMessage("Please enter Valid Farmercode")
                                .setCancelable(false)
                                .setNeutralButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                                dialog.cancel();
                                            }
                                        });
                        AlertDialog alertDialog1 = alert.create();
                        alertDialog1.show();
                    }
                }
                else
                {
                    farmer_code.setError("Please Enter Farmer Code");
                }

            }

        });

    }

    public static String Code() {
        return code;
    }


    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}

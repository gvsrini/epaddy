package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.PaddyMovementDB;
import com.callippus.epaddy.resources.DBConnection;

import java.io.IOException;

/**
 * Created by Callippus on 08-02-2018.
 */

public class DeleteVehicleDetailsActivity extends Activity {


    public static final String TAG = "DeleteVehicleDetails";
    public Button delete;
    public EditText vehicle_no;
    PaddyMovementDB paddymovementDB = null;
    DBConnection dbc = null;
    public static String vehicle_number;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.deletevehicledetails);
       /* ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        Preferences pref = new Preferences(getApplicationContext());

        delete = (Button) findViewById(R.id.btndel_DVD);
        vehicle_no = (EditText) findViewById(R.id.et_delete_vehicle_no_hint_DFD);
        vehicle_no.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        try {
            dbc = new DBConnection(this);
            paddymovementDB = new PaddyMovementDB(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vehicle_number = vehicle_no.getText().toString();
                Log.d(TAG, "Entered vehicle Number in Delete Vehicle Details screen  :: " + vehicle_number);
                if (vehicle_number.length() > 0) {
                    int res = paddymovementDB.isValidVehicle(vehicle_number);
                    if (res == 0) {
                    /* for dialog box  starts here */
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DeleteVehicleDetailsActivity.this);

                        // Setting Dialog Title
                        alertDialog.setTitle("Confirm Delete...");

                        // Setting Dialog Message
                        alertDialog.setMessage("Are you sure, You  want to delete vehicle number"+vehicle_number+"?");
                        // Setting Icon to Dialog
                        alertDialog.setIcon(R.drawable.deleteconfirmation);

                        // Setting Positive "Yes" Button
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                // Write your code here to invoke YES event
                                Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();

                                //int resp1 = paddymovementDB.isValidVehicle(vehicle_number);
                                int resp = paddymovementDB.deleteVehicle(vehicle_number);
                                if (resp == 0) {
                                        Toast.makeText(DeleteVehicleDetailsActivity.this, "Sucessfully Deteled Vehicle" + vehicle_number, Toast.LENGTH_LONG).show();
                                        Intent goToNextActivity = new Intent(
                                                getApplicationContext(), VehicleManagementActivity.class);
                                        startActivity(goToNextActivity);

                                    }
                                else  {
                                    Toast.makeText(DeleteVehicleDetailsActivity.this, "Failed to Delete Vehicle "+vehicle_number, Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                        // Setting Negative "NO" Button
                        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to invoke NO event
                                Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                                dialog.cancel();
                            }
                        });

                        // Showing Alert Message
                        alertDialog.show();
         /* here ended delete dialog */
                    }
                    else{
                        vehicle_no.setError("Please Enter Valid Vehicle Number");
                    }
                }
                else {
                    vehicle_no.setError("Please Enter Vehicle Number");
                }


            }
        });
    }


    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

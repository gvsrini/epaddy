package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.FarmerDB;
import com.callippus.epaddy.dbfunctions.MenuDB;
import com.callippus.epaddy.resources.DBConnection;
import com.callippus.epaddy.saleitems.UpdateFarmerActivity;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Callippus on 08-02-2018.
 */

public class EditFarmerDetailsActivity extends Activity{


    public static final String TAG="EditFarmerDetails";
    public Button submit;
    public EditText farmer_code;
    public ArrayList<String> list;
    SQLiteDatabase db;
    MenuDB SDB;
    FarmerDB farmerDB = null;
    DBConnection dbc = null;
    UpdateFarmerActivity update=null;
    public static String code;

        protected void onCreate(Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);
            setContentView(R.layout.editfarmerdetails);
      /*      ActionBar actionBar = getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);*/

            Preferences pref = new Preferences(getApplicationContext());

            submit = (Button) findViewById(R.id.btnsub_EFD);
            farmer_code = (EditText) findViewById(R.id.etfcode_EFD);
            update=new UpdateFarmerActivity();

            try {
                dbc = new DBConnection(this);
                farmerDB = new FarmerDB(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    code = farmer_code.getText().toString();
                    Log.d(TAG, "Entered Farmer Code in Edit Farmer Details screen  :: " + code);
                    if(code.length() > 0) {
                        int resp = farmerDB.isValidCode(code);
                        Log.d(TAG, "resp from  Edit Farmer Details screen  :: " + resp);
                        Log.d(TAG, "After Isvalid()   :: " + resp);
                        if (resp == 0) {

                            Intent goToNextActivity = new Intent(
                                    getApplicationContext(), UpdateFarmerActivity.class);
                            startActivity(goToNextActivity);

                        } else {
                            farmer_code.setError("Please Enter Valid Farmer Code");
                        }
                    }
                    else
                    {
                        farmer_code.setError("Please Enter Farmer Code");
                    }

                }

            });

        }

    public static String Code() {
        return code;
    }


    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

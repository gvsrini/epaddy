package com.callippus.epaddy.saleitems;

/**
 * Created by l_mah on 06-02-2018.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.MainActivityDAO;
import com.callippus.epaddy.interfaces.OnFarmerEvents;
import com.callippus.epaddy.resources.DBConnection;
import com.callippus.epaddy.resources.FarmersArrayAdapter;

import java.util.ArrayList;


public class FarmerDetailsActivity extends Activity implements OnFarmerEvents{

        private final static int MESSAGE_BOX = 1;
        public final static String TAG="detaild_farmerdetails";
        /* Handler to display UI response messages   */
        @SuppressLint("HandlerLeak")
        Handler handler = new Handler() {
                public void handleMessage(android.os.Message msg) {
                        switch (msg.what) {
                                case MESSAGE_BOX:
                                        String str = (String) msg.obj;
                                        showDialog(str);
                                        break;
                                default:
                                        break;
                        }
                }

                private void showDialog(String str) {
                }
        };

        OnFarmerEvents mCallback1;
        ListView farmerListView = null;
        Context context = this;
        MainActivityDAO mainActivityDAO = null;
        public ArrayList<String[]> farmerList = new ArrayList<String[]>();
        public FarmersArrayAdapter farmersListAdapter;
        int i=0;
        public static String farmer_code;

        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.farmer_listview);
                /*ActionBar actionBar = getActionBar();
                actionBar.setDisplayHomeAsUpEnabled(true);*/
                Log.d(Constants.TAG, "About to create member activity");

                try {
                        mainActivityDAO = new MainActivityDAO(this);
                } catch (Exception e) {
                        //Show an alert dialog here.

                        handler.obtainMessage(MESSAGE_BOX, "Exception opening DB" + e.getMessage()).sendToTarget();
                        e.printStackTrace();
                        return;
                }

                DBConnection db = null;

                farmerListView = (ListView) findViewById(R.id.farmerList);
                farmerList = mainActivityDAO.getFarmers(farmerList);
                farmersListAdapter = new FarmersArrayAdapter(this, farmerList);
                farmerListView.setAdapter(farmersListAdapter);




                farmerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                                long arg3) {
                                // i = i + 1;
                                arg1.setSelected(true);
                                String farmer[] = farmersListAdapter.getItem(arg2);
                                mCallback1 = FarmerDetailsActivity.this;
                                mCallback1.onFarmerClick(farmersListAdapter.getItem(arg2));
                        }
                });

        }

        @Override
        public void onFarmerClick( String[] farmer) {

                farmer_code=farmer[0];
                Log.d(TAG,"farmer[0] value::"+farmer_code);
                Intent goToNextActivity = new Intent(
                        getApplicationContext(), DetailedFarmerDetails.class);
                startActivity(goToNextActivity);

        }

        /*added for back menu*/
        public boolean onOptionsItemSelected(MenuItem item) {
                switch (item.getItemId()) {
                        case android.R.id.home:
                                // app icon in action bar clicked; goto parent activity.
                                this.finish();
                                return true;
                        default:
                                return super.onOptionsItemSelected(item);
                }
        }


}

package com.callippus.epaddy.saleitems;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.FarmerDB;
import com.callippus.epaddy.resources.DBConnection;
import java.io.IOException;

/**
 * Created by Callippus on 15-02-2018.
 */

public class TruckNumberInputActivity extends Activity {

    public static final String TAG = "Truck Memo Input : ";
    public static String str;
    EditText truckNo;
    Button next;
    DBConnection dbc;
    FarmerDB farmerDB;
    public static  String truck_number = null;

    final Context context = this;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.trucknumber_input);

         next = (Button) findViewById(R.id.btnnext_TNI);
         truckNo=(EditText)findViewById(R.id.ettruckno_TNI);

        try {
            dbc = new DBConnection(this);
            farmerDB = new FarmerDB(this);
        } catch (IOException e) {
            e.printStackTrace();
        }


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                str=truckNo.getText().toString();

                truck_number = str;

                Log.d(TAG,"Enterd vehicel number in screen is :: "+str);

                if(!str.isEmpty()) {

                    int ret =farmerDB.delete_table_data("STOCK_IN_PROCESS");

                    Log.d(TAG, "resp in  delete_table_data() :: " + ret);
                    int resp = farmerDB.isValidTruckId(str);
                    Log.d(TAG, "resp in  isValidTruckId() :: " + resp);
                    if (resp == 0) {
                        Intent goToNextActivity = new Intent(
                                getApplicationContext(), TruckInfoActivity.class);
                        startActivity(goToNextActivity);
                    }
                    else
                    {

                        AlertDialog.Builder alert = new AlertDialog.Builder(context);
                        alert.setTitle("Paddy Procurement");
                        alert.setMessage("Invalid Truck Number,Please Enter Valid Truck Number")
                                .setCancelable(false)
                                .setNeutralButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                                dialog.cancel();
                                            }
                                        });
                        AlertDialog alertDialog1 = alert.create();
                        alertDialog1.show();
                    }

                }
                else
                {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setTitle("Paddy Procurement");
                    alert.setMessage("Please Enter Truck Number")
                            .setCancelable(false)
                            .setNeutralButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alertDialog1 = alert.create();
                    alertDialog1.show();

                }
            }

        });




    }

}

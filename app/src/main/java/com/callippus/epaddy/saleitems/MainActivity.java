package com.callippus.epaddy.saleitems;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.callippus.epaddy.LoginActivity;
import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.CustomerSaleDB;
import com.callippus.epaddy.dbfunctions.MainActivityDAO;
import com.callippus.epaddy.interfaces.OnOrderEvents;
import com.callippus.epaddy.resources.DBConnection;
import com.callippus.epaddy.resources.ProductsArrayAdapter;
import com.callippus.epaddy.resources.Utils;
/*import com.callippus.retail.mobile.saleitems.DTO.Distribution;
import com.callippus.retail.mobile.saleitems.DTO.Member;
import com.callippus.retail.mobile.saleitems.DTO.Order;
import com.callippus.retail.mobile.saleitems.DTO.OrderHeader;
import com.callippus.retail.mobile.saleitems.DTO.OrderTrailer;
import com.esys.leoparddemo.GlobalPool;
import com.esys.leoparddemo.R;
import com.leopard.api.FPS;
import com.leopard.api.FpsConfig;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;*/

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;


public class MainActivity extends Activity implements OnOrderEvents {
    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    private final static int MESSAGE_BOX = 1;
    AlertDialog dialog;

    static ProgressDialog dlgCustom;
  /*  FPS fps;
    FpsConfig fpsconfig = new FpsConfig();*/
    private byte[] bufvalue = {};
    private byte[] image = {};
    private byte[] image1 = {};
    private int iRetVal;
    //private boolean conStatus = false;
    public static final int DEVICE_NOTCONNECTED = -100;
    private boolean verifyfinger = false;
    private boolean verifycompressed = false;
    private boolean verifyuncompressed = false;
    private ImageView imgCapture;


    /* Handler to display UI response messages   */
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case MESSAGE_BOX:
                    String str = (String) msg.obj;
                    showDialog(str);
                    break;
                default:
                    break;
            }
        }

        ;
    };
    public static String qty = "0.00";
    OnOrderEvents mCallback;
    Context context = this;
    int i = 0;
    String rationCard, familyHead;
    Button btnDistribution;
    TextView amount;
    TextView txtFamilyHead;

    EditText rcNo;
   /* List<Distribution> distList = new ArrayList<>();
    List<Member> memList = new ArrayList<>();*/
    ListView list = null;
    MainActivityDAO mainActivityDAO = null;
    LinearLayout ib = null;
    Utils utils = new Utils();
    String message = "";
    private String URL = "";
    private ProductsArrayAdapter listAdapter;
    private ArrayList<String[]> productList = new ArrayList<String[]>();
   /* private OrderHeader oh = new OrderHeader();*/
    private AsyncTask<Void, Integer, Void> task = null;

    //alert dialog for downloadDialog
    private static AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {

                }
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return downloadDialog.show();
    }

    public boolean validate() {
        //Check for presence of sqlite file
        File file = new File(DBConnection.DB_PATH);
        if (!file.exists()) {
            handler.obtainMessage(MESSAGE_BOX, "Device database missing. Please contact Administrator.").sendToTarget();
            return false;
        }

        //Check for presence of UIDAI pub key certificate
        file = new File(DBConnection.CERT_PATH);
        if (!file.exists()) {
            //Enable this 2 lines for every time download certificate file.
            // OnOrderEvents o = MainActivity.this;
            //task = new AsyncCallCertificateFile(o).execute();
        }
        //Check for presence of voice files
        file = new File(DBConnection.APP_PATH + "/audio");
        if (!file.exists()) {
            file.mkdir();
        }
        file = new File(DBConnection.APP_PATH + "/audio/audio.zip");
        if (file.exists()) {
            file.delete();
        }
        List<File> audio_files_list = getListFiles(new File(DBConnection.APP_PATH + "/audio/"));
        if (audio_files_list.size() == 0) {
            OnOrderEvents o = MainActivity.this;
            task = new AsyncCallAudioFiles(o).execute();
        }
        //Check for presence of

        return true;
    }

    private List<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<File>();
        File[] files = parentDir.listFiles();
        for (File file : files) {
            inFiles.add(file);
        }
        return inFiles;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        /*ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        DBConnection db = null;
        try {
            db = new DBConnection(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        URL = Preferences.remoteUrlPref;//db.fromProperties("remote_url");
        //Ration Card No.
        rcNo = (EditText) findViewById(R.id.rcno);
        utils.playMP3("rationcardno.mp3");
        amount = (TextView) findViewById(R.id.amount);
        //Button to trigger web service invocation
        btnDistribution = (Button) findViewById(R.id.button1);
        txtFamilyHead = (TextView) findViewById(R.id.familyHead);

        txtFamilyHead.setText("Head");
//        ib = (LinearLayout) findViewById(R.id.itembar);
//
//        ib.setVisibility(View.GONE);

//        ImageView imgBus = (ImageView) findViewById(R.id.fingerimage);
//        imgBus.setEnabled(false);
        if (!validate())
            return;

        try {
            mainActivityDAO = new MainActivityDAO(this);
        } catch (Exception e) {
            //Show an alert dialog here.
            handler.obtainMessage(MESSAGE_BOX, "Exception opening DB" + e.getMessage()).sendToTarget();
            e.printStackTrace();
            return;
        }

        list = (ListView) findViewById(R.id.itemList);
        listAdapter = new ProductsArrayAdapter(this, productList);
        list.setAdapter(listAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                i = i + 1;
                mCallback = MainActivity.this;
                mCallback.onItemSelected(arg0, listAdapter.getItem(arg2), arg2);
            }
        });

        if (Preferences.rationCardPref.equals("qrCode")) {
            rcNo.setEnabled(false);
            scanQR();
        } else if (Preferences.rationCardPref.equals("barCode")) {
            rcNo.setEnabled(false);
            scanBar();
        } else if (Preferences.rationCardPref.equals("manual")) {
            rcNo.setEnabled(true);
        }


        //Button Click Listener
       /* btnDistribution.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                //Check if Celcius text control is not empty
                if (!rcNo.getText().toString().equals("")) {
                    //Get the text control value
                    rcNo.setFocusable(false);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(rcNo.getWindowToken(), 0);

                    rationCard = rcNo.getText().toString();
                    OnOrderEvents o = MainActivity.this;
                    //Create instance for AsyncCallWS
                    if (task == null || task.getStatus() == AsyncTask.Status.FINISHED)
                        task = new AsyncCallGetDistribution(o).execute();
                    else
                        Toast.makeText(MainActivity.this, "Another task is already running. Please try later.", Toast.LENGTH_LONG).show();
                    //If text control is empty
                } else {
                    rcNo.setText("Please enter Ration Card No.");
                }
            }
        });*/

        Button btnClearOrder = (Button) findViewById(R.id.clearOrder);
        /*btnClearOrder.setOnClickListener(new OnClickListener() {
                                             public void onClick(View v) {
                                                 try {
                                                     mainActivityDAO.deleteOrder();
                                                     productList = mainActivityDAO.getProducts(productList);
                                                     ListAdapterUpdateThread r = new ListAdapterUpdateThread();
                                                     r.start();
                                                     Toast.makeText(MainActivity.this, "Cleared Orders in Process", Toast.LENGTH_LONG).show();
                                                 } catch (Exception e) {
                                                     e.printStackTrace();
                                                 } finally {
                                                     rcNo.setFocusableInTouchMode(true);
                                                 }
                                             }
                                         }
        );*/

        Button btnNext = (Button) findViewById(R.id.next);
      /*  btnNext.setOnClickListener(new OnClickListener() {
                                       public void onClick(View v) {
                                           try {
                                               GlobalPool myapp = (GlobalPool) getApplicationContext();
                                               myapp.setOrder(getOrder());
                                               //Check items in cart
                                               if(oh.amount.equals("0.00") || amount.equals("0.00"))
                                               {
                                                   utils.playMP3("noitems.mp3");
                                                   showMessageBox("No items in cart.");
                                                   return;
                                               }
                                               Log.d(Constants.TAG, "About to call members");
                                               Intent memberActivity = new Intent(getApplicationContext(), MemberActivity.class);
                                               startActivity(memberActivity);
                                           } catch (Exception e) {
                                               Log.e(Constants.TAG, "Exception:" + e.getMessage());
                                               e.printStackTrace();
                                           } finally {
                                           }
                                       }
                                   }
        );*/
    }

    //product barcode mode
    public void scanBar() {
        try {
            //start the scanning activity from the com.google.zxing.client.android.SCAN intent
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException anfe) {
            //on catch, show the download dialog
            showDialog(MainActivity.this, "No Scanner Found", "Download a scanner code activity?", "Yes", "No").show();
        }
    }

    //product qr code mode
    public void scanQR() {
        try {
            //start the scanning activity from the com.google.zxing.client.android.SCAN intent
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException anfe) {
            //on catch, show the download dialog
            showDialog(MainActivity.this, "No Scanner Found", "Download a scanner code activity?", "Yes", "No").show();
        }
    }


    //on ActivityResult method
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                //get the extras that are returned from the intent
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                rcNo.setText(contents);
                Toast toast = Toast.makeText(this, "Content:" + contents + " Format:" + format, Toast.LENGTH_LONG);
                toast.show();
                btnDistribution.performClick();
            }
        }
    }

   /* public void initOrder() {
        amount.setTextColor(Color.parseColor("#DDDDDD"));
        amount.setText("\u20B90.00");

        familyHead = "";
        txtFamilyHead.setText("Head");
        txtFamilyHead.setTextColor(Color.parseColor("#DDDDDD"));

        //Remove fingerprint buffer and XML buffer
        oh = new OrderHeader();

        rcNo.setFocusable(true);

        productList = mainActivityDAO.getProducts(productList);

        listAdapter.notifyDataSetChanged();
    }*/

    /*  To show response messages  */
    public void showDialog(String str) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("PDS Demo Application");
        alertDialogBuilder.setMessage(str).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        /* create alert dialog*/
        AlertDialog alertDialog = alertDialogBuilder.create();
        /* show alert dialog*/
        alertDialog.show();
    }

/*
    public Order getOrder() {
        List<OrderTrailer> items = new ArrayList<OrderTrailer>();
        Cursor cr = null;
        Order order = null;

        try {

            cr = mainActivityDAO.getOrdersInProcess();

            oh.id = mainActivityDAO.getConfig("MAX_ORDER_ID");

            BigDecimal amount = new BigDecimal("0.0");
            while (cr.moveToNext()) {
                OrderTrailer ot = new OrderTrailer();
                ot.order_id = oh.id;
                ot.product_name = cr.getString(0);
                ot.product_id = cr.getString(2);
                ot.lower_limit = cr.getString(1);
                ot.price_upper_limit = cr.getString(1);
                ot.quantity = cr.getString(3);
                ot.actual_price = cr.getString(4);
                ot.scheme_id = "";
                items.add(ot);
                amount = amount.add(new BigDecimal(ot.actual_price));
            }

            oh.familyHead = familyHead;
            oh.amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString();
            oh.card_id = rationCard;
            oh.card_type = "";
            oh.office_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
            Date dt = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            oh.order_date = sdf.format(dt);
            oh.order_discount = "0.00";
            oh.payment_mode = "0";
            oh.reconcile_status = "0";
            oh.remarks = "";
            oh.status = "0";
            oh.sync_status = "0";
            oh.user_id = LoginActivity.un;
            oh.vat = "0.00";

            order = new Order(oh, items);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cr != null)
                cr.close();
        }
        return order;
    }
*/

   /* public void getDistribution(String rationCard) {

        message = "";
        //Create request
        SoapObject request = new SoapObject(Constants.NAMESPACE, Constants.DIST_METHOD_NAME);
        //Property which holds input parameters
        PropertyInfo rationCardPI = new PropertyInfo();
        //Set Name
        rationCardPI.setName("RationCardNo");
        //Set Value
        rationCardPI.setValue(rationCard);
        //Set dataType
        //rationCardPI.setType(String.class);
        //Add the property to request object
        request.addProperty(rationCardPI);
        //Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.dotNet = true;
        //Set output SOAP object
        envelope.setOutputSoapObject(request);
        //Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL, Constants.TIMEOUT);

        try {
            mainActivityDAO.deleteDistribution();

            //Invoke web service
            androidHttpTransport.call(Constants.DIST_SOAP_ACTION, envelope);

            //Get the response
            SoapObject response = (SoapObject) envelope.getResponse();
            int i = RetrieveFromSoap(response);

            if (i == -1) {
                message = "Invalid response from server";
                return;
            }

            if (i == 99) {
                message = "99";//Invalid Ration Card No. Please contact DSO.";
                return;
            }

            if (i == 0 && memList.size() > 0 && distList.size() > 0) {
                mainActivityDAO.populateDB(memList, distList);
            } else
                message = "No data for Ration Card No. Please contact DSO.";

        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof SocketTimeoutException) {
                message = "Server did not respond in a timely manner";
            } else if (e instanceof SocketException) {
                message = "Connection timed out";
            } else {
                message = "Error:" + e.getMessage();
            }
        }
    }*/

  /*  public int RetrieveFromSoap(SoapObject soap) {
        memList.clear();
        distList.clear();

        SoapObject s_members = (SoapObject) soap.getProperty("members");
        SoapObject s_dist = (SoapObject) soap.getProperty("distribution");

        if (s_members.getPropertyCount() == 0)
            return -1; //Invalid response from server

        for (int i = 0; i < s_members.getPropertyCount(); i++) {
            SoapObject pii = (SoapObject) s_members.getProperty(i);
            String text[] = pii.getProperty(0).toString().split("/");
            //Response: 000/762777496156/रमेश कुमार/1/स्वयं/null/0/0/0/0
            //Member(String slNo, String name, String uid, String relation)

            int ret = Integer.parseInt(text[0]);
            if (ret != 0)
                return ret; //Some error occurred

            if (i == 0 || text[3].equals("1")) //Family Head when Sl. No is 1 //Assign Family Head just in case Sl. No. 1 does not exist
                familyHead = text[2];

            Member member = new Member(text[3], text[2], text[1], text[4]);
            memList.add(member);
            Log.d(Constants.TAG, pii.getProperty(0).toString());
        }*/

       /* for (int i = 0; i < s_dist.getPropertyCount(); i++) {
            SoapObject pii = (SoapObject) s_dist.getProperty(i);
            String text[] = pii.getProperty(0).toString().split("/");

            //Response: 100/5/3/चना दाल/1.00/0/1.00/32.00/1
            //Distribution(String productId, String name, String cardType, String allotmentType, String qty, String price, String uom, String max_qty, String availed_qty) {
            Distribution distribution = new Distribution(text[2], text[3], text[1], text[1], text[4], text[7], text[8], text[4], text[5]);
            distList.add(distribution);
            Log.d(Constants.TAG, pii.getProperty(0).toString());
        }
        return 0;
    }
*/
    @Override
    public void onGetDistributionCompleted() {
        try {
            mainActivityDAO.deleteOrder();
            productList = mainActivityDAO.getProducts(productList);
            listAdapter.notifyDataSetChanged();

            ib.setVisibility(View.VISIBLE);
            if (!message.equals("")) {
                rcNo.setFocusable(true);
                ib.setVisibility(View.GONE);
                if (message.equals("99")) {
                    utils.playMP3("JH 9(retryaadhaarno).mp3");
                }
            } else {
                txtFamilyHead.setTextColor(Color.BLACK);
                txtFamilyHead.setText(familyHead);
                Log.d(Constants.TAG, "Family Head: " + familyHead);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onGetAudioFilesCompleted() {
        try {
            if (!message.equals(""))
                rcNo.setFocusable(true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Context getContext() {
        return context;
    }

    /*public void onSetQty(AdapterView<?> view, String[] product, BigDecimal qty, boolean itemExists, int rowIndex) {
        try {
            Cursor cr = null;
            cr = mainActivityDAO.getOrdersInProcess();
            BigDecimal amount_new = new BigDecimal("0.0");
            CustomerSaleDB CSDB = new CustomerSaleDB(this);
            BigDecimal max_qty = new BigDecimal(product[7]);

            if (qty.compareTo(max_qty) == 1) {
                Toast.makeText(this, "Max Qty for this item reached:" + max_qty, Toast.LENGTH_LONG).show();
                return;
            }

            product[4] = qty.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString();
            product[9] = qty.multiply(new BigDecimal(product[5])).setScale(2, BigDecimal.ROUND_HALF_EVEN).toString();

            if (!itemExists) {
                CSDB.addOrdersInProcess(product, i, "custSale");
            } else {
                CSDB.changeQty(product[0], qty.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString(), product[5], "0");
            }

            if (qty.compareTo(new BigDecimal("0.0")) == 1) {
                list.setItemChecked(rowIndex, true);
                LinearLayout qtyView = (LinearLayout) view.findViewById(R.id.qtySegment);
                qtyView.setBackgroundColor(Color.CYAN);
            } else {
                list.setItemChecked(rowIndex, false);
                LinearLayout qtyView = (LinearLayout) view.findViewById(R.id.qtySegment);
                qtyView.setBackgroundColor(Color.WHITE);
            }

            if (oh.amount == null || oh.amount.equals(""))
                oh.amount = "0.00";
            BigDecimal amt = new BigDecimal(oh.amount);
            amt = amt.add(qty.multiply(new BigDecimal(product[5])));
            oh.amount = amt.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString();

            while (cr.moveToNext()) {
                OrderTrailer ot = new OrderTrailer();
                ot.actual_price = cr.getString(4);
                amount_new = amount_new.add(new BigDecimal(ot.actual_price));
            }
            oh.amount = amount_new.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString();

            amount.setTextColor(Color.BLACK);
            amount.setText("\u20B9" + oh.amount);

            //Update list
            //SELECT PRODUCT_ID,NAME,CARD_TYPE,ALLOTMENT_TYPE,QUANTITY,PRICE,UOM,MAX_QUANTITY,AVAILED_QTY, '0.00' AMT FROM DISTRIBUTION_ONLINE
            listAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public void onItemSelected(final AdapterView<?> rowView, final String[] product, final int rowIndex) {
        try {
            CustomerSaleDB CSDB = new CustomerSaleDB(this);
            BigDecimal qty = new BigDecimal("0.00");
            final Cursor check = CSDB.checkCart(product[0]);
           /* 1	चावल
            2	गेहूँ
            3	चना दाल
            4	साबुत उरद
            5	काबुली चना
            6	खाद्य तेल - रिफाइंड
            7	नमक
            8	चीनी
            9	आटा
            10	NFSA चावल
            11	NFSA गेहूँ
            12	खाद्य तेल- सरसो*/
            String s = null;
            s = product[0].toString();
            if (s.equals("1"))
                s = "chaval.mp3";
            else if (s.equals("2"))
                s = "gehu.mp3";
            else if (s.equals("3"))
                s = "chanadal.mp3";
            else if (s.equals("4"))
                s = "sabuturd.mp3";
            else if (s.equals("5"))
                s = "kabulichana.mp3";
            else if (s.equals("6"))
                s = "oil.mp3";
            else if (s.equals("7"))
                s = "namak.mp3";
            else if (s.equals("8"))
                s = "3_com.wav";
            else if (s.equals("9"))
                s = "aata.mp3";
            else if (s.equals("10"))
                s = "nfsachawal.mp3";
            else if (s.equals("11"))
                s = "nfsagehu.mp3";
            else if (s.equals("12"))
                s = "saraso.mp3";

            utils.playMP3(s);

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            LayoutInflater factory = LayoutInflater.from(this);
            //final View view = factory.inflate(R.layout.qty, null);
            //alert.setView(view);
            dialog = alert.create();
           if( dialog.isShowing())
               return;

            //TextView title = (TextView) view.findViewById(R.id.proTitle);
            //title.setText(product[1].toString());

            //ImageView iv = (ImageView) view.findViewById(R.id.proImage);
            //iv.setImageResource(getFlagResource("pro_" + product[0].toString()));

            //TextView tv = (TextView) view.findViewById(R.id.proDetails);
            String alert1 = "Max Qty   : " + product[7];
            Float lifted_qty = Float.parseFloat(product[8].toString());
            DecimalFormat df = new DecimalFormat("0.00");
            df.setMaximumFractionDigits(2);
            String str_lifted_qty = df.format(lifted_qty);
            String alert2 = "Lifted Qty: " + str_lifted_qty;
            //tv.setText(alert1 + "   " + alert2);

            //final EditText input = (EditText) view.findViewById(R.id.txtQty);
            //input.setSelectAllOnFocus(true);
            //input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            final boolean itemExists = (check.getCount() > 0);
            if (!itemExists) {
                qty = new BigDecimal(product[4]);
            } else {
                check.moveToFirst();
                qty = new BigDecimal(check.getString(1));
            }

            //input.setText(qty.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());


            alert.setNeutralButton("Get", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Get value from weighing machine.
                    return;
                }
            });
            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Do something with value!
                    //Log.d(Constants.TAG, "This is the entered value:" + input.getText());

                    MainActivity.qty = "0.00";
//                    Intent serial = new Intent(getApplicationContext(), Act_Serial.class);
//                    startActivity(serial);
                    //onSetQty(rowView, product, new BigDecimal(input.getText().toString()), itemExists, rowIndex);
                    //utils.playTotal(input.getText().toString(), 2);
                }
            });

            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {

                    /*** GATTU: To check this code ***/
                    BigDecimal qty = new BigDecimal(product[4]);
                    if (qty.compareTo(new BigDecimal("0.0")) == 1)
                        list.setItemChecked(rowIndex, true);
                    else
                        list.setItemChecked(rowIndex, false);

                    // Canceled.
                    return;
                }
            });

            alert.show();
            //dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showMessageBox(String str) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("PDS");
        alert.setMessage(str)
                .setCancelable(false)
                .setNeutralButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialog,
                                    int which) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog1 = alert.create();
        alertDialog1.show();
    }

    public int getFlagResource(String name) {
        String package_name = context.getPackageName();
        int resId = getContext().getResources().getIdentifier(name, "drawable", package_name);
        return resId;
    }


    public class Decompress {
        private String _zipFile;
        private String _location;

        public Decompress(String zipFile, String location) {
            _zipFile = zipFile;
            _location = location;

            _dirChecker("");
        }

//        public void unzip() {
//            try {
//                FileInputStream fin = new FileInputStream(_zipFile);
//                ZipInputStream zin = new ZipInputStream(fin);
//                ZipEntry ze = null;
//                long total = 0;
//
//                while ((ze = zin.getNextEntry()) != null) {
//                    total += ze;
//                    Log.v("Decompress", "Unzipping " + ze.getName());
//                    publishProgress(Constants.LOADING,  (int) ((total * 100) / lenghtOfFile));
//                    if (ze.isDirectory()) {
//                        _dirChecker(ze.getName());
//                    } else {
//                        FileOutputStream fout = new FileOutputStream(_location + ze.getName());
//                        for (int c = zin.read(); c != -1; c = zin.read()) {
//                            fout.write(c);
//                        }
//
//                        zin.closeEntry();
//                        fout.close();
//                    }
//
//                }
//                zin.close();
//            } catch (Exception e) {
//                Log.e("Decompress", "unzip", e);
//            }
//
//        }

        private void _dirChecker(String dir) {
            File f = new File(_location + dir);

            if (!f.isDirectory()) {
                f.mkdirs();
            }
        }
    }
/*
    class AsyncCallGetDistribution extends AsyncTask<Void, Integer, Void> {

        Dialog dialog;
        ProgressBar progressBar;
        TextView tvLoading, tvPer;
        Button btnCancel;

        private OnOrderEvents listener;

        public AsyncCallGetDistribution(OnOrderEvents listener) {
            this.listener = listener;
        }

       *//* @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(MainActivity.this);
            dialog.setCancelable(false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.progressdialog);

            progressBar = (ProgressBar) dialog.findViewById(R.id.progressBar1);
            tvLoading = (TextView) dialog.findViewById(R.id.tv1);
            tvPer = (TextView) dialog.findViewById(R.id.tvper);

            dialog.show();
        }*//*

       *//* @Override
        protected Void doInBackground(Void... params) {
            getDistribution(rationCard);
            return null;
        }
*//*
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0]);
            tvLoading.setText("Loading...  " + values[0] + " %");
            tvPer.setText(values[0] + " %");
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            dialog.dismiss();

            listener.onGetDistributionCompleted();

            AlertDialog alert = new AlertDialog.Builder(MainActivity.this)
                    .create();

            if (message.equals("")) {
                //alert.setTitle("Completed!!!");
                //alert.setMessage("Loaded Distribution SuccessFully!!!");

            } else {
                alert.setTitle("Failed!!!");
                if (message.equals("99"))
                    message = "Invalid Ration Card No. Please contact DSO.";
                alert.setMessage(message);
                alert.setButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }

        }
    }*/

    class AsyncCallAudioFiles extends AsyncTask<Void, Integer, Void> {

        Dialog dialog;
        ProgressBar progressBar;
        TextView tvLoading, tvPer;
        Button btnCancel;

        private OnOrderEvents listener;

        public AsyncCallAudioFiles(OnOrderEvents listener) {
            this.listener = listener;
        }

      /*  @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(MainActivity.this);
            dialog.setCancelable(false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.progressdialog);

            progressBar = (ProgressBar) dialog.findViewById(R.id.progressBar1);
            tvLoading = (TextView) dialog.findViewById(R.id.tv1);
            tvPer = (TextView) dialog.findViewById(R.id.tvper);

            dialog.show();
        }*/

        @Override
        protected Void doInBackground(Void... params) {
            message = "";
            int count;
            try {
                URL url = new URL(URL + "/audio.zip");
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream(DBConnection.APP_PATH + "/audio/audio.zip");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress(Constants.LOADING,  (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                String zipFile = DBConnection.APP_PATH + "/audio/audio.zip";
                String unzipLocation = DBConnection.APP_PATH + "/audio/";
                //publishProgress(Constants.EXTRACTING, (int) ((total * 100) / lenghtOfFile));
                //Decompress d = new Decompress(zipFile, unzipLocation);
                //d.unzip();
                ZipFile zip = new ZipFile(zipFile);
                FileInputStream fin = new FileInputStream(zipFile);
                ZipInputStream zin = new ZipInputStream(fin);
                ZipEntry ze = null;
                int tot = 0;
                int zip_size = zip.size();

                while ((ze = zin.getNextEntry()) != null) {
                    tot ++;
                    Log.v("Decompress", "Unzipping " + ze.getName());
                    publishProgress(Constants.EXTRACTING,  tot,zip_size);
                    if (ze.isDirectory()) {
                        _dirChecker(ze.getName());
                    } else {
                        FileOutputStream fout = new FileOutputStream(unzipLocation + ze.getName());
                        for (int c = zin.read(); c != -1; c = zin.read()) {
                            fout.write(c);
                        }

                        zin.closeEntry();
                        fout.close();
                    }

                }
                zin.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        private void _dirChecker(String name) {
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[1]);
            if(values[0] == Constants.LOADING)
                tvLoading.setText("First time use: Loading Audio Files..." + values[1] + " %");
            else
                tvLoading.setText("Extracting Audio Files...:"+ values[1] + "/" +values[2]);
            //tvPer.setText(values[0] + " %");
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            dialog.dismiss();

            listener.onGetAudioFilesCompleted();

            AlertDialog alert = new AlertDialog.Builder(MainActivity.this)
                    .create();

            if (message.equals("")) {

                alert.setTitle("Completed!!!");
                alert.setMessage("Loaded Audio Files SuccessFully!!!");

            } else {
                alert.setTitle("Failed!!!");
                alert.setMessage(message);
            }

            alert.setButton("Dismiss", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.show();
        }
    }

    class AsyncCallCertificateFile extends AsyncTask<Void, Integer, Void> {

        Dialog dialog;
        ProgressBar progressBar;
        TextView tvLoading, tvPer;
        Button btnCancel;

        private OnOrderEvents listener;

        public AsyncCallCertificateFile(OnOrderEvents listener) {
            this.listener = listener;
        }

       /* @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(MainActivity.this);
            dialog.setCancelable(false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.progressdialog);

            progressBar = (ProgressBar) dialog.findViewById(R.id.progressBar1);
            tvLoading = (TextView) dialog.findViewById(R.id.tv1);
            tvPer = (TextView) dialog.findViewById(R.id.tvper);

            dialog.show();
        }
*/
        @Override
        protected Void doInBackground(Void... params) {
            message = "";
            int count;
            try {
                URL url = new URL(URL + "/uidai_auth_stage.cer");
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream(DBConnection.APP_PATH + "/uidai_auth_stage.cer");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress((int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            // progressBar.setProgress(values[0]);
            // tvLoading.setText("Loading...  " + values[0] + " %");
            //tvPer.setText(values[0] + " %");
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            dialog.dismiss();

            listener.onGetAudioFilesCompleted();

            AlertDialog alert = new AlertDialog.Builder(MainActivity.this)
                    .create();

            if (message.equals("")) {

                // alert.setTitle("Completed!!!");
                // alert.setMessage("Loaded UIDAI Certificate SuccessFully!!!");

            } else {
                alert.setTitle("Failed!!!");
                alert.setMessage(message);
            }

//            alert.setButton("Dismiss", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
            alert.show();
        }
    }
    /*private class ListAdapterUpdateThread extends Thread {
        @Override
        public void run() {
            MainActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    initOrder();
                }
            });
        }
    }*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

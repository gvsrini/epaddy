package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.ReportsDB;

import java.io.IOException;
import java.util.Calendar;

/**
 * Created by callippus on 21/2/18.
 */

public class StockSummaryReport extends Activity{

    TextView fr, to,title,sub_title;
    TableLayout reporttable;
    String flag = null;
    private String myYear, myMonth, myDay;
    static final int ID_DATEPICKER = 0;
    Calendar c = Calendar.getInstance();
    Cursor cr;
    ReportsDB rDB;
    SQLiteDatabase database;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.stock_summary_report);
     /*   ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        fr = (TextView) findViewById(R.id.frDt_SSR);
        /*to = (TextView) findViewById(R.id.toDt_SSR);*/
        reporttable = (TableLayout) findViewById(R.id.reportStockSummaryTable);

        title = (TextView) findViewById(R.id.Title_SSDR);


        try {
            rDB = new ReportsDB(getApplicationContext());
            database = rDB.myDataBase;
        } catch (IOException e) {
            e.printStackTrace();
        }
        myYear = Integer.toString(c.get(Calendar.YEAR));
        myMonth = Integer.toString(c.get(Calendar.MONTH));
        myDay = Integer.toString(c.get(Calendar.DAY_OF_MONTH));
        myMonth = Integer.toString(Integer.parseInt(myMonth) + 1);

        if (myDay.length() == 1)
            myDay = "0" + myDay;
        if (myMonth.length() == 1)
            myMonth = "0" + myMonth;
        /*fr.setText(myYear + "-" + myMonth + "-" +myDay );
        to.setText(myYear + "-" + myMonth + "-" +myDay);*/


      /*  sub_title = (TextView) findViewById(R.id.SubTitle);
        sub_title.setText("Report from: "+myDay+ "-" + myMonth + "-" +myYear +" to: "+myDay+ "-" + myMonth + "-" +myYear );*/

       /* displayReport();*/
    }

    @SuppressWarnings("deprecation")
    public void getFrDt(View v) {
        flag = "from";
        showDialog(ID_DATEPICKER);
    }

    @SuppressWarnings("deprecation")
    public void getToDt(View v) {
        flag = "to";
        showDialog(ID_DATEPICKER);
    }

    public void getDailyRpt(View v) {
        displayReport();
       /* sub_title = (TextView) findViewById(R.id.SubTitle);
        sub_title.setText("Report from: "+fr.getText().toString() +" to: "+to.getText().toString() );*/

    }

    public  void getPrint(View v) {
        //rDB.getImage(reporttable);
    }

    private void displayReport() {

        try {
            reporttable.removeAllViews();
            String fromdate=fr.getText().toString();
            String date=fromdate.substring(0,2);
            String month=fromdate.substring(3,5);
            String year=fromdate.substring(6,10);
            String fr1=year.concat("-").concat(month).concat("-").concat(date);
            cr = rDB.getStockSummaryReport(fr1);
           /* Float qty=0.0f, price = 0.0f, tot_qty=0.0f, tot_price = 0.0f;*/

            title.setText("Stock Summary Report");
            reporttable.addView(rDB.HeaderFooterRowsFarmerReport(new String[]{"ID","PADDY TYPE","OPENING","CLOSING"}));


            while (cr.moveToNext()) {

                LayoutInflater inflater = LayoutInflater
                        .from(getApplicationContext());
                TableRow row = (TableRow) inflater.inflate(R.layout.stock_summary_report_row,
                        null);

                while(row.getChildCount()>cr.getColumnCount())
                    row.removeViewAt(row.getChildCount()-1);

                TextView one = (TextView) row.findViewById(R.id.r1_SSR);
                TextView two = (TextView) row.findViewById(R.id.r2_SSR);
                TextView three = (TextView) row.findViewById(R.id.r3_SSR);
                TextView four = (TextView) row.findViewById(R.id.r4_SSR);

            /*    qty = Float.parseFloat(cr.getString(2));
                price = Float.parseFloat(cr.getString(3));

                one.setText(cr.getString(0));
                two.setText(cr.getString(1));
                three.setText(Float.toString(qty));
                four.setText(Float.toString(price));

                tot_qty = tot_qty + Float.parseFloat(cr.getString(2));
                tot_price = tot_price + Float.parseFloat(cr.getString(3));
//                net = net + Float.parseFloat(cr.getString(3));

                reporttable.addView(row);*/
                reporttable.addView(row);

                one.setText(cr.getString(0));
                two.setText(cr.getString(1));
                three.setText(cr.getString(2));
                four.setText(cr.getString(3));
            }
           /* reporttable.addView(rDB.HeaderFooterRows(new String[]{"Total", "",Float.toString(tot_qty),Float.toString(tot_price)}));*/


        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        switch (id) {
            case ID_DATEPICKER:
                return new DatePickerDialog(this, myDateSetListener,
                        Integer.parseInt(myYear), (Integer.parseInt(myMonth)-1),
                        Integer.parseInt(myDay));
            default:
                return null;
        }
    }

    private DatePickerDialog.OnDateSetListener myDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(android.widget.DatePicker arg0, int year,
                              int monthOfYear, int dayOfMonth) {

            String month = String.valueOf(monthOfYear + 1);
            String day = String.valueOf(dayOfMonth);
            if (month.length() == 1)
                month = "0" + month;
            if (day.length() == 1)
                day = "0" + day;

            if (flag.equals("from"))
                fr.setText(day+ "/" + month + "/" + String.valueOf(year));
            else if (flag.equals("to"))
                to.setText(day+ "/" + month + "/" + String.valueOf(year));
        }
    };



    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}

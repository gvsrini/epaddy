package com.callippus.epaddy.saleitems;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.FarmerDB;
import com.callippus.epaddy.dbfunctions.PaddyMovementDB;
import com.callippus.epaddy.resources.DBConnection;
import com.callippus.epaddy.resources.Utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Lenovo on 05-06-2015.
 */
public class FarmerRegistrationActivity extends Activity {

    public static final String TAG="In Registation :";
    String flag = null;
    private String myYear, myMonth, myDay;
    static final int ID_DATEPICKER = 0;
    Calendar c = Calendar.getInstance();
    public static String un = null, pwd = null;
    final Context context = this;
    EditText t1, t2;
    Button reg,back;
    TextView d1, terminal;
    EditText farmerCode,firstName,lastName,fatherName,presentAddr,permanentAddr,town;
    EditText district,pincode,districtcode,mobileno,altmobile,bankaccno,aadhaarno,ifsc,bankname;
    Spinner spinnerDropDown,spinnerDropDown1;
    TextView dob;
    LinearLayout farmer_registration_xml;
    FarmerDB farmerDB = null;
    DBConnection dbc = null;
    Utils u = new Utils();
    String blockcode1,districtcode1;
    public ArrayList<String> block_code = new ArrayList<String>();
    public ArrayList<String> district_code = new ArrayList<String>();
    CheckBox checkboxforpermanent;
    PaddyMovementDB paddymovementDB = null;
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat datetime = new SimpleDateFormat("yyMMddHHmmss",
            Locale.US);
    String date=datetime.format(cal.getTime());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmerregistration);
        //ActionBar actionBar = getActionBar();
        //actionBar.setDisplayHomeAsUpEnabled(true);
        Preferences pref = new Preferences(getApplicationContext());

        farmer_registration_xml = (LinearLayout) findViewById(R.id.farmerRegistrationXML);
        reg = (Button) findViewById(R.id.Reg);
       /* back = (Button) findViewById(R.id.Back_btn_FRA);*/
        //farmerid=(EditText)findViewById(R.id.FARMERID);
      /*  farmerCode=(EditText)findViewById(R.id.FID);*/
        firstName = (EditText) findViewById(R.id.FNAME);
        lastName = (EditText) findViewById(R.id.LNAME);
        fatherName = (EditText) findViewById(R.id.FATHERNAME);
        presentAddr = (EditText) findViewById(R.id.Adress1);
        permanentAddr = (EditText) findViewById(R.id.Adress2);
        town = (EditText) findViewById(R.id.Town);
        district = (EditText) findViewById(R.id.District);
        pincode = (EditText) findViewById(R.id.Pincode);
        //blockcode = (EditText) findViewById(R.id.Blockcode);

        //districtcode = (EditText) findViewById(R.id.Districtcode);
        dob = (TextView) findViewById(R.id.DOB);
        //phoneno = (EditText) findViewById(R.id.Phoneno);
        mobileno = (EditText) findViewById(R.id.Mobileno);
        altmobile = (EditText) findViewById(R.id.AlterMobileno);
        bankaccno = (EditText) findViewById(R.id.BankAccno);
        aadhaarno = (EditText) findViewById(R.id.Aadhaarno);
        ifsc=(EditText) findViewById(R.id.etIfsc);
        bankname=(EditText) findViewById(R.id.etbankname);




        try {
            dbc = new DBConnection(this);
            farmerDB = new FarmerDB(this);
            paddymovementDB = new PaddyMovementDB(this);
        } catch (IOException e) {
            e.printStackTrace();
        }


        myYear = Integer.toString(c.get(Calendar.YEAR));
        myMonth = Integer.toString(c.get(Calendar.MONTH));
        myDay = Integer.toString(c.get(Calendar.DAY_OF_MONTH));
        myMonth = Integer.toString(Integer.parseInt(myMonth) + 1);

        if (myDay.length() == 1)
            myDay = "0" + myDay;
        if (myMonth.length() == 1)
            myMonth = "0" + myMonth;

        /*dob.setText(myYear + "-" + myMonth + "-" +myDay );*/


      /*  farmer_registration_xml.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                View i = getWindow().getCurrentFocus();
                if (i != null)
                    imm.hideSoftInputFromWindow(i.getWindowToken(), 0);
                return false;
            }
        });*/



      /* this is for dropdowns block code starts here*/

        spinnerDropDown =(Spinner)findViewById(R.id.Blockcode);
        block_code=farmerDB.getBlockCodes();
        ArrayAdapter<String> adapter= new ArrayAdapter<String>(this,android.
                R.layout.simple_spinner_dropdown_item ,block_code);

        spinnerDropDown.setAdapter(adapter);

        spinnerDropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                int sid=spinnerDropDown.getSelectedItemPosition();
                blockcode1 = block_code.get(sid);


            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        /* here dropdown end for block code*/

        /* district code starts here*/
        spinnerDropDown1 =(Spinner)findViewById(R.id.Districtcode);
        district_code=farmerDB.getDistrictCodes();

        ArrayAdapter<String> adapter1= new ArrayAdapter<String>(this,android.
                R.layout.simple_spinner_dropdown_item ,district_code);

        spinnerDropDown1.setAdapter(adapter1);

        spinnerDropDown1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                int sid=spinnerDropDown1.getSelectedItemPosition();
                districtcode1 = district_code.get(sid);

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        /* here distict code end*/



        /*back.setOnClickListener(new View.OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity = new Intent(
                        getApplicationContext(), GridLayoutActivity.class);
                startActivity(goToNextActivity);
            }
        });*/




        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                String MobilePattern = "[0-9]{10}";
                Pattern p = Pattern.compile(MobilePattern);

                String PinPattern = "(\\d{6})";
                Pattern p1 = Pattern.compile(PinPattern);


               /* String AadhaarPattern = "[0-9]{12}";*/
                /*Pattern p2 = Pattern.compile(AadhaarPattern);*/
                String ifscpattern="^[a-zA-Z0-9]+$";
                Pattern p3 = Pattern.compile(ifscpattern);
                //String block_code_pattern= "\\d+",district_code_pattern = "\\d+";
                //Pattern p4 = Pattern.compile(block_code_pattern);
                //Pattern p5 = Pattern.compile(district_code_pattern);
                String bank_acc_pattern = "\\d+", district_code_pattern = "\\d+";
                Pattern p4 = Pattern.compile(bank_acc_pattern);


                String code = dbc.getPropValue("DPC_ID");
                String code1=code.concat(date);
                Log.d(TAG,"Farmer code : "+code1);
                String Fcode =code1.toString();
                String fname = firstName.getText().toString();
                String lname = lastName.getText().toString();
                String fathername = fatherName.getText().toString();
                String add1 = presentAddr.getText().toString();
                String add2 = permanentAddr.getText().toString();
                String town1 = town.getText().toString();
                String dist = district.getText().toString();
                String pin = pincode.getText().toString();
                String dob1 = dob.getText().toString();
                String mobile = mobileno.getText().toString();
                String altmobile1 = altmobile.getText().toString();
                String bankaccno1=bankaccno.getText().toString();
                String aadhaar =aadhaarno.getText().toString();
                String ifsc_code=ifsc.getText().toString();
                String bank_name=bankname.getText().toString();

                Matcher m = p.matcher(mobile);
                Matcher m1=p.matcher(altmobile1);
                Matcher m2 = p1.matcher(pin);
                /*Matcher m3 = p2.matcher(aadhaar);*/
                Matcher m4 = p3.matcher(ifsc_code);
               // Matcher m5 = p4.matcher(blockcode1);
                //Matcher m6 = p5.matcher(distcode);
                Matcher m5 = p4.matcher(bankaccno1);


                boolean aadhaarvalid= VerhoeffAlgorithm.validateVerhoeff(aadhaar);
                String res=String.valueOf(aadhaarvalid);

                // for date
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                        Locale.US);
                String current_date=dateTime.format(cal.getTime());

                //validation starts here
                if(Fcode.equals("")){
                    farmerCode.setError("Please Enter Farmer Code");
                    farmerCode.requestFocus();
                }
                else if(fname.equals("")){
                    firstName.setError("Please Enter First Name");
                    firstName.requestFocus();
                }
                else if(lname.equals("")){
                    lastName.setError("Please Enter Last Name");
                    lastName.requestFocus();
                }
                else if(fathername.equals("")){
                    fatherName.setError("Please Enter Father Name");
                    fatherName.requestFocus();
                }
                else if(add1.equals("")){
                    presentAddr.setError("Please Enter Present Address");
                    presentAddr.requestFocus();
                }
                else if(add2.equals("")){
                    permanentAddr.setError("Please Enter Permanent Address");
                    permanentAddr.requestFocus();
                }
                else if(town1.equals("")){
                    town.setError("Please Enter Town");
                    town.requestFocus();
                }
                else if(dist.equals("")){
                    district.setError("Please Enter Town");
                    district.requestFocus();
                }
                else if(pin.equals(""))
                {
                    pincode.setError("Please Enter PinCode");
                    pincode.requestFocus();
                }
                else if(!m2.matches()){
                    pincode.setError("Please Enter Valid PinCode");
                    pincode.requestFocus();
                }
                else if(blockcode1.equals("")){
                    TextView errorText = (TextView)spinnerDropDown.getSelectedView();
                    errorText.setError("Please Enter Valid Block Code");
                    errorText.requestFocus();
                }
                else if(districtcode1.equals("")){
                    TextView errorText1 = (TextView)spinnerDropDown1.getSelectedView();
                    errorText1.setError("Please Enter Valid District Code");
                    errorText1.requestFocus();
                }
                else if(dob1.equals("")){
                    dob.setError("Please Enter Date of Birth");
                    dob.requestFocus();
                }
                else if(dob1.compareTo(current_date) >= 0){
                    dob.setError("Please Enter Date of Birth");
                    dob.requestFocus();}
                else if(mobile.equals(""))
                {
                    mobileno.setError("Please Enter Mobile Number");
                    mobileno.requestFocus();
                }
                else if(!m.matches()) {
                    mobileno.setError("Please Enter Valid Mobile Number");
                    mobileno.requestFocus();
                }
               /* else if(altmobile1.equals(""))
                {
                    altmobile.setError("Please Enter Mobile Number");
                    altmobile.requestFocus();
                }*/

                /*else if(altmobile1.length() > 10 || altmobile1.length() < 10){
                    altmobile.setError("Please Enter 10 Digits");
                    altmobile.requestFocus();
                }*/
                else if(bankaccno1.equals("")){
                    bankaccno.setError("Please Enter Bank Account Number");
                    bankaccno.requestFocus();

                }
               else if(!m5.matches()){
                    bankaccno.setError("Please Enter Valid Bank Account Number");
                    bankaccno.requestFocus();
                }
                else if(ifsc_code.equals("")){
                    ifsc.setError("Please Enter IFSC");
                    ifsc.requestFocus();
                }
                else if(!m4.matches()){
                    ifsc.setError("Please Enter Valid IFSC");
                    ifsc.requestFocus();
                }
                else if(bank_name.equals("")){
                    bankname.setError("Please Enter Bank Name");
                    bankname.requestFocus();
                }
                else if(aadhaar.equals("")){
                    aadhaarno.setError("Please Enter Aadhaar Number");
                    aadhaarno.requestFocus();
                }
                else if(res=="false"){
                    aadhaarno.setError("Please Enter Valid Aadhaar Number");
                    aadhaarno.requestFocus();
                }
                else
                {
                    ArrayList<ContentValues> list = new ArrayList<ContentValues>();

                    ContentValues c= new ContentValues();
                    c.put("FARMER_CODE", Fcode);
                    list.add(c);

                    ContentValues c2 = new ContentValues();
                    c.put("FIRST_NAME", fname);
                    list.add(c2);

                    ContentValues c4 = new ContentValues();
                    c.put("LAST_NAME", lname);
                    list.add(c4);

                    ContentValues c5 = new ContentValues();
                    c.put("FATHERS_NAME", fathername);
                    list.add(c5);

                    ContentValues c6 = new ContentValues();
                    c.put("PRESENT_ADDRESS", add1);
                    list.add(c6);

                    ContentValues c7 = new ContentValues();
                    c.put("PERMANENT_ADDRESS", add2);
                    list.add(c7);

                    ContentValues c8 = new ContentValues();
                    c.put("TOWN", town1);
                    list.add(c8);

                    ContentValues c9 = new ContentValues();
                    c.put("DISTRICT", dist);
                    list.add(c9);

                    ContentValues c10 = new ContentValues();
                    c.put("PIN_CODE", pin);
                    list.add(c10);

                    ContentValues c11 = new ContentValues();
                    c.put("BLOCK_CODE", blockcode1);
                    list.add(c11);
                    ContentValues c12 = new ContentValues();
                    c.put("DISTRICT_CODE", districtcode1);
                    list.add(c12);

                    ContentValues c13 = new ContentValues();
                    c.put("DOB", dob1);
                    list.add(c13);

                    /*ContentValues c14 = new ContentValues();
                    c.put("PHONE_NO", phn);
                    list.add(c14);*/

                    ContentValues c15 = new ContentValues();
                    c.put("MOBILE_NO", mobile);
                    list.add(c15);

                    ContentValues c16 = new ContentValues();
                    c.put("ALT_MOBILE_NO", altmobile1);
                    list.add(c16);


                    ContentValues c17 = new ContentValues();
                    c.put("BANK_ACCOUNT_NO", bankaccno1);
                    list.add(c17);

                    ContentValues c18 = new ContentValues();
                    c.put("AADHAAR_NO", aadhaar);
                    list.add(c18);

                    ContentValues c19 = new ContentValues();
                    c.put("IFSC", ifsc_code);
                    list.add(c19);

                    ContentValues c20 = new ContentValues();
                    c.put("BANK_NAME", bank_name);
                    list.add(c20);

                    ContentValues c21 = new ContentValues();
                    c.put("VERSION",dbc.appVersion());
                    list.add(c21);

                    ContentValues c22 = new ContentValues();
                    c.put("INSERTED_DATE", dateTime.format(cal.getTime()));
                    list.add(c22);

                   /* ContentValues c23= new ContentValues();
                    c.put("ID",farmer_id);
                    list.add(c23);*/


                    /*ContentValues c19 = new ContentValues();
                    c.put("ID", farmerid1);
                    list.add(c19);*/

                    int resp = farmerDB.insert_tableData(list,"FARMER_DETAILS");

                    Log.d(TAG,"Response from FarmerDB in Insert Farmer_details table  :: "+resp);

                    if(resp==0)
                    {
                       /* int id=Integer.parseInt(farmer_id);
                        id=id+1;
                        paddymovementDB.setConfig("FARMER_REG_ID",Integer.toString(id));*/
                        Toast.makeText(FarmerRegistrationActivity.this, "Successfully created farmer:" + fname, Toast.LENGTH_LONG).show();
                        /*Intent gotopreScreen = new Intent(
                                getApplicationContext(), FarmerRegistrationActivity.class);
                        startActivity(gotopreScreen);*/
                    }
                    else
                    {
                        Toast.makeText(FarmerRegistrationActivity.this, "Failed created farmer:" + fname, Toast.LENGTH_LONG).show();
                    }

                }
            }
        });
    }

    public void getDate(View v) {
        flag = "dob";
        showDialog(ID_DATEPICKER);
    }


    @Override
    protected Dialog onCreateDialog(int id) {

        switch (id) {
            case ID_DATEPICKER:
                return new DatePickerDialog(this, myDateSetListener,
                        Integer.parseInt(myYear), (Integer.parseInt(myMonth)-1),
                        Integer.parseInt(myDay));
            default:
                return null;
        }
    }

    private DatePickerDialog.OnDateSetListener myDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(android.widget.DatePicker arg0, int year,
                              int monthOfYear, int dayOfMonth) {

            String month = String.valueOf(monthOfYear + 1);
            String day = String.valueOf(dayOfMonth);
            if (month.length() == 1)
                month = "0" + month;
            if (day.length() == 1)
                day = "0" + day;
                dob.setText(String.valueOf(year)+ "-" + month + "-" +day);
        }
    };

    /*for check box validation*/

    public void onCheckboxClicked(View view) {
        String check=presentAddr.getText().toString();
        //checkboxforpermanent=(CheckBox)findViewById(R.id.checkBox1);
        //boolean checked = checkboxforpermanent.isChecked();
        boolean checked = ((CheckBox) view).isChecked();

        if(checked){
            permanentAddr.setText(check);
            permanentAddr.setEnabled(false);
        }
        else {
            permanentAddr.setText("");
            permanentAddr.setEnabled(true);
        }

    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}

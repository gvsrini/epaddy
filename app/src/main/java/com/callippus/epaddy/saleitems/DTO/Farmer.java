package com.callippus.epaddy.saleitems.DTO;

/**
 * Created by l_mah on 07-02-2018.
 */

public class Farmer {


    public final String farmercode;
    public final String farmerfirstname;
    public final String farmerlastname;


    public Farmer(String farmercode, String farmerfirstname, String farmerlastname) {
        this.farmercode = farmercode;
        this.farmerfirstname = farmerfirstname;
        this.farmerlastname = farmerlastname;
    }
}

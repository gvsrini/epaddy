package com.callippus.epaddy.saleitems;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Lenovo on 07-06-2015.
 */
public class Preferences {
    public static boolean audioPref = false;
    public static String langPref = "0";
    public static boolean weighingPref = false;
    public static String rationCardPref = "manual";
    public static String remoteUrlPref = "http://hpdssvc.callippus.co.uk";
    Context context;

    public Preferences(Context context) {
        this.context = context;
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
        refresh(context, SP);
    }

    public static void refresh(Context context, SharedPreferences SP) {
        audioPref = SP.getBoolean("audioPref", true);
        langPref = SP.getString("langPref", "1");
        weighingPref = SP.getBoolean("weighingPref", true);
        rationCardPref = SP.getString("rationCardPref", "manual");
        remoteUrlPref = SP.getString("remoteUrlPref", "http://hpdssvc.callippus.co.uk");
    }
}

package com.callippus.epaddy.saleitems;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.FarmerDB;
import com.callippus.epaddy.resources.DBConnection;


import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Callippus on 20-02-2018.
 */

public class StockEntryActivity extends Activity{

    public static final String TAG="activity_stock_entry";
    public static final int SUCCESS = 0;
    public static final int ERROR = -1;
    TextView product_head;
    Context context = this;
    EditText quintals,kgs,grams,tot_bags,bag_weight,moisture;
    LinearLayout activity_stock_entry_xml;
    Button btn_back,submit;
    public ArrayList<String> list;
    FarmerDetailsActivity grd;
    String code,p_name,p_id;
    FarmerDB FDB = null;
    DBConnection dbc = null;
    SQLiteDatabase db;
    public static double total_qty;
    public int total_bag_count=0;

    public int getTotal_bag_count() {
        return total_bag_count;
    }

    public void setTotal_bag_count(int total_bag_count) {
        this.total_bag_count = total_bag_count;
    }

    public String each_bag_weight,mosture_per;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_entry);
        try {
            dbc = new DBConnection(this);
            FDB = new FarmerDB(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Preferences pref = new Preferences(getApplicationContext());
        activity_stock_entry_xml = (LinearLayout) findViewById(R.id.activity_stock_entryXML);

        Log.d(TAG,"code value in DetailedFarmerDetails is::"+code);

        p_name = ProductListViewActivity.product_name;
        p_id = ProductListViewActivity.product_id;     //////////getting data from Product List View screen


        Log.d(TAG,"Product Name from ProductListView Class ::"+p_name);
        show_details(p_name);

        submit=(Button)findViewById(R.id.btnsubmit_SE);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             int response1=isvailddata(); ////Check is valid data or not
                Log.d(TAG,"Resp from isValiddata in stockEntry screen :: "+response1);
                if(response1 == SUCCESS)
                {
                    setQuantity();
                    total_qty = 0.0;
                    Intent goToNextActivity = new Intent(
                            getApplicationContext(), ProductListViewActivity.class);
                    startActivity(goToNextActivity);

                }

            }
        });

    }
    public void show_details(String product_name)
    {
        product_head=(TextView)findViewById(R.id.tvheading_product);
        grams=(EditText) findViewById(R.id.etgrams);
        kgs=(EditText) findViewById(R.id.etkgs);
        quintals =(EditText) findViewById(R.id.etquintals);

        Log.d(TAG,"list value in show details::"+product_name.toString());
        product_head.setText(product_name.toString());
        grams.setText("0");
        kgs.setText("0");
        quintals .setText("0");
    }

    public  void setQuantity()
    {
        quintals = (EditText)findViewById(R.id.etquintals);
        kgs= (EditText)findViewById(R.id.etkgs);
        grams= (EditText)findViewById(R.id.etgrams);
        tot_bags = (EditText)findViewById(R.id.ettot_bag);
        bag_weight = (EditText)findViewById(R.id.etone_bag_qty);
        moisture = (EditText)findViewById(R.id.etmoisture);

        int qty_quintals =Integer.parseInt(quintals.getText().toString());
        int qty_kgs=Integer.parseInt(kgs.getText().toString());
        int qty_grams=Integer.parseInt(grams.getText().toString());
        total_bag_count =Integer.parseInt(tot_bags.getText().toString());
        each_bag_weight = bag_weight.getText().toString();
        mosture_per=moisture.getText().toString();


        total_qty = (qty_quintals*100)+(qty_kgs)+(qty_grams*0.001);

      insert_stock_in_process();

    }

    public void insert_stock_in_process()
    {
        ContentValues values = new ContentValues();
        values.put("ITEM_ID", p_id);
        values.put("TOTAL_QTY", total_qty);
        values.put("BAG_COUNT", total_bag_count);
        values.put("ITEM_NAME", p_name);
        values.put("TRUCK_MEMO_ID", 0);
        values.put("BAG_WEIGHT", each_bag_weight);
        values.put("MOISTURE", mosture_per);

        Log.d(TAG,"Values :: "+values);

        int row_count = FDB.check_rowexists(p_id);
        if(row_count > 0)
        {
           FDB.update_stock_in_process(p_id,total_qty,total_bag_count,"0",each_bag_weight,mosture_per);
        }
        else
        {
            FDB.insert_stockinprocess(values);
        }

    }

    public int isvailddata()
    {

        quintals= (EditText)findViewById(R.id.etquintals);
        kgs= (EditText)findViewById(R.id.etkgs);
        grams= (EditText)findViewById(R.id.etgrams);
        tot_bags = (EditText)findViewById(R.id.ettot_bag);
        bag_weight = (EditText)findViewById(R.id.etone_bag_qty);
        moisture = (EditText)findViewById(R.id.etmoisture);

        String qtyPattern = "\\d+";
        String qty_kgsPattern = "\\d+";
        String qty_gramsPattern = "\\d+";

        Pattern p = Pattern.compile(qtyPattern);
        Pattern p1 = Pattern.compile(qty_kgsPattern);
        Pattern p2 = Pattern.compile(qty_gramsPattern);

        String qty_q = quintals.getText().toString();
        String qty_kgs = kgs.getText().toString();
        String qty_grams = grams.getText().toString();
        String count=tot_bags.getText().toString();
        String bagweight=bag_weight.getText().toString();
        String per=moisture.getText().toString();


        Matcher m = p.matcher(qty_q);
        Matcher m1 = p1.matcher(qty_kgs);
        Matcher m2 = p2.matcher(qty_grams);

        Matcher m3 = p.matcher(count);
        Matcher m4 = p2.matcher(bagweight);
        Matcher m5= p1.matcher(per);


        if((!m.matches())||(qty_q.equals(""))){
            quintals.setError("Please Enter Valid Quantity");
            quintals.requestFocus();
            return ERROR;
        }
        else if((!m1.matches())||(qty_kgs.equals(""))){
            kgs.setError("Please Enter Valid Quantity");
            kgs.requestFocus();
            return ERROR;
        }
        else if((!m2.matches())||(qty_grams.equals(""))){
            grams.setError("Please Enter Valid Quantity");
            grams.requestFocus();
            return ERROR;
        }
        else if((!m3.matches())||(count.equals(""))){
            tot_bags.setError("Please Enter Valid Bag Count");
            tot_bags.requestFocus();
            return ERROR;
        }
        else if((!m4.matches())||(bagweight.equals(""))){
            bag_weight.setError("Please Enter Valid Bag Weight");
            bag_weight.requestFocus();
            return ERROR;
        }
        else if((!m5.matches())||(per.equals(""))){
            moisture.setError("Please Enter Valid Moisture Percentage");
            moisture.requestFocus();
            return ERROR;
        }

       return SUCCESS;

    }
}

package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.FarmerDB;
import com.callippus.epaddy.resources.DBConnection;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by callippus on 14/2/18.
 */

public class DetailedFarmerDetails extends Activity {

    public static final String TAG="detailed_farmer_details";
    TextView farmerCode,firstName,lastName,fatherName,presentAddr,permanentAddr,town;
    TextView district,pincode,blockcode,districtcode,dob,mobileno,altmobile,bankaccno,ifsc,bankname,aadhaarno;
    LinearLayout detailed_farmerdetails_xml;
   /* Button btn_back;*/
    public ArrayList<String> list;
    FarmerDetailsActivity grd;
    String code;
    FarmerDB FDB = null;
    DBConnection dbc = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailed_farmerdetails);
        /*ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/
        try {
            dbc = new DBConnection(this);
            FDB = new FarmerDB(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Preferences pref = new Preferences(getApplicationContext());
        detailed_farmerdetails_xml = (LinearLayout) findViewById(R.id.detailed_farmerdetailsXML);
       // farmer_details=FDB.populateFarmerDetails(code);
      code=grd.farmer_code;

        Log.d(TAG,"code value in DetailedFarmerDetails is::"+code);
        list = FDB.populateFarmerDetails(code);
        show_details(list);
//        farmerCode=(TextView)findViewById(R.id.tvdfcode_DF);
        /*btn_back=(Button)findViewById(R.id.btnback_DF);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent goToNextActivity = new Intent(
                        getApplicationContext(), FarmerDetailsActivity.class);
                startActivity(goToNextActivity);
            }
        });*/

       }

       public void show_details(ArrayList<String> list1)
       {

           farmerCode=(TextView)findViewById(R.id.tvfcode_DF);
           firstName=(TextView)findViewById(R.id.tvfirstname_DF);
           lastName=(TextView)findViewById(R.id.tvlastname_DF);
           fatherName=(TextView)findViewById(R.id.tvfathername_DF);
           presentAddr=(TextView)findViewById(R.id.tvpresentadd_DF);
           permanentAddr=(TextView)findViewById(R.id.tvpermentadd_DF);
           town=(TextView)findViewById(R.id.tvtown_DF);
           district=(TextView)findViewById(R.id.tvdistrict_DF);
           pincode=(TextView)findViewById(R.id.tvpincode_DF);
           blockcode=(TextView)findViewById(R.id.tvblockcode_DF);
           districtcode=(TextView)findViewById(R.id.tvdistrictcode_DF);
           dob=(TextView)findViewById(R.id.tvdob_DF);
           mobileno=(TextView)findViewById(R.id.tvmobileno_DF);
           altmobile=(TextView)findViewById(R.id.tvaltmobileno_DF);
           bankaccno=(TextView)findViewById(R.id.tvbankaccno_DF);
           ifsc=(TextView)findViewById(R.id.tvifsc_DF);
           bankname=(TextView)findViewById(R.id.tvbankname_DF);
           aadhaarno=(TextView)findViewById(R.id.tvaadhar_DF);

           Log.d(TAG,"list value in show details::"+list1.get(0).toString());
           farmerCode.setText(list1.get(0).toString());
           firstName.setText(list1.get(1).toString());
           lastName.setText(list1.get(2).toString());
           fatherName.setText(list1.get(3).toString());
           presentAddr.setText(list1.get(4).toString());
           permanentAddr.setText(list1.get(5).toString());
           town.setText(list1.get(6).toString());
           district.setText(list1.get(7).toString());
           pincode.setText(list1.get(8).toString());
           blockcode.setText(list1.get(9).toString());
           districtcode.setText(list1.get(10).toString());
           dob.setText(list1.get(11).toString());
           mobileno.setText(list1.get(12).toString());
           altmobile.setText(list1.get(13).toString());
           bankaccno.setText(list1.get(14).toString());
           ifsc.setText(list1.get(15).toString());
           bankname.setText(list1.get(16).toString());
           aadhaarno.setText(list1.get(17).toString());
       }


    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

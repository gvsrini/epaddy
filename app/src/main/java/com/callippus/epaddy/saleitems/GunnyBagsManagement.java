package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.FarmerDB;
import com.callippus.epaddy.dbfunctions.PaddyMovementDB;
import com.callippus.epaddy.resources.DBConnection;
import com.callippus.epaddy.resources.Utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Callippus on 12-02-2018.
 */

public class GunnyBagsManagement extends Activity {

    private String myYear, myMonth, myDay;
    static final int ID_DATEPICKER = 0;
    Calendar c = Calendar.getInstance();
    SimpleDateFormat dateTime = new SimpleDateFormat("yyyy-MM-dd",
            Locale.US);
    String current_date= dateTime.format(c.getTime());
    String flag=null,gunny_type_list;
    Spinner gunnytype;
    public static final String TAG="In Gunny Bags Details :";
    public static String un = null, pwd = null;
    final Context context = this;
    Button submit;
    EditText date,gunny_type,gunny_rec,gunny_utilised;
    EditText district;
    LinearLayout gunny_bags_xml;
    FarmerDB farmerDB = null;
    PaddyMovementDB paddymovementDB = null;
    DBConnection dbc = null;
    ArrayList<String> gunny_types;
    ArrayList<String> gunnyOpenCloseDetails;
    Utils u = new Utils();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gunnybags_management);
     /*   ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        Preferences pref = new Preferences(getApplicationContext());

        gunny_bags_xml = (LinearLayout) findViewById(R.id.gunnybagsXML);
        submit = (Button) findViewById(R.id.submit_GBD);
        //farmerid=(EditText)findViewById(R.id.FARMERID);
        date=(EditText)findViewById(R.id.date_GBD);
        gunnytype = (Spinner) findViewById(R.id.gunnytype_GBD);
        gunny_rec = (EditText) findViewById(R.id.gunny_rec_GBD);
        gunny_utilised = (EditText) findViewById(R.id.gunnyuse_GBD);


        try {
            dbc = new DBConnection(this);
            farmerDB = new FarmerDB(this);
            paddymovementDB = new PaddyMovementDB(this);
        } catch (IOException e) {
            e.printStackTrace();
        }

        myYear = Integer.toString(c.get(Calendar.YEAR));
        myMonth = Integer.toString(c.get(Calendar.MONTH));
        myDay = Integer.toString(c.get(Calendar.DAY_OF_MONTH));
        myMonth = Integer.toString(Integer.parseInt(myMonth) + 1);

        if (myDay.length() == 1)
            myDay = "0" + myDay;
        if (myMonth.length() == 1)
            myMonth = "0" + myMonth;


        gunny_types=farmerDB.getgunny_types();
        ArrayAdapter<String> adapter= new ArrayAdapter<String>(this,android.
                R.layout.simple_spinner_dropdown_item ,gunny_types);

        gunnytype.setAdapter(adapter);

        gunnytype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                                      @Override
                                                      public void onItemSelected(AdapterView<?> parent, View view,
                                                                                 int position, long id) {
                                                          int sid = gunnytype.getSelectedItemPosition();
                                                          gunny_type_list = gunny_types.get(sid);


                                                      }

                                                      @Override
                                                      public void onNothingSelected(AdapterView<?> parent) {
                                                          // TODO Auto-generated method stub
                                                      }
                                                  });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                String MobilePattern = "[0-9]+$";
                Pattern p = Pattern.compile(MobilePattern);


                String Date_user = date.getText().toString();
//                String GunnyType = gunny_type.getText().toString();
                String Gunny_Rec = gunny_rec.getText().toString();
                String Gunny_Use = gunny_utilised.getText().toString();

                Matcher m1 = p.matcher(Gunny_Rec);
                Matcher m2 = p.matcher(Gunny_Use);

                String gunny_id = paddymovementDB.getConfiguration("GUNNY_BAG_ID");



                //validation starts here

                Log.d(TAG,"The entered user date is : "+Date_user);
                Log.d(TAG,"System date is  : "+current_date);


                if(Date_user.equals("")){
                    date.setError("Please Select Date");
                    date.requestFocus();
                }
                else if (Date_user.compareTo(current_date) > 0) {
                    date.setError("Please Select Vaild date");
                    date.requestFocus();
                }
                else if(gunny_type_list.equals("")){
                    TextView errorText = (TextView)gunnytype.getSelectedView();
                    errorText.setError("Please Select Gunny Type");
                    errorText.requestFocus();
                }
                else if((Gunny_Rec.equals("")) || (!m1.matches())){
                    gunny_rec.setError("Please Enter Received Bag Details");
                    gunny_rec.requestFocus();
                }
                else if((Gunny_Use.equals("")) || (!m2.matches())){
                    gunny_utilised.setError("Please Enter Used Bag Details");
                    gunny_utilised.requestFocus();
                }
                else
                {
                    ArrayList<ContentValues> list = new ArrayList<ContentValues>();

                    ContentValues c= new ContentValues();
                    c.put("DATE", Date_user);
                    list.add(c);

                    ContentValues c2 = new ContentValues();
                    c.put("GUNNY_TYPE", gunny_type_list);
                    list.add(c2);

                    ContentValues c4 = new ContentValues();
                    c.put("GUNNY_REC", Gunny_Rec);
                    list.add(c4);

                    ContentValues c5 = new ContentValues();
                    c.put("GUNNY_USE", Gunny_Use);
                    list.add(c5);

                    ContentValues c6 = new ContentValues();
                    c.put("STATUS","1");
                    list.add(c6);

                    ContentValues c7 = new ContentValues();
                    c.put("SYNC_STATUS","0");
                    list.add(c7);

                    ContentValues c8= new ContentValues();
                    c.put("ID",gunny_id);
                    list.add(c8);

                    String table_name="GUNNY_BAG_DETAILS";
                    int resp = farmerDB.insert_tableData(list,table_name);

                    Log.d(TAG,"Response from FarmerDB in Insert GUNNY BAG DETAILS table  :: "+resp);

                    if(resp == 0)
                    {
                        int id=Integer.parseInt(gunny_id);
                        id=id+1;
                        paddymovementDB.setConfig("GUNNY_BAG_ID",Integer.toString(id));


                        String table_name1="GUNNY_INVENTORY";
                        gunnyOpenCloseDetails=farmerDB.getGunnyInventoryOpenCloseDetails(gunny_id);
                        String date1=gunnyOpenCloseDetails.get(0);
                        String gunnyopen=gunnyOpenCloseDetails.get(1);
                        String gunnyclose=gunnyOpenCloseDetails.get(2);
                        if(date1.compareTo(Date_user)  <= 0){
                            int gunny_recieved=Integer.parseInt(Gunny_Rec);
                            int gunny_close_existed=Integer.parseInt(gunnyclose);
                            int gunny_close_latest=gunny_recieved+gunny_close_existed;
                            String gunnycloseupdate=String.valueOf(gunny_close_latest);

                         int result=farmerDB.updateGunnyInventory(gunny_id,gunnycloseupdate);

                        }

                        Toast.makeText(GunnyBagsManagement.this, "Successfully Inserted. " , Toast.LENGTH_LONG).show();
                        Intent gotopreScreen = new Intent(
                                getApplicationContext(), GridLayoutActivity.class);
                        startActivity(gotopreScreen);
                    }
                    else
                    {
                        Toast.makeText(GunnyBagsManagement.this, "Failed to insert gunny bag details " , Toast.LENGTH_LONG).show();
                    }

                }
            }
        });
    }

    public void getDate(View v) {
        flag = "date";
        showDialog(ID_DATEPICKER);
    }


    @Override
    protected Dialog onCreateDialog(int id) {

        switch (id) {
            case ID_DATEPICKER:
                return new DatePickerDialog(this, myDateSetListener,
                        Integer.parseInt(myYear), (Integer.parseInt(myMonth)-1),
                        Integer.parseInt(myDay));
            default:
                return null;
        }
    }

    private DatePickerDialog.OnDateSetListener myDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(android.widget.DatePicker arg0, int year,
                              int monthOfYear, int dayOfMonth) {

            String month = String.valueOf(monthOfYear + 1);
            String day = String.valueOf(dayOfMonth);
            if (month.length() == 1)
                month = "0" + month;
            if (day.length() == 1)
                day = "0" + day;
            date.setText(String.valueOf(year) + "-" + month + "-" + day);
        }
    };


    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}

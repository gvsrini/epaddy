package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.callippus.epaddy.R;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by srini_000 on 15-06-2015.
 */


public class VehicleMovementReports extends Activity {

    static final int ID_DATEPICKER = 0;
    final Context mcontext = this;
    Button b1, b2;
    SimpleDateFormat date = new SimpleDateFormat("ddMMyyyy");
    Calendar cal = Calendar.getInstance();
    EditText frDate;
    EditText toDate;
    private int myYear, myMonth, myDay;
    private Button.OnClickListener datePickerButtonOnClickListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            final Calendar c = Calendar.getInstance();
            myYear = c.get(Calendar.YEAR);
            myMonth = c.get(Calendar.MONTH);
            myDay = c.get(Calendar.DAY_OF_MONTH);
            showDialog(ID_DATEPICKER);
        }
    };
    private DatePickerDialog.OnDateSetListener myDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            String month = String.valueOf(monthOfYear + 1);
            String day = String.valueOf(dayOfMonth);
            if (month.length() == 1)
                month = "0" + month;
            if (day.length() == 1)
                day = "0" + day;
            // TODO Auto-generated method stub

            frDate.setText(day + month + String.valueOf(year));
            toDate.setText(day + month + String.valueOf(year));

        }
    };

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        switch (id) {
            case ID_DATEPICKER:
                return new DatePickerDialog(this, myDateSetListener, myYear,
                        myMonth, myDay);
            default:
                return null;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicle_movement_reports);
        /*ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        b1 = (Button) findViewById(R.id.btn1_VMR);
        b2 = (Button) findViewById(R.id.btn2_VMR);

        b1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                /*Intent i = new Intent(
                        getApplicationContext(), DailyReport.class);*/
                Intent i = new Intent(
                        getApplicationContext(), VehicleMovementDailyReport.class);
                startActivity(i);
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {

                                  @Override
                                  public void onClick(View arg0) {
                                      // TODO Auto-generated method stub

                                      Intent i = new Intent(
                                              getApplicationContext(), VehicleMovementMonthlyReport.class);
                                      startActivity(i);

                                  }
                              }
        );
    }

    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
package com.callippus.epaddy.saleitems;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.FarmerDB;
import com.callippus.epaddy.dbfunctions.MainActivityDAO;
import com.callippus.epaddy.dbfunctions.PaddyMovementDB;
import com.callippus.epaddy.interfaces.OnProductsEntry;
import com.callippus.epaddy.resources.DBConnection;
import com.callippus.epaddy.saleitems.ProductDetailsAdapter;

import java.util.ArrayList;

/**
 * Created by Callippus on 15-02-2018.
 */

public class ProductListViewActivity extends Activity implements OnProductsEntry{
    private final static int MESSAGE_BOX = 1;
    public final static String TAG="stock_entry_to_truck";
    public static final int SUCCESS=0;
    public static final int ERROR = -1;
    /* Handler to display UI response messages   */
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case MESSAGE_BOX:
                    String str = (String) msg.obj;
                    showDialog(str);
                    break;
                default:
                    break;
            }
        }

        private void showDialog(String str) {
        }
    };



    OnProductsEntry mCallback1;
    ListView productListView = null;
    Context context = this;
    FarmerDB FDB = null;
    DBConnection dbc = null;
    MainActivityDAO mainActivityDAO = null;
    StockEntryActivity sea=null;
    PaddyMovementDB paddymovementDB;
    public ArrayList<String[]> productList = new ArrayList<String[]>();
    public ProductDetailsAdapter productListAdapter;
    public static String product_id,product_name,product_qty;
    Button submit;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_entry_to_truck);
        Log.d(Constants.TAG, "About to create member activity");

        try {
            dbc = new DBConnection(this);
            FDB = new FarmerDB(this);
            paddymovementDB = new PaddyMovementDB(this);
            mainActivityDAO = new MainActivityDAO(this);
        } catch (Exception e) {
            //Show an alert dialog here.

            handler.obtainMessage(MESSAGE_BOX, "Exception opening DB" + e.getMessage()).sendToTarget();
            e.printStackTrace();
            return;
        }

        DBConnection db = null;
        Log.d(TAG,"in stock entry screen");
        productListView = (ListView) findViewById(R.id.productList_SET);
        productList = mainActivityDAO.getProduct_details(productList);
        productListAdapter = new ProductDetailsAdapter(this, productList);
        Log.d(TAG,"productListAdapter value: "+productListAdapter);
        productListView.setAdapter(productListAdapter);
        Log.d(TAG,"after productListView setting adapter ");


        productListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

          @Override
           public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                   long arg3) {
               // i = i + 1;
                arg1.setSelected(true);
               String product[] = productListAdapter.getItem(arg2);
               mCallback1 = ProductListViewActivity.this;
               mCallback1.onProductClick(productListAdapter.getItem(arg2));
            }
        });

        submit = (Button)findViewById(R.id.btnsubmit_PLV);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int res = insertStockQty();
                if(res==0) {
                    Intent goToNextActivity = new Intent(
                            getApplicationContext(), PaddyMovementActivity.class);
                    startActivity(goToNextActivity);
                }
                else
                {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setTitle("Paddy Procurement");
                    alert.setMessage("Please Select Product In List")
                            .setCancelable(false)
                            .setNeutralButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alertDialog1 = alert.create();
                    alertDialog1.show();
                }
            }
        });
    }
    public void onProductClick( String[] product) {

        product_id=product[0];
        product_name=product[1];
        Log.d(TAG,"product[0] id::"+product_id);
        Log.d(TAG,"product[0] value::"+product_name);

        Intent goToNextActivity = new Intent(
                getApplicationContext(), StockEntryActivity.class);
        startActivity(goToNextActivity);

    }

    public int insertStockQty() {
        double qty = mainActivityDAO.getqty_from_stock_in_process();

        Log.d(TAG,"Total qty in Stock In Process :: "+qty);

        if (qty > 0) {
            Log.d(TAG, "Truck Number :: " + TruckNumberInputActivity.truck_number + " And Total qty :: " + qty);
            int response = paddymovementDB.insert_stock_header(qty, TruckNumberInputActivity.truck_number);

            return response;
        }

        return  ERROR;

    }


}

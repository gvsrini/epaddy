package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.ReportsDB;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import vtek.GL11.AndroidPrinter;

/**
 * Created by callippus on 21/2/18.
 */

public class GunnySummaryReportActivity extends Activity{

    static{
        System.loadLibrary("printerfinal");
    }

    AndroidPrinter ap = new AndroidPrinter();

    public static final String TAG="GunnySummaryreport::";
    public String report_date;
    TextView fr, to,title,sub_title;
    TableLayout reporttable;
    String flag = null;
    private String myYear, myMonth, myDay;
    static final int ID_DATEPICKER = 0;
    Calendar c = Calendar.getInstance();
    Cursor cr;
    ReportsDB rDB;
    SQLiteDatabase database;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gunny_summary_reports);

      /*  ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        fr = (TextView) findViewById(R.id.frDt_GSR);
        /*to = (TextView) findViewById(R.id.toDt_SSR);*/
        reporttable = (TableLayout) findViewById(R.id.reportGunnySummaryTable);

       /* title = (TextView) findViewById(R.id.Title);
        title.setText("Daily Report");*/

        try {
            rDB = new ReportsDB(getApplicationContext());
            database = rDB.myDataBase;
        } catch (IOException e) {
            e.printStackTrace();
        }
        myYear = Integer.toString(c.get(Calendar.YEAR));
        myMonth = Integer.toString(c.get(Calendar.MONTH));
        myDay = Integer.toString(c.get(Calendar.DAY_OF_MONTH));
        myMonth = Integer.toString(Integer.parseInt(myMonth) + 1);

        if (myDay.length() == 1)
            myDay = "0" + myDay;
        if (myMonth.length() == 1)
            myMonth = "0" + myMonth;

    }

    @SuppressWarnings("deprecation")
    public void getFrDt_GSR(View v) {
        flag = "from";
        showDialog(ID_DATEPICKER);
    }

    @SuppressWarnings("deprecation")
    public void getToDt(View v) {
        flag = "to";
        showDialog(ID_DATEPICKER);
    }

    public void getsummaryRpt(View v) {
        summaryReport();
       /* sub_title = (TextView) findViewById(R.id.SubTitle);
        sub_title.setText("Report from: "+fr.getText().toString() +" to: "+to.getText().toString() );*/

    }

    public  void getPrint(View v) {

        print_summary_data();
        //rDB.getImage(reporttable);
    }

    private void summaryReport() {

        try {
            reporttable.removeAllViews();
            String fromdate=fr.getText().toString();
            String date=fromdate.substring(0,2);
            String month=fromdate.substring(3,5);
            String year=fromdate.substring(6,10);
            String fr1=year.concat("-").concat(month).concat("-").concat(date);
            report_date=fr1;
            cr = rDB.getGunnySummaryReport(fr1);
           /* Float qty=0.0f, price = 0.0f, tot_qty=0.0f, tot_price = 0.0f;*/
            reporttable.addView(rDB.GunnySummaryTitleHead(new String[]{"ID","GUNNYTYPE","OPENING","CLOSING"}));


            while (cr.moveToNext()) {

                LayoutInflater inflater = LayoutInflater
                        .from(getApplicationContext());
                TableRow row = (TableRow) inflater.inflate(R.layout.activity_gunny_summary_report_rows,
                        null);

                while(row.getChildCount()>cr.getColumnCount())
                    row.removeViewAt(row.getChildCount()-1);

                TextView one = (TextView) row.findViewById(R.id.r1_GSR);
                TextView two = (TextView) row.findViewById(R.id.r2_GSR);
                TextView three = (TextView) row.findViewById(R.id.r3_GSR);
                TextView four = (TextView) row.findViewById(R.id.r4_GSR);


                reporttable.addView(row);

                one.setText(cr.getString(0));
                two.setText(cr.getString(1));
                three.setText(cr.getString(2));
                four.setText(cr.getString(3));
            }
           /* reporttable.addView(rDB.HeaderFooterRows(new String[]{"Total", "",Float.toString(tot_qty),Float.toString(tot_price)}));*/


        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        switch (id) {
            case ID_DATEPICKER:
                return new DatePickerDialog(this, myDateSetListener,
                        Integer.parseInt(myYear), (Integer.parseInt(myMonth)-1),
                        Integer.parseInt(myDay));
            default:
                return null;
        }
    }

    private DatePickerDialog.OnDateSetListener myDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(android.widget.DatePicker arg0, int year,
                              int monthOfYear, int dayOfMonth) {

            String month = String.valueOf(monthOfYear + 1);
            String day = String.valueOf(dayOfMonth);
            if (month.length() == 1)
                month = "0" + month;
            if (day.length() == 1)
                day = "0" + day;

            if (flag.equals("from"))
                fr.setText(day+ "/" + month + "/" + String.valueOf(year));
//            else if (flag.equals("to"))
//                to.setText((day+ "/" + month + "/" + String.valueOf(year)));
        }
    };

    public   void print_summary_data()
    {
        ap.prn_open();
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        String today = date.format(cal.getTime());
        String UID =  android.os.Build.SERIAL ;
        Log.d(TAG,"Devicce uid :: "+UID);
        System.out.println("UID :"+UID);
        String h="Gunny Summary Report\n\n";
        ap.prn_write_text(h,h.length(),2);
        String head ="DPC_Name = Callippus\n";
        head +="DPC_ID = Bhasker PP\n";
        head +="Dist = HYD\n";
        head +="Block = KPHB\n";
        head +="........................................\n";
        head +="Report Date :"+today+"\n";
        head +=".........................................\n";
        head += "ID    GUNNYTYPE      OPENING    CLOSING\n";
        ap.prn_write_text(head,head.length(),1);
        String str=".........................................\n";
        ap.prn_write_text(str,str.length(),1);
        Log.d(TAG,"report data  :: "+report_date);
        cr = rDB.getGunnySummaryReport(report_date);

        String data="";

        while (cr.moveToNext()) {

           data += cr.getString(0)+"      "+cr.getString(1)+"           "+cr.getString(2)+"         "+cr.getString(3)+"\n";

        }
        Log.d(TAG,"Data in gunny summary report :: "+data);
        ap.prn_write_text(data,data.length(),1);
         str=".......................................\n";
        ap.prn_write_text(str,str.length(),1);
        ap.prn_paper_feed(10);
        ap.prn_close();

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.callippus.epaddy.R;


/**
 * Created by l_mah on 06-02-2018.
 */


public class FarmerManagementActivity extends Activity {


    public static final String TAG = "FarmerDetails";
    public Button farmerreg, getfarmerdetails,editformardetails;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmerdetails);
       /* ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        //farmerreg = (Button) findViewById(R.id.btnfarmerreg_FD);
        //getfarmerdetails = (Button) findViewById(R.id.btnfarmerdetails_FD);
        ImageView farmerreg = (ImageView) findViewById(R.id.btnfarmerreg_FD);
        ImageView getfarmerdetails = (ImageView) findViewById(R.id.btnfarmerdetails_FD);
        ImageView modifydetails = (ImageView) findViewById(R.id.btnmodifyfarmer_FD);

        ImageView deletefarmer = (ImageView) findViewById(R.id.btndeletefarmer);


        farmerreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Intent goToNextActivity = new Intent(
                        getApplicationContext(), FarmerRegistrationActivity.class);
                startActivity(goToNextActivity);
            }

        });



        getfarmerdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Intent goToNextActivity = new Intent(
                        getApplicationContext(), FarmerDetailsActivity.class);
                startActivity(goToNextActivity);
            }

        });



        modifydetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Log.d(TAG,"On Click here editformardetails button --->");
                Intent goToNextActivity = new Intent(
                        getApplicationContext(), EditFarmerDetailsActivity.class);
                startActivity(goToNextActivity);

                Log.d(TAG,"On Click here =====> editformardetails button --->");
            }

        });

        deletefarmer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent goToNextActivity = new Intent(
                        getApplicationContext(), DeleteFarmerDetails.class);
                startActivity(goToNextActivity);
            }

        });


    }
    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
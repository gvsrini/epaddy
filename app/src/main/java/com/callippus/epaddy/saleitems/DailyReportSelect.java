package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.callippus.epaddy.R;


/**
 * Created by callippus on 21/2/18.
 */

public class DailyReportSelect extends Activity {


    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.daily_report_select);
       /* ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/



        ImageView img_farmer_daily_report = (ImageView) findViewById(R.id.img_farmer_details_report_DRS);
        img_farmer_daily_report.setOnClickListener(new View.OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity = new Intent(
                        getApplicationContext(),FarmerDetailsReport.class);
                startActivity(goToNextActivity);
            }
        });


    }


    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

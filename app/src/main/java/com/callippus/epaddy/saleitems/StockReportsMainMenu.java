package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.callippus.epaddy.R;

/**
 * Created by callippus on 26/2/18.
 */

public class StockReportsMainMenu extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stock_reports_main_menu);
        /*ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true)*/;

        ImageView stock_summary_reports = (ImageView) findViewById(R.id.image1_SRMM);
        stock_summary_reports.setOnClickListener(new View.OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity = new Intent(
                        getApplicationContext(), StockSummaryReport.class);
                startActivity(goToNextActivity);
            }
        });

        ImageView stock_detailed_reports = (ImageView) findViewById(R.id.image2_SRMM);
        stock_detailed_reports.setOnClickListener(new View.OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity = new Intent(
                        getApplicationContext(), StockDetailedReportDetails.class);
                startActivity(goToNextActivity);
            }
        });
    }


    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
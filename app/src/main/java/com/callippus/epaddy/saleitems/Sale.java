package com.callippus.epaddy.saleitems;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.callippus.epaddy.R;
import com.callippus.epaddy.resources.DBConnection;
import com.callippus.epaddy.saleitems.SaleBar;

import java.io.IOException;

public class Sale extends SaleBar {

	LinearLayout menuXML, homepage;
	DBConnection db;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		 
		try {
			db = new DBConnection(this);
			} catch (IOException e) {
			e.printStackTrace();
		}

		LayoutInflater inflater = LayoutInflater.from(this);
		homepage = (LinearLayout) inflater
				.inflate(R.layout.menu_homepage, null);

		TextView welcomeText = (TextView) homepage.findViewById(R.id.welcome);
		TextView dev = (TextView) homepage.findViewById(R.id.dev);
		TextView RN = (TextView) homepage.findViewById(R.id.RN);
		TextView RL = (TextView) homepage.findViewById(R.id.RL);

        welcomeText.setText("Welcome To \n PDS Android Application\n"
                + db.VersionNo + "\n");
		dev.setText("Device Id : "
				+ Secure.getString(this.getContentResolver(), Secure.ANDROID_ID));

        setContentView(homepage);
	}
}

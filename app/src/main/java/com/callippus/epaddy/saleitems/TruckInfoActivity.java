package com.callippus.epaddy.saleitems;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.callippus.epaddy.R;
import com.callippus.epaddy.resources.DBConnection;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Callippus on 15-02-2018.
 */

public class TruckInfoActivity extends Activity {

    TruckNumberInputActivity TNI;
    TextView  trucknumber,trucktype,contractorname,drivername,drivermobileno;
    public static final String TAG = "Truck Info : ";
    public ArrayList<String> list;
    String truck_number = TNI.str;
    DBConnection dbc = null;
    LinearLayout truckinfo_xml;
    Button Next;
    final Context context = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.truckinfo);
        try {
            dbc = new DBConnection(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
        Preferences pref = new Preferences(getApplicationContext());
         truckinfo_xml = (LinearLayout) findViewById(R.id.truck_info_XML);

        Log.d(TAG, "code value in DetailedFarmerDetails is:: " + truck_number);
     list = dbc.poopulateTruckDetails(truck_number);
        if(list.size() > 0)
              display_data(list);
        else
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setTitle("Paddy Procurement");
            alert.setMessage("No Data Found With Entered Truck Number")
                    .setCancelable(false)
                    .setNeutralButton("OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(
                                        DialogInterface dialog,
                                        int which) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog1 = alert.create();
            alertDialog1.show();
        }

        Next = (Button) findViewById(R.id.btnnext_TI);


         Next.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {

                 Intent goToNextActivity = new Intent(
                         getApplicationContext(), ProductListViewActivity.class);
                 startActivity(goToNextActivity);
             }
         });

    }

    public void display_data(ArrayList<String> list1)
    {
        trucknumber=(TextView)findViewById(R.id.tv_num_TI);
           trucktype=(TextView)findViewById(R.id.tv_type_TI);
          contractorname=(TextView)findViewById(R.id.tv_tv_contractor_name_TI);
           drivername=(TextView)findViewById(R.id.tv_driver_name_TI);
         drivermobileno=(TextView)findViewById(R.id.tv_driver_mobile_no_TI);



        Log.d(TAG,"list value in show details::"+list1);
        trucknumber.setText(list1.get(0).toString());
        trucktype.setText(list1.get(1).toString());
        contractorname.setText(list1.get(2).toString());
        drivername.setText(list1.get(3).toString());
        drivermobileno.setText(list1.get(4).toString());


    }


}
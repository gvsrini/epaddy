package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.callippus.epaddy.R;


/**
 * Created by callippus on 14/2/18.
 */

public class PaddyMovementActivity extends Activity {
    public static final String TAG = "Paddy_Movement";


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paddymovement);
       /* ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true)*/;

        ImageView vehicle_details = (ImageView) findViewById(R.id.img_vehicle_details_PM);

        vehicle_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Intent goToNextActivity = new Intent(
                        getApplicationContext(), VehicleManagementActivity.class);
                startActivity(goToNextActivity);
            }

        });

        ImageView Truck_Memo = (ImageView) findViewById(R.id.img_memo_gen_PM);

        Truck_Memo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Intent goToNextActivity = new Intent(
                        getApplicationContext(), TruckNumberInputActivity.class);
                startActivity(goToNextActivity);
            }

        });
        

    }

    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}

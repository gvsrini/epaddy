package com.callippus.epaddy.saleitems;

import android.os.Bundle;
import android.widget.TextView;

import com.callippus.epaddy.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Logs extends Sale{

	  @Override
	  public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.log_cat);
	    TextView tv = (TextView)findViewById(R.id.logCat);
	    try {
	      Process process = Runtime.getRuntime().exec("logcat -d");
	      BufferedReader bufferedReader = new BufferedReader(
	      new InputStreamReader(process.getInputStream()));
	      String separator = System.getProperty("line.separator"); 
	      StringBuilder log=new StringBuilder();
	      String line;
	      tv.setText("");
	      while ((line = bufferedReader.readLine()) != null) {
	        log.append(line);
	        log.append(separator);
	      }
	      tv.setText(log.toString());
	    } catch (IOException e) {
	    }
	  }
}

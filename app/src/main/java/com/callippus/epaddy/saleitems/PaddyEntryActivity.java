package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.FarmerDB;
import com.callippus.epaddy.resources.DBConnection;


import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by callippus on 3/3/18.
 */

public class PaddyEntryActivity extends Activity {



    public static final String TAG = "paddy_entry";
    final Context context = this;
    public Button submit;
    public EditText paddy_quantity,moisture_content,total_amount;
    Spinner spinnerDropDown;
    public ArrayList<String> paddy_category = new ArrayList<String>();
    FarmerDB farmerDB = null;
    DBConnection dbc = null;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paddy_entry);
        /*ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        //farmerreg = (Button) findViewById(R.id.btnfarmerreg_FD);
        //getfarmerdetails = (Button) findViewById(R.id.btnfarmerdetails_FD);
        submit = (Button) findViewById(R.id.btnsubmit_PE);
        paddy_quantity=(EditText)findViewById(R.id.etpaddy_quantity_PE);
        moisture_content=(EditText)findViewById(R.id.etmoisture_content_PE);
        total_amount= (EditText)findViewById(R.id.ettotal_amount_PE);

        String paddy_quantity1=paddy_quantity.getText().toString();
        String moisturepercentage=moisture_content.getText().toString();

        if(paddy_quantity1.equals(""))
        {
            paddy_quantity1="10";
            paddy_quantity.setText(paddy_quantity1);

        }
        if( moisturepercentage.equals(""))
        {
            moisturepercentage="5%";
            moisture_content.setText(moisturepercentage);

        }

        int paddy_quantity2=Integer.parseInt(paddy_quantity1);
        int amount=(paddy_quantity2*1000);
        String totalamt=String.valueOf(amount);
        total_amount.setText(totalamt);




        try {
            dbc = new DBConnection(this);
            farmerDB = new FarmerDB(this);
        } catch (IOException e) {
            e.printStackTrace();
        }




      /* this is for dropdowns block code starts here*/

        spinnerDropDown =(Spinner)findViewById(R.id.paddy_category_PE);
        paddy_category=farmerDB.getPaddyCategory();
        ArrayAdapter<String> adapter= new ArrayAdapter<String>(this,android.
                R.layout.simple_spinner_dropdown_item ,paddy_category);

        spinnerDropDown.setAdapter(adapter);

        spinnerDropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
               /* int sid=spinnerDropDown.getSelectedItemPosition();*/
                /*blockcode1 = block_code.get(sid);*/


            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });



       submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                /*String paddy_quantity1=paddy_quantity.getText().toString();
                int paddy_quantity2=Integer.parseInt(paddy_quantity1);
                int amount=(paddy_quantity2*1000);
                String totalamt=String.valueOf(amount);
                total_amount.setText(totalamt);*/

                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Paddy Procurement");
                alert.setMessage("Procurement Successfully Completed")
                        .setCancelable(false)
                        .setNeutralButton("OK",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog1 = alert.create();
                alertDialog1.show();
            }

        });




    }

    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}










package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.PaddyMovementDB;
import com.callippus.epaddy.resources.DBConnection;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by callippus on 15/2/18.
 */

public class VehicleRegistrationDetailsActivity extends Activity {
    Button reg;
    PaddyMovementDB paddymovementDB = null;
    DBConnection dbc = null;
    Spinner spinnerDropDown;
    String vehicle_type;
    EditText vehicle_no,/*vehicle_type,*/transport_contractor_name,driver_name,driver_mobile_no;
    public static final String TAG="Vehicle Registration";
    public ArrayList<String> vehicle_type12 = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicle_reg_details);
       /* ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        reg = (Button) findViewById(R.id.Reg_VR);

        vehicle_no=(EditText)findViewById(R.id.et_vehicle_no);
        vehicle_no.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
       /* vehicle_type=(EditText)findViewById(R.id.et_vehicle_type);*/
        transport_contractor_name=(EditText)findViewById(R.id.et_transport_contractor_name);
        driver_name=(EditText)findViewById(R.id.et_driver_name);
        driver_mobile_no=(EditText)findViewById(R.id.et_driver_mobile_name);

        try {
            dbc = new DBConnection(this);
            paddymovementDB = new PaddyMovementDB(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        spinnerDropDown =(Spinner)findViewById(R.id.et_vehicle_type);
        vehicle_type12=paddymovementDB.getVehicleType();
        ArrayAdapter<String> adapter= new ArrayAdapter<String>(this,android.
                R.layout.simple_spinner_dropdown_item ,vehicle_type12);

        spinnerDropDown.setAdapter(adapter);

        spinnerDropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                int sid=spinnerDropDown.getSelectedItemPosition();
                vehicle_type = vehicle_type12.get(sid);


            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                String MobilePattern = "[0-9]{10}";
                String Vehicle_no_pattern="^[A-Z]{2}[0-9]{2}[A-Z]{2}[0-9]{4}$";
                Pattern p = Pattern.compile(MobilePattern);
                Pattern p1 = Pattern.compile(Vehicle_no_pattern);

                String vehicle_no1 = vehicle_no.getText().toString();
               /* String vehicle_type1 = vehicle_type.getText().toString();*/
                String transport_contractor_name1 = transport_contractor_name.getText().toString();
                String driver_name1 = driver_name.getText().toString();
                String driver_mobile_no1 = driver_mobile_no.getText().toString();

                Matcher m = p.matcher(driver_mobile_no1);
                Matcher vehicle_no_matcher=p1.matcher(vehicle_no1);

                // for date
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                        Locale.US);


                // get configuration

                String vehi_id = paddymovementDB.getConfiguration("TRUCK_REG_ID");
                int CheckVehicleNumber=paddymovementDB.isValidVehicle(vehicle_no1);



                //validation starts here
                if(vehicle_no1.equals("")){
                    vehicle_no.setError("Please Enter Vehicle Number");
                    vehicle_no.requestFocus();
                }
                else if(!vehicle_no_matcher.matches())
                {
                    vehicle_no.setError("Please Enter Valid Vehicle Number Ex: AP 15 BE 3338");
                    vehicle_no.requestFocus();
                }
                else if(vehicle_type.equals("")){
                    TextView errorText = (TextView)spinnerDropDown.getSelectedView();
                    errorText.setError("Please select vehicle type");
                    errorText.requestFocus();
                }
                else if(transport_contractor_name1.equals("")){
                    transport_contractor_name.setError("Please Enter Transport Contractor Name");
                    transport_contractor_name.requestFocus();
                }
                else if(driver_name1.equals("")){
                    driver_name.setError("Please Enter Driver Name");
                    driver_name.requestFocus();
                }
                else if(driver_mobile_no1.equals("")){
                    driver_mobile_no.setError("Please Enter Driver Mobile Number");
                    driver_mobile_no.requestFocus();
                }
                else if(!m.matches()){
                    driver_mobile_no.setError("Please Enter Valid Mobile Number");
                    driver_mobile_no.requestFocus();
                }
                else if(CheckVehicleNumber == 0)
                    {
                    vehicle_no.setError("Vehicle Number Already Registered");
                    vehicle_no.requestFocus();
                    }
                else
                {
                    ArrayList<ContentValues> list = new ArrayList<ContentValues>();

                    ContentValues c= new ContentValues();
                    c.put("ID",vehi_id);
                    list.add(c);

                    ContentValues c1= new ContentValues();
                    c.put("VEHICLE_NUMBER", vehicle_no1);
                    list.add(c1);

                    ContentValues c2 = new ContentValues();
                    c.put("VEHICLE_TYPE", vehicle_type);
                    list.add(c2);

                    ContentValues c4 = new ContentValues();
                    c.put("CONTRACTOR_NAME", transport_contractor_name1);
                    list.add(c4);

                    ContentValues c5 = new ContentValues();
                    c.put("DRIVER_NAME", driver_name1);
                    list.add(c5);

                    ContentValues c6 = new ContentValues();
                    c.put("DRIVER_MOBILE_NO", driver_mobile_no1);
                    list.add(c6);

                    ContentValues c7 = new ContentValues();
                    c.put("REG_DATE", dateTime.format(cal.getTime()));
                    list.add(c7);

                    ContentValues c8 = new ContentValues();
                    c.put("VERSION",dbc.appVersion());
                    list.add(c8);

                    ContentValues c9 = new ContentValues();
                    c.put("STATUS","1");
                    list.add(c9);

                    ContentValues c10 = new ContentValues();
                    c.put("SYNC_STATUS","0");
                    list.add(c10);

                    String table_name="VEHICLE_DETAILS";

                    int resp = paddymovementDB.saveDetails(list,table_name);

                    Log.d(TAG,"Response from PaddyMovementDB in Insert Vehicle_details table  :: "+resp);

                    if(resp == 0)
                    {
                        ////////////// For updating Config table value........
                        int id=Integer.parseInt(vehi_id);
                        id=id+1;
                        paddymovementDB.setConfig("TRUCK_REG_ID",Integer.toString(id));


                        Toast.makeText(VehicleRegistrationDetailsActivity.this, "Successfully created vehicle number:" + vehicle_no1, Toast.LENGTH_LONG).show();
                        Intent gotopreScreen = new Intent(
                                getApplicationContext(), VehicleManagementActivity.class);
                        startActivity(gotopreScreen);
                    }
                    else
                    {
                        Toast.makeText(VehicleRegistrationDetailsActivity.this, "Failed to create Vehicle No:" + vehicle_no1, Toast.LENGTH_LONG).show();
                    }

                }
            }
        });



    }


    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

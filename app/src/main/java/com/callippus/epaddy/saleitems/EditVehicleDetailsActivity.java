package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.PaddyMovementDB;
import com.callippus.epaddy.resources.DBConnection;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by callippus on 15/2/18.
 */

public class EditVehicleDetailsActivity extends Activity {



    public static final String TAG="EditVehicleDetails";
    public Button submit;
    public EditText vehicle_no;
    public ArrayList<String> list;
    PaddyMovementDB paddymovement = null;
    DBConnection dbc = null;
    UpdateVehicleDetailsActivity update=null;
    public static String vehicle_no1;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.editvehicledetails);
       /* ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        Preferences pref = new Preferences(getApplicationContext());

        submit = (Button) findViewById(R.id.btnsub_EVD);
        vehicle_no = (EditText) findViewById(R.id.et_vehicle_no_EVD);
        vehicle_no.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        update=new UpdateVehicleDetailsActivity();

        try {
            dbc = new DBConnection(this);
            paddymovement = new PaddyMovementDB(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vehicle_no1 = vehicle_no.getText().toString();
                Log.d(TAG, "Enterd Vehicle Number in Edit Vehicle Details screen  :: " + vehicle_no1);
                if(vehicle_no1.length() > 0) {
                    int resp = paddymovement.isValidVehicle(vehicle_no1);
                    Log.d(TAG, "resp from  Edit Vehicle Details screen  :: " + resp);
                    if (resp == 0) {
                        Intent goToNextActivity = new Intent(
                                getApplicationContext(), UpdateVehicleDetailsActivity.class);
                        startActivity(goToNextActivity);

                    } else {
                        vehicle_no.setError("Please Enter Valid  Vehicle Number");
                    }
                }
                else
                {
                    vehicle_no.setError("Please Enter Vehicle Number");
                }

            }

        });

    }

    public static String vehicleNumber() {
        return vehicle_no1;
    }


    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

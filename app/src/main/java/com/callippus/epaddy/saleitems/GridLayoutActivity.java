package com.callippus.epaddy.saleitems;

/**
 * Created by srini_000 on 04-06-2015.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.callippus.epaddy.LoginActivity;
import com.callippus.epaddy.PreferenceWithHeaders;
import com.callippus.epaddy.R;

public class GridLayoutActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_layout);

        ImageView img_sale = (ImageView) findViewById(R.id.image1);
        img_sale.setOnClickListener(new OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity = new Intent(
                        getApplicationContext(), PurchaseFarmerVerificationActivity.class);
                startActivity(goToNextActivity);
            }
        });

        ImageView img_farmerdetails = (ImageView) findViewById(R.id.image3);
        img_farmerdetails.setOnClickListener(new OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity;
                goToNextActivity = new Intent(
                        getApplicationContext(), FarmerManagementActivity.class);
                startActivity(goToNextActivity);
            }
        });

       ImageView img_settings = (ImageView) findViewById(R.id.image6);
        img_settings.setOnClickListener(new OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity = new Intent(
                        getApplicationContext(), PreferenceWithHeaders.class);
                startActivity(goToNextActivity);
            }
        });

       ImageView img_reports = (ImageView) findViewById(R.id.image5);
        img_reports.setOnClickListener(new OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity = new Intent(
                        getApplicationContext(), ReportsMainMenu.class);
                startActivity(goToNextActivity);
            }
        });
        ImageView img_logout = (ImageView) findViewById(R.id.image9);
        img_logout.setOnClickListener(new OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity = new Intent(
                        getApplicationContext(), LoginActivity.class);
                startActivity(goToNextActivity);
            }
        });


        /* paddy movement**/

        ImageView paddy_movement = (ImageView) findViewById(R.id.image7);
        paddy_movement.setOnClickListener(new OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity = new Intent(
                        getApplicationContext(), PaddyMovementActivity.class);
                startActivity(goToNextActivity);
            }
        });


        ImageView img_gunny = (ImageView) findViewById(R.id.image4);
        img_gunny.setOnClickListener(new OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity = new Intent(
                        getApplicationContext(), GunnyBagsManagement.class);
                startActivity(goToNextActivity);
            }
        });

    }

}
package com.callippus.epaddy.saleitems;

/**
 * Created by srini_000 on 04-06-2015.
 */

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.callippus.epaddy.R;
import com.callippus.epaddy.saleitems.Reports;

public class ReportsMainMenu extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reports_main_menu);
     /*   ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        ImageView farmer_related_reports = (ImageView) findViewById(R.id.image1_RMM);
        farmer_related_reports.setOnClickListener(new OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity = new Intent(
                        getApplicationContext(), Reports.class);
                startActivity(goToNextActivity);
            }
        });

        ImageView stock_related_reports = (ImageView) findViewById(R.id.image2_RMM);
        stock_related_reports.setOnClickListener(new OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity;
                goToNextActivity = new Intent(
                        getApplicationContext(), StockReportsMainMenu.class);
                startActivity(goToNextActivity);
            }
        });

       ImageView vehicle_movement_related_reports = (ImageView) findViewById(R.id.image3_RMM);
        vehicle_movement_related_reports.setOnClickListener(new OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity = new Intent(
                        getApplicationContext(), VehicleMovementReports.class);
                startActivity(goToNextActivity);
            }
        });

         ImageView gunny_bag_related_reports = (ImageView) findViewById(R.id.image4_RMM);
        gunny_bag_related_reports.setOnClickListener(new OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity = new Intent(
                        getApplicationContext(), GunnyReportsActivity.class);
                startActivity(goToNextActivity);
            }
        });

    }


    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
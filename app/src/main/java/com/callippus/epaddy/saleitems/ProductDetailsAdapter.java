package com.callippus.epaddy.saleitems;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.callippus.epaddy.R;

import java.util.ArrayList;

/**
 * Created by Callippus on 19-02-2018.
 */



public class ProductDetailsAdapter extends ArrayAdapter<String[]> {
    private final Context context;
    private final ArrayList<String[]> arrayList;


    public ProductDetailsAdapter(Context context, ArrayList<String[]> arrayList) {
        super(context, R.layout.rowlayout, arrayList);
        this.context = context;
        this.arrayList = arrayList;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View productView = inflater.inflate(R.layout.product_layout, parent, false);
        TextView product_id = (TextView) productView.findViewById(R.id.tvproductid_PL);
        TextView product_name = (TextView) productView.findViewById(R.id.tvproductname_PL);
        TextView product_qty = (TextView) productView.findViewById(R.id.tvqty_PL);

        product_id.setText(arrayList.get(position)[0]);
        product_name.setText(arrayList.get(position)[1]);
        product_qty.setText(arrayList.get(position)[2]);

        return productView;
    }



}


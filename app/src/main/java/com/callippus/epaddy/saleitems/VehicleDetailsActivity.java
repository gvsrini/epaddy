package com.callippus.epaddy.saleitems;

/**
 * Created by l_mah on 06-02-2018.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.PaddyMovementDB;
import com.callippus.epaddy.interfaces.OnVehicleEvents;
import com.callippus.epaddy.resources.DBConnection;
import com.callippus.epaddy.resources.FarmersArrayAdapter;


import java.util.ArrayList;

public class VehicleDetailsActivity extends Activity implements OnVehicleEvents{


    private final static int MESSAGE_BOX = 1;
    /* Handler to display UI response messages   */
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case MESSAGE_BOX:
                    String str = (String) msg.obj;
                    showDialog(str);
                    break;
                default:
                    break;
            }
        }

        private void showDialog(String str) {
        }
    };


    ListView vehicleListView = null;
    OnVehicleEvents mCallback1;
    Context context = this;
    PaddyMovementDB paddymovementDB = null;
    private ArrayList<String[]> vehicleList = new ArrayList<String[]>();
    private FarmersArrayAdapter vehiclesListAdapter;
    public static String vehicle_no;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicle_listview);
       /* ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        try {
            paddymovementDB = new PaddyMovementDB(this);
        } catch (Exception e) {
            //Show an alert dialog here.

            handler.obtainMessage(MESSAGE_BOX, "Exception opening DB" + e.getMessage()).sendToTarget();
            e.printStackTrace();
            return;
        }

        DBConnection db = null;

        vehicleListView = (ListView) findViewById(R.id.vehicleList);
        vehicleList = paddymovementDB.getVehicleDetails(vehicleList);
        vehiclesListAdapter = new FarmersArrayAdapter(this, vehicleList);
        vehicleListView.setAdapter(vehiclesListAdapter);



        vehicleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // i = i + 1;
                arg1.setSelected(true);
                String farmer[] = vehiclesListAdapter.getItem(arg2);
                mCallback1 = VehicleDetailsActivity.this;
                mCallback1.onVehicleClick(vehiclesListAdapter.getItem(arg2));
                // mCallback.onItemSelected(arg0, farmersListAdapter.getItem(arg2), arg2);
            }
        });

    }

    @Override
    public void onVehicleClick( String[] vehicle) {

        vehicle_no=vehicle[1];
        Intent goToNextActivity = new Intent(
                getApplicationContext(), VehicleInfoActivity.class);
        startActivity(goToNextActivity);

    }



    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}

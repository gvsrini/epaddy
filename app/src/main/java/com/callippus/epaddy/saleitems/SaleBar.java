package com.callippus.epaddy.saleitems;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.callippus.epaddy.R;
import com.callippus.epaddy.LoginActivity;
import com.callippus.epaddy.PreferenceWithHeaders;

public class SaleBar extends FragmentActivity {

    Intent i;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.sale, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.planh:
                i = new Intent(this, MainActivity.class);
                startActivity(i);
                break;
            case R.id.orders:
                i = new Intent(this, PreferenceWithHeaders.class);
                startActivity(i);
                break;
            case R.id.reprint:
                i = new Intent(this, MainActivity.class);
                startActivity(i);
                break;
            case R.id.log:
                i = new Intent(this, Logs.class);
                startActivity(i);
                break;
            case R.id.logout_item:
                Intent i = new Intent(this, LoginActivity.class);
                startActivity(i);
                break;

           /* case R.id.farmerdetails:
               i = new Intent(this, FarmarDetailsActivity.class);
                startActivity(i);
                break;*/
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.PaddyMovementDB;
import com.callippus.epaddy.resources.DBConnection;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by callippus on 16/2/18.
 */

public class VehicleInfoActivity extends Activity {
    public static final String TAG="VehicleInfoActivity";

    TextView vehicle_no,vehicle_type,transport_contractor_name,driver_name,driver_mobile_no;
   /* Button btn_back;*/
    String vehicle_no1;
    public ArrayList<String> list;
    PaddyMovementDB paddymovementDB = null;
    DBConnection dbc = null;
    VehicleDetailsActivity vda;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicle_info_activity);
       /* ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/
        try {
            dbc = new DBConnection(this);
            paddymovementDB = new PaddyMovementDB(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Preferences pref = new Preferences(getApplicationContext());
        vehicle_no1=vda.vehicle_no;
        list = paddymovementDB.populateVehicleDetails(vehicle_no1);
        showDetails(list);
        /*btn_back=(Button)findViewById(R.id.Back_btn);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent goToNextActivity = new Intent(
                        getApplicationContext(), VehicleDetailsActivity.class);
                startActivity(goToNextActivity);
            }
        });*/

    }

    public void showDetails(ArrayList<String> list1)
    {

        vehicle_no=(TextView)findViewById(R.id.et_vehicle_no);
        vehicle_type=(TextView)findViewById(R.id.et_vehicle_type);
        transport_contractor_name=(TextView)findViewById(R.id.et_transport_contractor_name);
        driver_name=(TextView)findViewById(R.id.et_driver_name);
        driver_mobile_no=(TextView)findViewById(R.id.et_driver_mobile_name);

        /*settinng text*/
        vehicle_no.setText(list1.get(1).toString());
        vehicle_type.setText(list1.get(2).toString());
        transport_contractor_name.setText(list1.get(3).toString());
        driver_name.setText(list1.get(4).toString());
        driver_mobile_no.setText(list1.get(5).toString());

    }

    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}

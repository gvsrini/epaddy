package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.FarmerDB;
import com.callippus.epaddy.resources.DBConnection;
import java.io.IOException;

/**
 * Created by Callippus on 08-02-2018.
 */

public class DeleteFarmerDetails extends Activity {


    public static final String TAG = "DeleteFarmerDetails";
    public Button delete;
    public EditText farmer_code;
    FarmerDB farmerDB = null;
    DBConnection dbc = null;
    public static String code;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.deletefarmerdetails);
      /*  ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        Preferences pref = new Preferences(getApplicationContext());

        delete = (Button) findViewById(R.id.btndel_EFD);
        farmer_code = (EditText) findViewById(R.id.etfcode_EFD);

        try {
            dbc = new DBConnection(this);
            farmerDB = new FarmerDB(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                code = farmer_code.getText().toString();
                Log.d(TAG, "Entered Farmer Code in Delete Farmer Details screen  :: " + code);
                if (code.length() > 0) {
                    int res = farmerDB.isValidCode(code);
                    if (res == 0) {
                    /* for dialog box  starts here */
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DeleteFarmerDetails.this);

                        // Setting Dialog Title
                        alertDialog.setTitle("Confirm Delete...");

                        // Setting Dialog Message
                        alertDialog.setMessage("Are you sure, You  want to delete FCODE "+code+"?");
                        // Setting Icon to Dialog
                        alertDialog.setIcon(R.drawable.deleteconfirmation);

                        // Setting Positive "Yes" Button
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                // Write your code here to invoke YES event
                                /*Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();*/

                                int resp1 = farmerDB.isValidCode(code);
                                String resp = farmerDB.deleteFarmer(code);
                                if (resp1 == 0) {

                                    if (resp.equals("Success")) {
                                        Toast.makeText(DeleteFarmerDetails.this, "Sucessfully Deteled" + code, Toast.LENGTH_LONG).show();
                                        Intent goToNextActivity = new Intent(
                                                getApplicationContext(), FarmerManagementActivity.class);
                                        startActivity(goToNextActivity);

                                    }
                                } else if (resp.equals("001_DELETE_FARMER_DEATILS")) {
                                    Toast.makeText(DeleteFarmerDetails.this, "Failed to Delete Farmer with Farmer Code.", Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                        // Setting Negative "NO" Button
                        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to invoke NO event
                                /*Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();*/
                                dialog.cancel();
                            }
                        });

                        // Showing Alert Message
                        alertDialog.show();
         /* here ended delete dialog */
                    }
                    else{
                        farmer_code.setError("Please Enter Valid Farmer Code");
                    }
                }
                else {
                    farmer_code.setError("Please Enter Farmer Code");
                }


            }
        });
    }




    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

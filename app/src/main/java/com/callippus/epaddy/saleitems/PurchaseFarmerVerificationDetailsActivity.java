package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.callippus.epaddy.R;
import com.callippus.epaddy.dbfunctions.FarmerDB;
import com.callippus.epaddy.resources.DBConnection;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by callippus on 3/3/18.
 */

public class PurchaseFarmerVerificationDetailsActivity extends Activity {


    public static final String TAG="detailed_farmer_details";
    TextView farmerCode,firstName,lastName,fatherName,presentAddr,permanentAddr,town;
    TextView district,pincode,blockcode,districtcode,dob,mobileno,altmobile,bankaccno,ifsc,bankname,aadhaarno;
    LinearLayout purchase_farmer_verification_details_xml;
    /* Button btn_back;*/
    public ArrayList<String> list;
   PurchaseFarmerVerificationActivity pfv;
   Button next;
    String code;
    FarmerDB FDB = null;
    DBConnection dbc = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_farmer_verification_details);
       /* ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/

        next=(Button)findViewById(R.id.btnnext_PFVD);
        try {
            dbc = new DBConnection(this);
            FDB = new FarmerDB(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Preferences pref = new Preferences(getApplicationContext());
        purchase_farmer_verification_details_xml = (LinearLayout) findViewById(R.id.purchase_farmer_verification_detailsXML);
        // farmer_details=FDB.populateFarmerDetails(code);
        code=pfv.Code();

        Log.d(TAG,"code value in PurchaseFarmerVerificationDetailsActivity is::"+code);
        list = FDB.populateFarmerDetails(code);
        show_details(list);
//        farmerCode=(TextView)findViewById(R.id.tvdfcode_DF);
        /*btn_back=(Button)findViewById(R.id.btnback_DF);*/
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent goToNextActivity = new Intent(
                        getApplicationContext(), PaddyEntryActivity.class);
                startActivity(goToNextActivity);
            }
        });

    }

    public void show_details(ArrayList<String> list1)
    {

        farmerCode=(TextView)findViewById(R.id.tvfcode_PFVD);
        firstName=(TextView)findViewById(R.id.tvfirstname_PFVD);
        lastName=(TextView)findViewById(R.id.tvlastname_PFVD);
        fatherName=(TextView)findViewById(R.id.tvfathername_PFVD);
        presentAddr=(TextView)findViewById(R.id.tvpresentadd_PFVD);
        permanentAddr=(TextView)findViewById(R.id.tvpermentadd_PFVD);
        town=(TextView)findViewById(R.id.tvtown_PFVD);
        district=(TextView)findViewById(R.id.tvdistrict_PFVD);
        pincode=(TextView)findViewById(R.id.tvpincode_PFVD);
        blockcode=(TextView)findViewById(R.id.tvblockcode_PFVD);
        districtcode=(TextView)findViewById(R.id.tvdistrictcode_PFVD);
        dob=(TextView)findViewById(R.id.tvdob_PFVD);
        mobileno=(TextView)findViewById(R.id.tvmobileno_PFVD);
        altmobile=(TextView)findViewById(R.id.tvaltmobileno_PFVD);
        bankaccno=(TextView)findViewById(R.id.tvbankaccno_PFVD);
        ifsc=(TextView)findViewById(R.id.tvifsc_PFVD);
        bankname=(TextView)findViewById(R.id.tvbankname_PFVD);
        aadhaarno=(TextView)findViewById(R.id.tvaadhar_PFVD);

        Log.d(TAG,"list value in show details::"+list1.get(0).toString());
        farmerCode.setText(list1.get(0).toString());
        firstName.setText(list1.get(1).toString());
        lastName.setText(list1.get(2).toString());
        fatherName.setText(list1.get(3).toString());
        presentAddr.setText(list1.get(4).toString());
        permanentAddr.setText(list1.get(5).toString());
        town.setText(list1.get(6).toString());
        district.setText(list1.get(7).toString());
        pincode.setText(list1.get(8).toString());
        blockcode.setText(list1.get(9).toString());
        districtcode.setText(list1.get(10).toString());
        dob.setText(list1.get(11).toString());
        mobileno.setText(list1.get(12).toString());
        altmobile.setText(list1.get(13).toString());
        bankaccno.setText(list1.get(14).toString());
        ifsc.setText(list1.get(15).toString());
        bankname.setText(list1.get(16).toString());
        aadhaarno.setText(list1.get(17).toString());
    }


    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

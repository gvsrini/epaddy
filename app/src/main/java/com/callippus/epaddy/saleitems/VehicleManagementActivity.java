package com.callippus.epaddy.saleitems;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.callippus.epaddy.R;


/**
 * Created by callippus on 14/2/18.
 */

public class VehicleManagementActivity extends Activity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicledetails);
       /* ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/


        ImageView vehicle_reg_details = (ImageView) findViewById(R.id.image1_VD);
        ImageView modify_vehicle_details = (ImageView) findViewById(R.id.image2_VD);
        ImageView get_vehicle_details = (ImageView) findViewById(R.id.image3_VD);
        ImageView delete_vehicle_details = (ImageView) findViewById(R.id.btn_delete_vehicle_details_VD);



        vehicle_reg_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Intent goToNextActivity = new Intent(
                        getApplicationContext(), VehicleRegistrationDetailsActivity.class);
                startActivity(goToNextActivity);
            }

        });

       modify_vehicle_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Intent goToNextActivity = new Intent(
                        getApplicationContext(), EditVehicleDetailsActivity.class);
                startActivity(goToNextActivity);
            }

        });

        get_vehicle_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Intent goToNextActivity = new Intent(
                        getApplicationContext(), VehicleDetailsActivity.class);
                startActivity(goToNextActivity);
            }

        });


        delete_vehicle_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Intent goToNextActivity = new Intent(
                        getApplicationContext(), DeleteVehicleDetailsActivity.class);
                startActivity(goToNextActivity);
            }

        });


    }



    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}

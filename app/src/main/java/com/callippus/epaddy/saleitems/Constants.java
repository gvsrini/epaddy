package com.callippus.epaddy.saleitems;

/**
 * Created by Lenovo on 07-06-2015.
 */
public class Constants {
    final public static String TAG = "CALLIPPUS_LOG";
    final public static String NAMESPACE = "http://microsoft.com/webservices/";
    final public static String DIST_SOAP_ACTION = "http://microsoft.com/webservices/cardholderFamilyInfo";
    final public static String DIST_METHOD_NAME = "cardholderFamilyInfo";
    final public static String ORDER_SOAP_ACTION = "http://microsoft.com/webservices/postTransaction";
    final public static String ORDER_METHOD_NAME = "postTransaction";
    final public static int TIMEOUT = 120000; //2 minutes
    final public static int LOADING = 1;
    final public static int EXTRACTING = 2;
}

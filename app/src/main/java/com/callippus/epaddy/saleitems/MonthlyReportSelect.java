package com.callippus.epaddy.saleitems;

/**
 * Created by callippus on 23/2/18.
 */

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.callippus.epaddy.R;


public class MonthlyReportSelect extends Activity {


    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.monthly_report_select);
       /* ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/




        ImageView img_farmer_monthly_report = (ImageView) findViewById(R.id.img_farmer_details_report_MRS);
        img_farmer_monthly_report.setOnClickListener(new View.OnClickListener() {
            //@Override
            public void onClick(View v) {
                Intent goToNextActivity = new Intent(
                        getApplicationContext(),FarmerRegMonthlyReport.class);
                startActivity(goToNextActivity);
            }
        });


    }

    /*added for back menu*/
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
